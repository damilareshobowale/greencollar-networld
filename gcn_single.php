<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once("admin/inc_dbfunctions.php");

function databaseConnect()
{
    require("admin/connectionstrings.php");


    $mycon = new PDO("mysql:host=$MYSQL_Server;dbname=$MYSQL_Database;charset=utf8", "$MYSQL_Username", "$MYSQL_Password"); 
    $mycon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $mycon->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);    
    return $mycon;
}


function sendEmail($email,$subject,$message)
{
   require 'vendor/autoload.php';

	$message = "<!DOCTYPE html>
	<html>
			<head>
					<meta charset='utf-8'>
					<meta name='viewport' content='width=device-width, initial-scale=1.0'>
					<meta name='description' content='Greencollar Networld - .'>
					<meta name='author' content='Wealth Fund Global'>
	
					<link rel='shortcut icon' href='img/logo/logowfg.ico'>
	
					<title>Email Templates</title>
	
					<link href='http://greencollarnetworld.com/mod/assets/css/bootstrap.min.css' rel='stylesheet' type='text/css' />
					<link href='http://greencollarnetworld.com/mod/assets/css/core.css' rel='stylesheet' type='text/css' />
					<link href='http://greencollarnetworld.com/mod/assets/css/components.css' rel='stylesheet' type='text/css' />
					<link href='http://greencollarnetworld.com/mod/assets/css/icons.css' rel='stylesheet' type='text/css' />
					<link href='http://greencollarnetworld.com/mod/assets/css/pages.css' rel='stylesheet' type='text/css' />
					<link href='http://greencollarnetworld.com/mod/assets/css/responsive.css' rel='stylesheet' type='text/css' />
					<link href='http://greencollarnetworld.com/mod/css/style.css' rel='stylesheet' type='text/css' />
	
					
			</head>
	
	
			<body>
				<div style='max-width: 100vw; height: auto;padding: 5vw;background-color: rgb(3, 174, 67);'>
					<div style='background-color: #FFFFFF;padding: 5vw 2%'>
					<div style='display:flex;justify-content: center;text-align: center;align-content: center;align-items: center;flex-direction:row;'>
							<center><div>
								<center><img src='http://greencollarnetworld.com/mod/img/logo/logogcn.png' alt='GreenCollar_Logo' style='height: 100px;' /></center>
								<br/>
								<h1>Greencollar Networld</h1>
							</div>
							</center>
					</div>
					<hr />
					<div style='margin: 5%;'>
					". $message ."
					</div>
	
					<div style='margin: 5%;'>
						<hr />
						<center>
							<div style='display:flex;justify-content: center;text-align: center;align-content: center;align-items: center;flex-direction: row;'>
							<p>This email is automatically generated from Greencollar Networld Mailing Platform</p><br />
							<p>&copy; 2019. <a href='https://greencollarnetworld.com'>Greencollar Networld</a></p>
						</div>
						</center>
					</div>
	
					</div>
				</div>
	
			</body>
	</html>";

	$mail = new PHPMailer(true);

	try {
			//Server settings                                     // Enable verbose debug output
			$mail->isSMTP();                                            // Set mailer to use SMTP
			$mail->Host       = 'smtp.gmail.com';  // Specify main and backup SMTP servers
			$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
			$mail->Username   = 'greencollarnetworld@gmail.com';                     // SMTP username
			$mail->Password   = 'security01@';                               // SMTP password
			$mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
			$mail->Port       = 587;                                    // TCP port to connect to

			//Recipients
			$mail->setFrom('noreply@greencollarnetworld.com', 'Greencollar Networld');
			$mail->addAddress($email);     // Add a recipient
			$mail->addReplyTo('support@greencollarnetworld.com', 'Greencollar Networld Support');

			// Content
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = $subject;
			$mail->Body    = $message;
			$mail->AltBody = strip_tags($message);

			$mail->send();
			return true;
	} catch (Exception $e) {
			echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
			return false;
	}
}

function numToOrdinalWord($num)
{
    $first_word = array('eth','First','Second','Third','Fouth','Fifth','Sixth','Seventh','Eighth','Ninth','Tenth','Elevents',
    'Twelfth','Thirteenth','Fourteenth','Fifteenth','Sixteenth','Seventeenth','Eighteenth','Nineteenth','Twentieth');
    $second_word =array('','','Twenty','Thirty','Forty','Fifty');

    if($num <= 20)
        return $first_word[$num];

    $first_num = substr($num,-1,1);
    $second_num = substr($num,-2,1);

    return $string = str_replace('y-eth','ieth',$second_word[$second_num].'-'.$first_word[$first_num]);
}



$mycon = databaseConnect();




//NotifySubsequentReCommitmentCheck();





function NotifySubsequentReCommitmentCheck()
{
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();
    //1. check all the donations with status of recommit =  0 and donation status = 3 and matched status = 5

    $memberstatus = 0;
    $status = 5;
    $recommitmenttype = 0;
    //check for all the Recommitment
	$providehelp_all = $dataRead->checklast_gethelpfirstdesc($mycon, '0', '5', '0', $recommitmenttype);

    foreach($providehelp_all as $row)
    {
        //get the cycle of the user 
		$getCycle = $dataRead->cycle_getbyid($mycon, $row['member_id']);
		
        if ($getCycle)
        {
            if (strtotime($row['readydonation_ph']) <= strtotime(date("Y-m-d H:i:s"))) // notify the receives add if the donation to PH again is more than the current time
            {
				
                //get the name of the receiver 
                $amount = $row['donation_ph'];
                $transfermessage = "<div class='container'>
                                    <p>Dear ".$row['username'].",</p>
                                    <p>This is to notify you that you are due to pay deposit. <br />
                                    Kindly click on the Deposit Fund Button on your dashboard. </p>
                                    <p> Thank you.</p>
                                    <p style='margin-top: 100px'><small><em>This message is auto-generated, please do not reply via your email.</em></small></p>
                                </div>";
                if(sendEmail($row['email'],"Notification for Deposit Fund - Greencollar Networld", $transfermessage)) {
                    echo "done!";
                    echo "Deposit Fund Request Matched";
                } 
                else {
                    var_dump($getCycle);
                }
            }

        }
    }
}