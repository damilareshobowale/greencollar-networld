<?php



class DataWrite
{   
    //create the useracccount
    function members_add($mycon,$username,$firstname,$lastname,$password,$email,$phonenumber,$referral_id,$expiry,$captcha)
    {
      $thedate = date("Y-m-d H:i:s");
      $sql = "INSERT INTO `members` SET `username` = :username
          ,`firstname` = :firstname
          ,`lastname` = :lastname
          ,`password` = :password
          ,`email` = :email
          ,`phonenumber` = :phonenumber
          ,`referral_id` = :referral_id
          ,`expiry` = :expiry
          ,`createdon` = :createdon
          ,`createdby` = :createdby
          ,`status` = :status
          ,`captcha` = :captcha";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":username", $username, PDO::PARAM_STR);
      $myrec->bindValue(":firstname", $firstname,PDO::PARAM_STR);
      $myrec->bindValue(":lastname", $lastname,PDO::PARAM_STR);
      $myrec->bindValue(":password", $password,PDO::PARAM_STR);
      $myrec->bindValue(":email", $email,PDO::PARAM_STR);
      $myrec->bindValue(":phonenumber", $phonenumber,PDO::PARAM_STR);
      $myrec->bindValue(":referral_id", $referral_id,PDO::PARAM_STR);
      $myrec->bindValue(":expiry", $expiry,PDO::PARAM_STR);
      $myrec->bindValue(":createdon", $thedate,PDO::PARAM_STR);
      $myrec->bindValue(":createdby", $referral_id,PDO::PARAM_STR);
      $myrec->bindValue(":status", '10',PDO::PARAM_STR);// status set to show member to pay activation fee
      $myrec->bindValue(":captcha", $captcha,PDO::PARAM_STR);
      $myrec->execute();
      
      if ($myrec->rowCount() < 1) return false;
      
      return $mycon->lastInsertId();
      
    }

    function accountdetails_add($mycon,$member_id, $bankaccountname,$bankaccountnumber, $bankname, $type)
    {
       $thedate = date("Y-m-d H:i:s");
        $sql = "INSERT INTO `accountdetails` SET `member_id` = :member_id
          ,`bankname` = :bankname
          ,`bankaccountname` = :bankaccountname
          ,`bankaccountnumber` = :bankaccountnumber
          ,`createdon` = :createdon
          ,`type` = :type
          ,`status` = :status";
          $myrec = $mycon->prepare($sql);
          $myrec->bindValue(":member_id", $member_id);
          $myrec->bindValue(":bankname", $bankname);
          $myrec->bindValue(":bankaccountname", $bankaccountname);
          $myrec->bindValue(":bankaccountnumber", $bankaccountnumber);
          $myrec->bindValue(":type", $type);
          $myrec->bindValue(":createdon", $thedate);
          $myrec->bindValue(":status", '5');// status set to 5 to show Active member
          $myrec->execute();
          
          if ($myrec->rowCount() < 1) return false;
          
          return $mycon->lastInsertId();
       
    }

    function amountdetails_add($mycon,$amount, $member_id)
    {
       $thedate = date("Y-m-d H:i:s");
        $sql = "INSERT INTO `activation-fee` SET `amount` = :amount
          ,`created_by` = :created_by
          ,`created_on` = :created_on";
          $myrec = $mycon->prepare($sql);
          $myrec->bindValue(":amount", $amount);
          $myrec->bindValue(":created_by", $member_id);
          $myrec->bindValue(":created_on", $thedate);
          $myrec->execute();
          
          if ($myrec->rowCount() < 1) return false;
          
          return $mycon->lastInsertId();
       
    }

    function monthlyamountdetails_add($mycon,$amount, $member_id)
    {
       $thedate = date("Y-m-d H:i:s");
        $sql = "INSERT INTO `monthly-fee` SET `amount` = :amount
          ,`created_by` = :created_by
          ,`created_on` = :created_on";
          $myrec = $mycon->prepare($sql);
          $myrec->bindValue(":amount", $amount);
          $myrec->bindValue(":created_by", $member_id);
          $myrec->bindValue(":created_on", $thedate);
          $myrec->execute();
          
          if ($myrec->rowCount() < 1) return false;
          
          return $mycon->lastInsertId();
       
    }


    //update the bank accounts details
    function accountdetails_update($mycon, $bankaccountname,$bankaccountnumber, $bankname, $accountdetail_id)
    {
           $thedate = date("Y-m-d H:i:s");
            $sql = "UPDATE `accountdetails` SET
              `bankname` = :bankname
              ,`bankaccountname` = :bankaccountname
              ,`bankaccountnumber` = :bankaccountnumber
              ,`createdon` = :createdon WHERE `accountdetail_id` = :accountdetail_id";
          $myrec = $mycon->prepare($sql);
          $myrec->bindValue(":accountdetail_id", $accountdetail_id);
          $myrec->bindValue(":bankname", $bankname);
          $myrec->bindValue(":bankaccountname", $bankaccountname);
          $myrec->bindValue(":bankaccountnumber", $bankaccountnumber);
          $myrec->bindValue(":createdon", $thedate);
          
          if (!$myrec->execute()) return false;
          
          return true;
       
    }

    //update the bank accounts details
    function donationupdate_ghdate($mycon, $donation_id,$readydonation_gh)
    {
           $thedate = date("Y-m-d H:i:s");
            $sql = "UPDATE `donations` SET
              `readydonation_gh` = :readydonation_gh WHERE `donation_id` = :donation_id";
          $myrec = $mycon->prepare($sql);
          $myrec->bindValue(":donation_id", $donation_id);
          $myrec->bindValue(":readydonation_gh", $readydonation_gh);
          
          if (!$myrec->execute()) return false;
          
          return true;
       
    }
    
    //update the bank accounts details
    function resetCycleToZero($mycon, $member_id, $status)
    {
            $sql = "UPDATE `cycle` SET
              `status` = :status WHERE `member_id` = :member_id";
          $myrec = $mycon->prepare($sql);
          $myrec->bindValue(":member_id", member_id);
          $myrec->bindValue(":status", $status);
          
          if (!$myrec->execute()) return false;
          
          return true;
       
    }

     //update the bank accounts details
     function updateMemberDonationRecommitment($mycon, $donation_id, $recommitment)
     {
            $sql = "UPDATE `donations` SET
               `recommitment` = :recommitment WHERE `donation_id` = :donation_id";
           $myrec = $mycon->prepare($sql);
           $myrec->bindValue(":donation_id", $donation_id);
           $myrec->bindValue(":recommitment", $recommitment);
           
           if (!$myrec->execute()) return false;
           
           return true;
        
     }


    //update the activation fee details
    function acctivationfee_update($mycon, $amount,$activation_fee_id)
    {
            $sql = "UPDATE `activation-fee` SET
              `amount` = :amount,
              `created_on` = :created_on WHERE `activation_fee_id` = :activation_fee_id";
          $myrec = $mycon->prepare($sql);
          $myrec->bindValue(":activation_fee_id", $activation_fee_id);
          $myrec->bindValue(":amount", $amount);
          
          if (!$myrec->execute()) return false;
          
          return true;
       
    }

        //update the activation fee details
        function monthlyfee_update($mycon, $amount,$monthly_fee_id)
        {
                $sql = "UPDATE `monthly-fee` SET
                  `amount` = :amount WHERE `monthly_fee_id` = :monthly_fee_id";
              $myrec = $mycon->prepare($sql);
              $myrec->bindValue(":monthly_fee_id", $monthly_fee_id);
              $myrec->bindValue(":amount", $amount);
              
              if (!$myrec->execute()) return false;
              
              return true;
           
        }


    //update the members
    function members_update($mycon, $firstname, $lastname, $phonenumber, $password, $member_id)
    {
        $thedate = date("Y-m-d H:i:s");
        $sql = "UPDATE `members` SET `firstname` = :firstname
            ,`lastname` = :lastname
            ,`password` = :password
            ,`phonenumber` = :phonenumber
            ,`modifiedon` = :modifiedon
            ,`modifiedby` = :modifiedby WHERE `member_id` = :member_id";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->bindValue(":firstname", $firstname);
        $myrec->bindValue(":lastname", $lastname);
        $myrec->bindValue(":phonenumber", $phonenumber);
        $myrec->bindValue(":password", $password);
        $myrec->bindValue(":modifiedon", $thedate);
        $myrec->bindValue(":modifiedby", $member_id);
        
        if (!$myrec->execute()) return false;
        
        return true;
        
    }

    function bankaccounts_deleteall($mycon, $member_id, $type)
    {
      $sql = "DELETE FROM `accountdetails` WHERE `member_id` = :member_id AND `type` = :type";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":member_id", $member_id, PDO::PARAM_STR);
      $myrec->bindValue(":type", $type, PDO::PARAM_STR);
      $myrec->execute();
      
      if ($myrec->rowCount() < 1) return false;

      return true;
    }

    function members_delete($mycon, $member_id)
    {
      $sql = "DELETE FROM `members` WHERE `member_id` = :member_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":member_id", $member_id, PDO::PARAM_STR);
      $myrec->execute();
      
      if ($myrec->rowCount() < 1) return false;

      return true;
    }
    

    function activationamount_deleteall($mycon)
    {
      $sql = "DELETE FROM `activation-fee`";
      $myrec = $mycon->prepare($sql);
      $myrec->execute();
      
      if ($myrec->rowCount() < 1) return false;

      return true;
    }

    function monthlyamount_deleteall($mycon)
    {
      $sql = "DELETE FROM `monthly-fee`";
      $myrec = $mycon->prepare($sql);
      $myrec->execute();
      
      if ($myrec->rowCount() < 1) return false;

      return true;
    }

    function bankaccounts_delete($mycon, $member_id, $accountdetail_id, $type)
    {
      $sql = "DELETE FROM `accountdetails` WHERE `member_id` = :member_id AND `accountdetail_id` = :accountdetail_id AND `type` = :type";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":member_id", $member_id, PDO::PARAM_STR);
      $myrec->bindValue(":accountdetail_id", $accountdetail_id, PDO::PARAM_STR);
      $myrec->bindValue(":type", $type, PDO::PARAM_STR);
      $myrec->execute();
      
      if ($myrec->rowCount() < 1) return false;

      return true;
    }

    function bankaccounts_deletesingle($mycon, $member_id)
    {
      $sql = "DELETE FROM `accountdetails` WHERE `member_id` = :member_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":member_id", $member_id, PDO::PARAM_STR);
      $myrec->execute();
      
      if ($myrec->rowCount() < 1) return false;

      return true;
    }


    //add to onations
    function donation_add($mycon,$amount,$currentuserid,$readydonation_ph,$readydonation_gh,$accountdetail_id,$firsttime, $firststatus, $recommitment, $type)
    {
        $thedate = date("Y-m-d H:i:s");
        $sql = "INSERT INTO `donations` SET `donation_ph` = :donation_ph
            ,`member_id` = :member_id
            ,`readydonation_ph` = :readydonation_ph
            ,`readydonation_gh` = :readydonation_gh
            ,`accountdetail_id` = :accountdetail_id
            ,`firsttime` = :firsttime
            ,`recommitment` = :recommitment
            ,`status` = :status
            ,`firstph` = :firstph
            ,`createdon` = :createdon
            ,`type` = :type";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $currentuserid);
        $myrec->bindValue(":donation_ph", $amount);
        $myrec->bindValue(":readydonation_ph", $readydonation_ph);
        $myrec->bindValue(":readydonation_gh", $readydonation_gh);
        $myrec->bindValue(":accountdetail_id", $accountdetail_id);
        $myrec->bindValue(":firsttime", $firsttime);
        $myrec->bindValue(":firstph", $firststatus);
        $myrec->bindValue(":recommitment", $recommitment);
        $myrec->bindValue(":createdon", $thedate);
        $myrec->bindValue(":type", $type);
        $myrec->bindValue(":status", '5');// status set to 5 to show New donation
        $myrec->execute();
        
        if($myrec->rowCount() < 1) return false;
        
        return $mycon->lastInsertId();
        
        
    }

     //add to donations made by admin
    function donation_add_admin($mycon,$amount,$currentuserid,$readydonation_ph,$readydonation_gh,$accountdetail_id,$firsttime, $firststatus, $createdon)
    {
        $sql = "INSERT INTO `donations` SET `donation_ph` = :donation_ph
            ,`member_id` = :member_id
            ,`readydonation_ph` = :readydonation_ph
            ,`readydonation_gh` = :readydonation_gh
            ,`accountdetail_id` = :accountdetail_id
            ,`firsttime` = :firsttime
            ,`status` = :status
            ,`matchedstatus` = :matchedstatus
            ,`firstph` = :firstph
            ,`createdon` = :createdon";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $currentuserid);
        $myrec->bindValue(":donation_ph", $amount);
        $myrec->bindValue(":readydonation_ph", $readydonation_ph);
        $myrec->bindValue(":readydonation_gh", $readydonation_gh);
        $myrec->bindValue(":accountdetail_id", $accountdetail_id);
        $myrec->bindValue(":firsttime", $firsttime);
        $myrec->bindValue(":firstph", $firststatus);
        $myrec->bindValue(":matchedstatus", '5'); //macthed status set to 5 to show already matched status
        $myrec->bindValue(":createdon", $createdon);
        $myrec->bindValue(":status", '0');// status set to 0 to show confirmed donation
        $myrec->execute();
        
        if($myrec->rowCount() < 1) return false;
        
        return $mycon->lastInsertId();
        
        
    }

    //delete the Deposit Fund request
    function donationrequest_delete($mycon, $donation_id)
    {
        $sql = "DELETE FROM `donations` WHERE `donation_id` = :donation_id AND `status` = :status";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":donation_id", $donation_id);
        $myrec->bindValue(":status", 5); //shows active donations
        $myrec->execute();
        
        if($myrec->rowCount() < 1) return false;
        
        return true;
 
    }

    //delete the Deposit Fund request
    function donation_delete($mycon, $donation_id)
    {
        $sql = "DELETE FROM `donations` WHERE `donation_id` = :donation_id LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":donation_id", $donation_id);
        $myrec->execute();
        
        if($myrec->rowCount() < 1) return false;
        
        return true;
 
    }

    function donation_deleteall($mycon, $member_id)
    {
        $sql = "DELETE FROM `donations` WHERE `member_id` = :member_id";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->execute();
        
        if($myrec->rowCount() < 1) return false;
        
        return true;
 
    }

    //delete the Deposit Fund request
    function matching_deletebyid($mycon, $matching_id)
    {
        $sql = "DELETE FROM `matching` WHERE `matching_id` = :matching_id LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":matching_id", $matching_id);
        $myrec->execute();
        
        if($myrec->rowCount() < 1) return false;
        
        return true;
 
    }
    
    //delete the activation evidence
    function activation_evidence_deletebyidmember($mycon, $member_id)
    {
        $sql = "DELETE FROM `activation-evidence` WHERE `member_id` = :member_id LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", member_id);
        $myrec->execute();
        
        if($myrec->rowCount() < 1) return false;
        
        return true;
 
    }

    //update the growth colum of the donations
    function donations_growthupdate($mycon, $donation_id, $growth)
    {
      $sql = "UPDATE `donations` SET `growth` = :growth WHERE `donation_id` = :donation_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":growth", $growth, PDO::PARAM_STR);
      $myrec->bindValue(":donation_id", $donation_id, PDO::PARAM_STR);

      if (!$myrec->execute()) return false;

      return true;

    }

    function donation_add_gh($mycon,$amount,$currentuserid,$readydonation_ph,$readydonation_gh,$accountdetail_id,$firsttime, $type)
    {
        $thedate = date("Y-m-d H:i:s");
        $sql = "INSERT INTO `donations` SET `donation_gh` = :donation_gh
            ,`member_id` = :member_id
            ,`readydonation_ph` = :readydonation_ph
            ,`readydonation_gh` = :readydonation_gh
            ,`accountdetail_id` = :accountdetail_id
            ,`firsttime` = :firsttime
            ,`status` = :status
            ,`createdon` = :createdon
            ,`type` = :type";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $currentuserid);
        $myrec->bindValue(":donation_gh", $amount);
        $myrec->bindValue(":readydonation_ph", $readydonation_ph);
        $myrec->bindValue(":readydonation_gh", $readydonation_gh);
        $myrec->bindValue(":accountdetail_id", $accountdetail_id);
        $myrec->bindValue(":firsttime", $firsttime);
        $myrec->bindValue(":createdon", $thedate);
        $myrec->bindValue(":type", $type);
        $myrec->bindValue(":status", '5');// status set to 5 to show Active receive fund requests
        $myrec->execute();
        
        if($myrec->rowCount() < 1) return false;
        
        return $mycon->lastInsertId();
        
        
    }

    function donation_add_ph($mycon,$amount,$currentuserid,$readydonation_ph,$readydonation_gh,$accountdetail_id,$firsttime, $type, $growth)
    {
        $thedate = date("Y-m-d H:i:s");
        $sql = "INSERT INTO `donations` SET `donation_ph` = :donation_ph
            ,`member_id` = :member_id
            ,`readydonation_ph` = :readydonation_ph
            ,`readydonation_gh` = :readydonation_gh
            ,`accountdetail_id` = :accountdetail_id
            ,`firsttime` = :firsttime
            ,`status` = :status
            ,`createdon` = :createdon
            ,`growth` = :growth
            ,`type` = :type";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $currentuserid);
        $myrec->bindValue(":donation_ph", $amount);
        $myrec->bindValue(":readydonation_ph", $readydonation_ph);
        $myrec->bindValue(":readydonation_gh", $readydonation_gh);
        $myrec->bindValue(":accountdetail_id", $accountdetail_id);
        $myrec->bindValue(":firsttime", $firsttime);
        $myrec->bindValue(":createdon", $thedate);
        $myrec->bindValue(":growth", $growth);
        $myrec->bindValue(":type", $type);
        $myrec->bindValue(":status", '5');// status set to 5 to show Active receive fund requests
        $myrec->execute();
        
        if($myrec->rowCount() < 1) return false;
        
        return $mycon->lastInsertId();
        
        
    }

    //add new to the donations receivable
    function donationsreceivable_add($mycon, $member_id, $amount, $withdrawn, $balance)
    {
      $thedate = date("Y-m-d H:i:s");
      $sql = "INSERT INTO `donationsreceivable` SET
              `member_id` = :member_id
              ,`amount` = :amount
              ,`withdrawn` = :withdrawn
              ,`balance` = :balance
              ,`thedate` = :thedate";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":member_id", $member_id, PDO::PARAM_STR);
      $myrec->bindValue(":amount", $amount, PDO::PARAM_STR);
      $myrec->bindValue(":withdrawn", $withdrawn, PDO::PARAM_STR);
      $myrec->bindValue(":balance", $balance, PDO::PARAM_STR);
      $myrec->bindValue(":thedate", $thedate, PDO::PARAM_STR);
      $myrec->execute();

      if ($myrec->rowCount() < 1) return false;

      return $mycon->lastInsertId();
    }


    //add new to the donations receivable
    function cycle_add($mycon, $member_id, $cycle)
    {
      $thedate = date("Y-m-d H:i:s");
      $sql = "INSERT INTO `cycle` SET
              `member_id` = :member_id
              ,`cycle` = :cycle";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":member_id", $member_id, PDO::PARAM_STR);
      $myrec->bindValue(":cycle", $cycle, PDO::PARAM_STR);
      $myrec->execute();

      if ($myrec->rowCount() < 1) return false;

      return $mycon->lastInsertId();
    }

    //add new to the donations receivable
    function donationsreceivable_updatezero($mycon, $member_id, $amount, $balance, $withdrawn)
    {
      $thedate = date("Y-m-d H:i:s");
      $sql = "UPDATE `donationsreceivable` SET
              `amount` = :amount
              ,`withdrawn` = :withdrawn
              ,`balance` = :balance
              ,`thedate` = :thedate WHERE `member_id` = :member_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":member_id", $member_id, PDO::PARAM_STR);
      $myrec->bindValue(":amount", $amount, PDO::PARAM_STR);
      $myrec->bindValue(":withdrawn", $withdrawn, PDO::PARAM_STR);
      $myrec->bindValue(":balance", $balance, PDO::PARAM_STR);
      $myrec->bindValue(":thedate", $thedate, PDO::PARAM_STR);

      if (!$myrec->execute()) return false;

      return true;
    }

    //update the cycle
    function cycle_update($mycon, $member_id, $cycle)
    {
      $sql = "UPDATE `cycle` SET
              `cycle` = :cycle WHERE `member_id` = :member_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":member_id", $member_id, PDO::PARAM_STR);
      $myrec->bindValue(":cycle", $cycle, PDO::PARAM_STR);

      if (!$myrec->execute()) return false;

      return true;
    }
    
    //update the cycle
    function cycle_updatelevel($mycon, $member_id, $level, $status)
    {
      $sql = "UPDATE `cycle` SET
              `level` = :level
              ,`status` = :status WHERE `member_id` = :member_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":member_id", $member_id, PDO::PARAM_STR);
      $myrec->bindValue(":level", $level, PDO::PARAM_STR);
      $myrec->bindValue(":status", $status, PDO::PARAM_STR);

      if (!$myrec->execute()) return false;

      return true;
    }

    //add new to the donations receivable
    function donationsreceivable_update($mycon, $member_id, $amount, $balance)
    {
      $thedate = date("Y-m-d H:i:s");
      $sql = "UPDATE `donationsreceivable` SET
              `amount` = :amount
              ,`balance` = :balance
              ,`thedate` = :thedate WHERE `member_id` = :member_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":member_id", $member_id, PDO::PARAM_STR);
      $myrec->bindValue(":amount", $amount, PDO::PARAM_STR);
      $myrec->bindValue(":balance", $balance, PDO::PARAM_STR);
      $myrec->bindValue(":thedate", $thedate, PDO::PARAM_STR);

      if (!$myrec->execute()) return false;

      return true;
    }

    function donationsreceivable_updatewithdrawn($mycon,$member_id,$amount,$balance)
    {
      $thedate = date("Y-m-d H:i:s");
      $sql = "UPDATE `donationsreceivable` SET
              `withdrawn` = :withdrawn
              ,`balance` = :balance
              ,`thedate` = :thedate WHERE `member_id` = :member_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":member_id", $member_id, PDO::PARAM_STR);
      $myrec->bindValue(":withdrawn", $amount, PDO::PARAM_STR);
      $myrec->bindValue(":balance", $balance, PDO::PARAM_STR);
      $myrec->bindValue(":thedate", $thedate, PDO::PARAM_STR);
      

      if (!$myrec->execute()) return false;

      return true;
    }

    function donationsreceivable_updateall($mycon,$member_id,$amount,$withdrawn,$balance)
    {
      $thedate = date("Y-m-d H:i:s");
      $sql = "UPDATE `donationsreceivable` SET
              `withdrawn` = :withdrawn
              ,`balance` = :balance
              ,`amount` = :amount
              ,`thedate` = :thedate WHERE `member_id` = :member_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":member_id", $member_id, PDO::PARAM_STR);
      $myrec->bindValue(":withdrawn", $withdrawn, PDO::PARAM_STR);
      $myrec->bindValue(":balance", $balance, PDO::PARAM_STR);
      $myrec->bindValue(":amount", $amount, PDO::PARAM_STR);
      $myrec->bindValue(":thedate", $thedate, PDO::PARAM_STR);
      

      if (!$myrec->execute()) return false;

      return true;
    }

    //matching add
    function matching_add($mycon,$receive_id,$transfer_id, $receivefund_id,$transferfund_id,$amount,$accountdetail_id,$expirydate)
    {
        $thedate = date("Y-m-d H:i:s");
        $sql = "INSERT INTO `matching` SET `receive_id` = :receive_id
            ,`transfer_id` = :transfer_id
            ,`amount` = :amount
            ,`thedate` = :thedate
            ,`expirydate` = :expirydate
            ,`status` = :status
            ,`accountdetail_id` = :accountdetail_id
            ,`transferfund_id` = :transferfund_id
            ,`receivefund_id` = :receivefund_id";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":receive_id", $receive_id);
        $myrec->bindValue(":transfer_id", $transfer_id);
        $myrec->bindValue(":amount", $amount);
        $myrec->bindValue(":accountdetail_id", $accountdetail_id);
        $myrec->bindValue(":transferfund_id", $transferfund_id);
        $myrec->bindValue(":receivefund_id", $receivefund_id);
        $myrec->bindValue(":expirydate", $expirydate);
        $myrec->bindValue(":thedate", $thedate);
        $myrec->bindValue(":status", '5');// status set to 5 to show paired matched donations
        $myrec->execute();
        
        if($myrec->rowCount() < 1) return false;
        
        return $mycon->lastInsertId();
  
    }


    function donationsupdatestatus($mycon, $donation_id, $status, $matchedstatus)
    {
      $sql = "UPDATE `donations` SET
              `status` = :status
              ,`matchedstatus` = :matchedstatus WHERE `donation_id` = :donation_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":status", $status, PDO::PARAM_STR);
      $myrec->bindValue(":matchedstatus", $matchedstatus, PDO::PARAM_STR);
      $myrec->bindValue(":donation_id", $donation_id, PDO::PARAM_STR);

      if (!$myrec->execute()) return false;

      return true;
    }

    function donationsupdate_recommit($mycon, $donation_id, $recommit)
    {
      $sql = "UPDATE `donations` SET `recommit` = :recommit WHERE `donation_id` = :donation_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":recommit", $recommit, PDO::PARAM_STR);
      $myrec->bindValue(":donation_id", $donation_id, PDO::PARAM_STR);

      if (!$myrec->execute()) return false;

      return true;
    }

    function donationsupdatestatusanddate($mycon, $donation_id, $readydonation_ph, $status, $matchedstatus, $recommitment = 0)
    {
      $sql = "UPDATE `donations` SET
              `status` = :status
              ,`recommitment` = :recommitment
              ,`readydonation_ph` = :readydonation_ph
              ,`matchedstatus` = :matchedstatus WHERE `donation_id` = :donation_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":status", $status, PDO::PARAM_STR);
      $myrec->bindValue(":recommitment", $recommitment, PDO::PARAM_STR);
      $myrec->bindValue(":readydonation_ph", $readydonation_ph, PDO::PARAM_STR);
      $myrec->bindValue(":matchedstatus", $matchedstatus, PDO::PARAM_STR);
      $myrec->bindValue(":donation_id", $donation_id, PDO::PARAM_STR);

      if (!$myrec->execute()) return false;

      return true;
    }

    function update_recommitment_status($mycon, $donation_id, $recommit_old)
    {
        $sql = "UPDATE `donations` SET
              `recommitment` = :recommitment WHERE `donation_id` = :donation_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":recommitment", $recommit_old, PDO::PARAM_STR);
      $myrec->bindValue(":donation_id", $donation_id, PDO::PARAM_STR);

      if (!$myrec->execute()) return false;

      return true;
    }

    function donationsupdatestatus_withtestimony($mycon, $donation_id, $status, $matchedstatus, $testimonystatus)
    {
      $sql = "UPDATE `donations` SET
              `status` = :status
              ,`matchedstatus` = :matchedstatus
              ,`testimonialstatus` = :testimonialstatus WHERE `donation_id` = :donation_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":status", $status, PDO::PARAM_STR);
      $myrec->bindValue(":matchedstatus", $matchedstatus, PDO::PARAM_STR);
      $myrec->bindValue(":donation_id", $donation_id, PDO::PARAM_STR);
      $myrec->bindValue(":testimonialstatus", $testimonystatus, PDO::PARAM_STR);

      if (!$myrec->execute()) return false;

      return true;
    }

    //update the leftover
    function leftoverupdate($mycon, $donation_id, $leftover, $leftover_id)
    {
      $thedate = date("Y-m-d H:i:s");
      $sql = "UPDATE `donations` SET
              `leftover` = :leftover
              ,`leftover_id` = :leftover_id WHERE `donation_id` = :donation_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":leftover", $leftover, PDO::PARAM_STR);
      $myrec->bindValue(":leftover_id", $leftover_id, PDO::PARAM_STR);
      $myrec->bindValue(":donation_id", $donation_id, PDO::PARAM_STR);

      if (!$myrec->execute()) return false;

      return true;
    }

    function updateMatchingExpiryDate($mycon, $matching_id, $expirydate)
    {
      $sql = "UPDATE `matching` SET
              `expirydate` = :expirydate WHERE `matching_id` = :matching_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":expirydate", $expirydate, PDO::PARAM_STR);
      $myrec->bindValue(":matching_id", $matching_id, PDO::PARAM_STR);

      if (!$myrec->execute()) return false;

      return true;
    }

    function updateDonationMessageStatus($mycon, $donation_id, $messagestatus)
    {
      $sql = "UPDATE `donations` SET
              `messagestatus` = :messagestatus WHERE `donation_id` = :donation_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":messagestatus", $messagestatus, PDO::PARAM_STR);
      $myrec->bindValue(":donation_id", $donation_id, PDO::PARAM_STR);

      if (!$myrec->execute()) return false;

      return true;
    }

     function updateMatchingStatus($mycon, $matching_id, $status)
    {
      $sql = "UPDATE `matching` SET
              `status` = :status WHERE `matching_id` = :matching_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":status", $status, PDO::PARAM_STR);
      $myrec->bindValue(":matching_id", $matching_id, PDO::PARAM_STR);

      if (!$myrec->execute()) return false;

      return true;
    }

    function updateActivationEvidenceStatus($mycon, $amount, $member_id, $status, $message)
    {
      $sql = "UPDATE `activation-evidence` SET
              `status` = :status
              ,`message` = :message
              ,`amount` = :amount WHERE `member_id` = :member_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":status", $status, PDO::PARAM_STR);
      $myrec->bindValue(":amount", $amount, PDO::PARAM_STR);
      $myrec->bindValue(":message", $message, PDO::PARAM_STR);
      $myrec->bindValue(":member_id", $member_id, PDO::PARAM_STR);

      if (!$myrec->execute()) return false;

      return true;
    }

    function addActivationEvidenceStatus($mycon, $member_id)
    {
      $sql = "INSERT INTO `activation-evidence` SET
              `member_id` = :member_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":member_id", $member_id, PDO::PARAM_STR);

      if (!$myrec->execute()) return false;

      return true;
    }

     function members_updatestatus($mycon, $member_id, $status)
    {
      $sql = "UPDATE `members` SET
              `status` = :status WHERE `member_id` = :member_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":status", $status, PDO::PARAM_STR);
      $myrec->bindValue(":member_id", $member_id, PDO::PARAM_STR);

      if (!$myrec->execute()) return false;

      return true;
    }


    function members_updateaccountstatus($mycon, $member_id, $role)
    {
      $sql = "UPDATE `members` SET
              `role` = :role WHERE `member_id` = :member_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":role", $role, PDO::PARAM_STR);
      $myrec->bindValue(":member_id", $member_id, PDO::PARAM_STR);

      if (!$myrec->execute()) return false;

      return true;
    }

    

    function members_updatepassword($mycon, $member_id, $password)
    {
      $sql = "UPDATE `members` SET
              `password` = :password WHERE `member_id` = :member_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":password", $password, PDO::PARAM_STR);
      $myrec->bindValue(":member_id", $member_id, PDO::PARAM_STR);

      if (!$myrec->execute()) return false;

      return true;
    }



    //update the picture status
    function updatePictureStatus($mycon, $member_id, $picturestatus)
    {
      $sql = "UPDATE `members` SET
              `picturestatus` = :picturestatus WHERE `member_id` = :member_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":picturestatus", $picturestatus);
      $myrec->bindValue(":member_id", $member_id);

      if (!$myrec->execute()) return false;

      return true;
    }

    //update the donation gh
    function donation_update_gh($mycon, $donation_gh, $donation_id)
    {
      $sql = "UPDATE `donations` SET `donation_gh` = :donation_gh WHERE `donation_id` = :donation_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":donation_gh", $donation_gh, PDO::PARAM_STR);
      $myrec->bindValue(":donation_id", $donation_id, PDO::PARAM_STR);

      if(!$myrec->execute()) return false;

      return true;
    }

    //add new testimony
    function testimony_add($mycon, $letter, $donation_id, $member_id)
    {
      $thedate = date("Y-m-d H:i:s");
      $sql = "INSERT INTO `testimony` SET 
              `member_id` = :member_id
              ,`donation_id` = :donation_id
              ,`letter` = :letter
              ,`thedate` = :thedate
              ,`status` = :status";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":member_id", $member_id, PDO::PARAM_STR);
      $myrec->bindValue(":donation_id", $donation_id, PDO::PARAM_STR);
      $myrec->bindValue(":letter", $letter, PDO::PARAM_STR);
      $myrec->bindValue(":thedate", $thedate, PDO::PARAM_STR);
      $myrec->bindValue(":status", '5', PDO::PARAM_STR); //5 shows approved testimony
      $myrec->execute();

      if ($myrec->rowCount() < 1) return false;

      return $mycon->lastInsertId();
    }

    //update the testimony status in the donation 
    function donationupdate_testimony($mycon, $testimonialstatus, $testimony_id, $donation_id)
    {
      $sql = "UPDATE `donations` SET 
              `testimonialstatus` = :testimonialstatus 
              ,`testimony_id` = :testimony_id WHERE `donation_id` = :donation_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":testimonialstatus", $testimonialstatus, PDO::PARAM_STR);
      $myrec->bindValue(":testimony_id", $testimony_id, PDO::PARAM_STR);
      $myrec->bindValue(":donation_id", $donation_id, PDO::PARAM_STR);

      if(!$myrec->execute()) return false;

      return true;
    }

    //add new news
    function news_add($mycon, $title, $content, $member_id)
    {
      $thedate = date("Y-m-d H:i:s");
      $sql = "INSERT INTO `news` SET 
              `member_id` = :member_id
              ,`title` = :title
              ,`content` = :content
              ,`thedate` = :thedate";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":member_id", $member_id, PDO::PARAM_STR);
      $myrec->bindValue(":title", $title, PDO::PARAM_STR);
      $myrec->bindValue(":content", $content, PDO::PARAM_STR);
      $myrec->bindValue(":thedate", $thedate, PDO::PARAM_STR);
      $myrec->execute();

      if ($myrec->rowCount() < 1) return false;

      return $mycon->lastInsertId();
    }

    //add new news
    function news_delete($mycon, $new_id)
    {
      $sql = "DELETE FROM `news` WHERE  `new_id` = :new_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":new_id", $new_id, PDO::PARAM_STR);
      $myrec->execute();

      if ($myrec->rowCount() < 1) return false;

      return true;
    }

    //update the news section
    function news_update($mycon, $title, $content, $member_id, $new_id)
    {
      $sql = "UPDATE `news` SET 
              `title` = :title 
              ,`content` = :content
              ,`member_id` = :member_id WHERE `new_id` = :new_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":title", $title, PDO::PARAM_STR);
      $myrec->bindValue(":content", $content, PDO::PARAM_STR);
      $myrec->bindValue(":donation_id", $donation_id, PDO::PARAM_STR);

      if(!$myrec->execute()) return false;

      return true; 
    }

    //update the send email status to 1
    function matching_updateemail($mycon, $matching_id, $status)
    {
        $sql = "UPDATE `matching` SET 
              `sendemail` = :sendemail WHERE `matching_id` = :matching_id";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":sendemail", $status, PDO::PARAM_STR);
      $myrec->bindValue(":matching_id", $matching_id, PDO::PARAM_STR);

      if(!$myrec->execute()) return false;

      return true; 
    }


}

//class dataRead

class DataRead
{
    //check if username exists
    function member_getbyusername($mycon,$username)
    {
        $sql = "SELECT * FROM `members` WHERE `username` = :username LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":username", $username);
        $myrec->execute();
        
        if ($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    //check if username exists
    function member_getbyrandommatch($mycon)
    {
        $sql = "SELECT m.`member_id`, a.* FROM `members` m LEFT JOIN `accountdetails` a ON a.`member_id` = m.`member_id` WHERE `random_match` = 1 ORDER BY RAND() LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->execute();
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    //check if username exists
    function member_getbyall($mycon)
    {
        $sql = "SELECT * FROM `members`";
        $myrec = $mycon->prepare($sql);
        $myrec->execute();

        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

    //check if email already exists
    function member_getbyemail($mycon,$email)
    {
        $sql = "SELECT * FROM `members` WHERE `email` = :email LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":email", $email);
        $myrec->execute();
        
        if ($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }


    //check if phonumber already exists
    function member_getbyphonenumber($mycon,$phonenumber)
    {
        $sql = "SELECT * FROM `members` WHERE `phonenumber` = :phonenumber LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":phonenumber", $phonenumber);
        $myrec->execute();
        
        if ($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    //check if phone number already exist during member update
    function member_getbyphonenumberupdate($mycon,$member_id, $phonenumber)
    {
        $sql = "SELECT * FROM `members` WHERE `phonenumber` = :phonenumber AND `member_id` != :member_id LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":phonenumber", $phonenumber);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->execute();
        
        if ($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    //get the member_id of the referral
    function member_referral ($mycon,$referral)
    {
        $sql = "SELECT * FROM `members` WHERE `username` = :username OR `email` = :email LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":username", $referral);
        $myrec->bindValue(":email", $referral);
        $myrec->execute();
        
        if ($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    //get member by id
    function member_getbyid($mycon,$member_id)
    {
        $sql = "SELECT m.*, m.role, m.status as memberstatus, m.createdon as dateofcreation, ad.* FROM `members` m LEFT JOIN `accountdetails` ad ON ad.member_id = m.member_id WHERE m.`member_id` = :member_id LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->execute();
        
        if ($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    //get the member_id of the referral
    function member_getbyusernamepassword($mycon,$username, $password)
    {
        $sql = "SELECT * FROM `members` WHERE (`username` = :username OR `email` = :email) AND `password` = :password LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":username", $username);
        $myrec->bindValue(":email", $username);
        $myrec->bindValue(":password", $password);
        $myrec->execute();
        
        if ($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    //check if bank name, bank account number and name is in the database
    function member_bankuniqueness($mycon, $bankaccountnumber)
    {
        $sql = "SELECT * FROM `accountdetails` WHERE `bankaccountnumber` = :bankaccountnumber LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":bankaccountnumber", $bankaccountnumber);
        $myrec->execute();
        
        if ($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }
    
    //check if bank name, bank account number and name is in the database
    function member_bankuniquenessupdate($mycon, $bankaccountnumber, $member_id)
    {
        $sql = "SELECT * FROM `accountdetails` WHERE `bankaccountnumber` = :bankaccountnumber AND `member_id` != :member_id LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":bankaccountnumber", $bankaccountnumber);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->execute();
        
        if ($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    //check if bank name, bank account number and name is in the database
    function cycle_getbyid($mycon, $member_id)
    {
        $sql = "SELECT * FROM `cycle` WHERE `member_id` = :member_id LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->execute();
        
        if ($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

        //check if bank name, bank account number and name is in the database
        function member_activationfee_get($mycon)
        {
            $sql = "SELECT * FROM `activation-fee` LIMIT 1";
            $myrec = $mycon->prepare($sql);
            $myrec->execute();
            
            if ($myrec->rowCount() < 1) return false;
            
            return $myrec->fetch(PDO::FETCH_ASSOC);
        }

    
        //check if bank name, bank account number and name is in the database
        function member_monthlyfee_get($mycon)
        {
            $sql = "SELECT * FROM `monthly-fee` LIMIT 1";
            $myrec = $mycon->prepare($sql);
            $myrec->execute();
            
            if ($myrec->rowCount() < 1) return false;
            
            return $myrec->fetch(PDO::FETCH_ASSOC);
        }

     // find whether there is an account with type of 1 in the database
     function member_bankactivationunique($mycon, $type = 1)
     {
         $sql = "SELECT * FROM `accountdetails` WHERE `type` = :type LIMIT 1";
         $myrec = $mycon->prepare($sql);
         $myrec->bindValue(":type", $type);
         $myrec->execute();
         
         if ($myrec->rowCount() < 1) return false;
         
         return $myrec->fetch(PDO::FETCH_ASSOC);
     }


      // find whether there is an amount in the database
      function member_bankactivationamount($mycon)
      {
          $sql = "SELECT * FROM `activation-fee`";
          $myrec = $mycon->prepare($sql);
          $myrec->execute();
          
          if ($myrec->rowCount() < 1) return false;
          
          return $myrec->fetch(PDO::FETCH_ASSOC);
      }

        // find whether there is an amount in the database
        function member_monthlyactivationamount($mycon)
        {
            $sql = "SELECT * FROM `monthly-fee`";
            $myrec = $mycon->prepare($sql);
            $myrec->execute();
            
            if ($myrec->rowCount() < 1) return false;
            
            return $myrec->fetch(PDO::FETCH_ASSOC);
        }

        // find whether there is an amount in the database
        function getMonthlydue($mycon)
        {
            $sql = "SELECT `amount` FROM `monthly-fee` LIMIT 1";
            $myrec = $mycon->prepare($sql);
            $myrec->execute();
            
            if ($myrec->rowCount() < 1) return false;
            
            return $myrec->fetch(PDO::FETCH_ASSOC);
        }

      
    //get the bank account details 
    function bankaccountdetails($mycon, $member_id,  $type = 0)
    {
        $sql = "SELECT * FROM `accountdetails` WHERE `member_id` = :member_id AND `type` = :type";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id, PDO::PARAM_STR);
        $myrec->bindValue(":type", $type, PDO::PARAM_STR);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }
    //get the bank account details 
    function bankaccountdetails_justone($mycon, $member_id,  $type = 0)
    {
        $sql = "SELECT * FROM `accountdetails` WHERE `member_id` = :member_id AND `type` = :type ORDER BY `createdon` ASC LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id, PDO::PARAM_STR);
        $myrec->bindValue(":type", $type, PDO::PARAM_STR);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

    function bankaccountdetails_activation($mycon, $type = 0)
    {
        $sql = "SELECT * FROM `accountdetails` WHERE `type` = :type";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":type", $type, PDO::PARAM_STR);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

    //get the bank account details 
    function bankaccountdetails_one($mycon, $type = 0)
    {
        $sql = "SELECT * FROM `accountdetails` WHERE `type` = :type LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":type", $type, PDO::PARAM_STR);
        $myrec->execute();
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

     //get the activation details of the member
     function memberactivationdetails($mycon, $member_id)
     {
         $sql = "SELECT * FROM `activation-evidence` WHERE `member_id` = :member_id LIMIT 1";
         $myrec = $mycon->prepare($sql);
         $myrec->bindValue(":member_id", $member_id, PDO::PARAM_STR);
         $myrec->execute();
         
         return $myrec->fetch(PDO::FETCH_ASSOC);
     }

    //get the bank account details 
    function amountaccountdetails($mycon)
    {
        $sql = "SELECT * FROM `activation-fee`";
        $myrec = $mycon->prepare($sql);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

    //get the bank account details 
    function monthlyamountdetails($mycon)
    {
        $sql = "SELECT * FROM `monthly-fee`";
        $myrec = $mycon->prepare($sql);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

    //get the bank account details 
    function amountaccountdetails_one($mycon)
    {
        $sql = "SELECT * FROM `activation-fee` LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->execute();
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    //get the bank account details 
    function activation_evidence_upload_getall($mycon)
    {
        $sql = "SELECT a.*, m.firstname, m.lastname  FROM `activation-evidence` a LEFT JOIN `members` m ON m.member_id = a.member_id ORDER BY a.`activation_evidence_id` DESC";
        $myrec = $mycon->prepare($sql);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

    //get the bank account details 
    function bankaccountdetails_getbyid($mycon,$accountdetail_id)
    {
        $sql = "SELECT m.`firstname`, m.`lastname`, m.`username`, m.`picturestatus`, m.`phonenumber`, m.`email`, a.* FROM `accountdetails` a LEFT JOIN `members` m ON m.member_id = a.member_id WHERE `accountdetail_id` = :accountdetail_id LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":accountdetail_id", $accountdetail_id, PDO::PARAM_STR);
        $myrec->execute();
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    
    //get the bank account details 
    function bankaccountdetails_getbyidmember($mycon,$member_id, $accountdetail_id)
    {
        $sql = "SELECT m.`firstname`, m.`lastname`, m.`username`, m.`picturestatus`, m.`phonenumber`, m.`email`, a.* 
        FROM `accountdetails` a LEFT JOIN `members` m ON m.member_id = a.member_id WHERE 
        `accountdetail_id` = :accountdetail_id AND m.`member_id` = :member_id LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":accountdetail_id", $accountdetail_id, PDO::PARAM_STR);
        $myrec->bindValue(":member_id", $member_id, PDO::PARAM_STR);
        $myrec->execute();
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    //get the list of all the members referred
    function memberreferral_getbyid($mycon,$member_id)
    {
        $sql = "SELECT * FROM `members` WHERE `referral_id` = :referral_id";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":referral_id", $member_id);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

    //get active donations donation by user
    function donations_getbyidrecent($mycon,$member_id)
    {
        $sql = "SELECT d.*, m.firstname, m.lastname, m.username, m.status as memberstatus FROM `donations` d LEFT JOIN `members` m ON m.member_id = d.member_id WHERE d.`member_id` = :member_id ORDER BY d.createdon DESC LIMIT 1 ";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->execute();
        
        if ($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    
     //get active donations donation by user
    function donations_getlastphwithoutrecommitment($mycon,$member_id, $recommitment)
    {
        $sql = "SELECT d.*, m.firstname, m.lastname, m.username, m.status as memberstatus FROM `donations` d LEFT JOIN `members` m ON m.member_id = d.member_id WHERE d.`member_id` = :member_id AND d.`donation_ph` != '' AND d.`recommitment` = :recommitment ORDER BY d.createdon DESC LIMIT 1 ";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->bindValue(":recommitment", $recommitment);
        $myrec->execute();
        
        if ($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    //get active donations donation by user
    function donations_getbyidrecentph($mycon,$member_id)
    {
        $sql = "SELECT d.*, m.firstname, m.lastname, m.username, m.status as memberstatus FROM `donations` d LEFT JOIN `members` m ON m.member_id = d.member_id WHERE d.`member_id` = :member_id AND d.`donation_ph` != '' ORDER BY d.createdon DESC LIMIT 1 ";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->execute();
        
        if ($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }
    
    //get active donations donation by user
    function donations_getbyidrecentphrecent($mycon,$member_id)
    {
        $sql = "SELECT d.*, m.firstname, m.lastname, m.username, m.status as memberstatus FROM `donations` d LEFT JOIN `members` m ON m.member_id = d.member_id WHERE d.`member_id` = :member_id AND d.`donation_ph` != '' AND d.`type` != '20% Monthly Due' ORDER BY d.createdon DESC LIMIT 1 ";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->execute();
        
        if ($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }
    
    //get active donations donation by user
    function donations_getbyidrecentconfirmedph($mycon,$member_id)
    {
        $sql = "SELECT d.*, m.firstname, m.lastname, m.username, m.status as memberstatus FROM `donations` d LEFT JOIN `members` m ON m.member_id = d.member_id WHERE d.`member_id` = :member_id AND d.`donation_ph` != '' ORDER BY d.createdon DESC LIMIT 1 ";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->execute();
        
        if ($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    //get active donations donation by user
    function donations_getbyidrecentgh($mycon,$member_id)
    {
        $sql = "SELECT d.*, m.firstname, m.lastname, m.username, m.`email`, m.status as memberstatus FROM `donations` d LEFT JOIN `members` m ON m.member_id = d.member_id WHERE d.`member_id` = :member_id
        AND d.`donation_gh` != '' ORDER BY d.createdon DESC LIMIT 1 ";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->execute();
        
        if ($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }


    //get active donations donation by user
    function donations_getbyid($mycon,$member_id, $limit = " ")
    {
        $sql = "SELECT d.*, m.firstname, m.lastname, m.username, m.status as memberstatus, m.`role` FROM `donations` d LEFT JOIN `members` m ON m.member_id = d.member_id WHERE d.`member_id` = :member_id ORDER BY d.createdon DESC ".$limit;
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->execute();

        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

      //get active donations donation by user
      function donations_getbyrecommitment($mycon,$member_id, $recommitment)
      {
          $sql = "SELECT * FROM `donations` WHERE `member_id` = :member_id AND `recommitment` = :recommitment LIMIT 1";
          $myrec = $mycon->prepare($sql);
          $myrec->bindValue(":member_id", $member_id);
          $myrec->bindValue(":recommitment", $recommitment);
          $myrec->execute();
  
          if ($myrec->rowCount() < 1) return false;
        
           return $myrec->fetch(PDO::FETCH_ASSOC);
      }

    //function to get the last donation of the user if it is still pending
    function donations_getlastdonation($mycon, $member_id)
    {
        $sql = "SELECT d.*, m.firstname, m.lastname, m.username, m.status as memberstatus FROM `donations` d LEFT JOIN `members` m ON m.member_id = d.member_id WHERE d.`member_id` = :member_id ORDER BY d.createdon DESC LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->execute();

        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    //function to get the last donation of the user if it is still pending
    function donation_getlastph($mycon, $member_id)
    {
        $sql = "SELECT d.*, m.firstname, m.lastname, m.username, m.status as memberstatus 
                FROM `donations` d LEFT JOIN `members` m ON m.member_id = d.member_id 
                WHERE d.`donation_ph` != '' AND d.`member_id` = :member_id ORDER BY d.createdon DESC LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->execute();

        if ($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }
    
        //function to get the last donation of the user if it is still pending
    function donation_getlastphrecent($mycon, $member_id)
    {
        $sql = "SELECT d.*, m.firstname, m.lastname, m.username, m.status as memberstatus 
                FROM `donations` d LEFT JOIN `members` m ON m.member_id = d.member_id 
                WHERE d.`donation_ph` != '' AND d.`member_id` = :member_id AND d.`type` != '20% Monthly Due' ORDER BY d.createdon DESC LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->execute();

        if ($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }


    //get active donations donation by user
    function news_getall($mycon)
    {
        $sql = "SELECT n.*, m.firstname, m.lastname, m.username, m.status as memberstatus FROM `news` n LEFT JOIN `members` m ON m.member_id = n.member_id ORDER BY n.`thedate` DESC";
        $myrec = $mycon->prepare($sql);
        $myrec->execute();
        
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

    //get all active donations with staus of 5 and 3
    function donations_getall($mycon)
    {
        $sql = "SELECT d.*, d.`createdon` as donationcreatedon, m.firstname, m.lastname, m.username, m.email, m.referral_id, m.status as memberstatus FROM `donations` d LEFT JOIN `members` m ON m.member_id = d.member_id WHERE d.`donation_ph` != '' ORDER BY d.createdon ASC";
        $myrec = $mycon->prepare($sql);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

    //get all active donations with staus of 5 and 3
    function donations_getallwithrecommitment($mycon, $recommitment)
    {
        $sql = "SELECT d.*, d.`createdon` as donationcreatedon, m.firstname, m.lastname, m.username, m.email, m.referral_id, 
        m.status as memberstatus FROM `donations` d LEFT JOIN `members` m ON m.member_id = d.member_id 
        WHERE d.`donation_ph` != '' AND d.`recommitment` = :recommitment ORDER BY d.createdon ASC";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":recommitment", $recommitment);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }
    
        //get all active donations with staus of 5 and 3
    function donations_getallwithrecommitmentdesc($mycon, $recommitment)
    {
        $sql = "SELECT d.*, d.`createdon` as donationcreatedon, m.firstname, m.lastname, m.username, m.email, m.referral_id, 
        m.status as memberstatus FROM `donations` d LEFT JOIN `members` m ON m.member_id = d.member_id 
        WHERE d.`donation_ph` != '' AND d.`recommitment` = :recommitment ORDER BY d.createdon DESC";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":recommitment", $recommitment);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

    //get all active donations with staus of 5 and 3
    function donations_getallwithunequalrecommitment($mycon, $recommitment)
    {
        $sql = "SELECT d.*, d.`createdon` as donationcreatedon, m.`member_id` as memberid, m.firstname, m.lastname, m.username, m.email, m.referral_id, 
        m.status as memberstatus FROM `donations` d LEFT JOIN `members` m ON m.member_id = d.member_id 
        WHERE d.`donation_ph` != '' AND d.`recommitment` = :recommitment ORDER BY d.createdon ASC";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":recommitment", $recommitment);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

    //get all active donations with staus of 5 and 3
    function donationsgh_getallwithrecommitment($mycon, $status, $matchedstatus, $memberstatus, $member_id, $recommitment)
    {
        $sql = "SELECT d.*, d.`createdon` as donationcreatedon, m.firstname, m.lastname, m.username, m.email, m.referral_id, 
        m.status as memberstatus FROM `donations` d LEFT JOIN `members` m ON m.member_id = d.member_id 
        WHERE d.`status` = :status AND d.`matchedstatus` = :matchedstatus AND m.`status` = :mstatus AND d.`donation_gh` != '' AND d.`recommitment` = :recommitment AND d.`member_id` = :member_id ORDER BY d.createdon ASC";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->bindValue(":recommitment", $recommitment);
        $myrec->bindValue(":status", $status);
        $myrec->bindValue(":matchedstatus", $matchedstatus);
        $myrec->bindValue(":mstatus", $memberstatus);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }
    
    //get all active donations with staus of 5 and 3
    function donationsph_getallwithrecommitment($mycon, $status, $matchedstatus, $memberstatus, $member_id, $recommitment)
    {
        $sql = "SELECT d.*, d.`createdon` as donationcreatedon, m.firstname, m.lastname, m.username, m.email, m.referral_id, 
        m.status as memberstatus FROM `donations` d LEFT JOIN `members` m ON m.member_id = d.member_id 
        WHERE d.`status` = :status AND d.`matchedstatus` = :matchedstatus AND m.`status` = :mstatus AND d.`donation_ph` != '' AND d.`recommitment` = :recommitment AND d.`member_id` = :member_id ORDER BY d.createdon ASC";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->bindValue(":recommitment", $recommitment);
        $myrec->bindValue(":status", $status);
        $myrec->bindValue(":matchedstatus", $matchedstatus);
        $myrec->bindValue(":mstatus", $memberstatus);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }
    
    //get all active donations with staus of 5 and 3
    function donationsph_getallwithrecommitmentnumber($mycon, $status, $memberstatus, $member_id, $recommitment)
    {
        $sql = "SELECT d.*, d.`createdon` as donationcreatedon, m.firstname, m.lastname, m.username, m.email, m.referral_id, 
        m.status as memberstatus FROM `donations` d LEFT JOIN `members` m ON m.member_id = d.member_id 
        WHERE d.`status` = :status AND d.`type` != '20% Monthly Due' AND d.`donation_ph` != '' AND d.`recommitment` = :recommitment AND d.`member_id` = :member_id ORDER BY d.createdon ASC";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->bindValue(":recommitment", $recommitment);
        $myrec->bindValue(":status", $status);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }
    
    //get all active donations with staus of 5 and 3
    function donationsph_getallwithoutrecommitmentnumber($mycon, $member_id)
    {
        $sql = "SELECT d.*, d.`createdon` as donationcreatedon, m.firstname, m.lastname, m.username, m.email, m.referral_id, 
        m.status as memberstatus FROM `donations` d LEFT JOIN `members` m ON m.member_id = d.member_id 
        WHERE d.`type` != '20% Monthly Due' AND d.`type` != 'Deposit Fund' AND d.`recommitment` != 3 AND d.`donation_ph` != '' AND d.`member_id` = :member_id ORDER BY d.createdon ASC";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }


    //get all active donations with staus of 5 and 3
    function donations_getidmember($mycon, $member_id)
    {
        $sql = "SELECT d.*, dr.* FROM `donations` d INNER JOIN `donationsreceivable` dr On dr.member_id = d.member_id WHERE d.`donation_ph` != '' AND dr.`member_id` = :member_id ORDER BY d.`createdon` ASC";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }


    //get all active donations with staus of 5 and 3 with recommitment of 2
    function donations_getidmemberwithrecommitment($mycon, $member_id, $recommitment)
    {
        $sql = "SELECT d.*, dr.* FROM `donations` d INNER JOIN `donationsreceivable` dr On dr.member_id = d.member_id 
        WHERE d.`donation_ph` != '' AND dr.`member_id` = :member_id AND d.`recommitment` = :recommitment ORDER BY d.`createdon` ASC";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->bindValue(":recommitment", $recommitment);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }
    
    //get all active donations with staus of 5 and 3 with recommitment of 2 and firsttime
    function donations_getidmemberwithrecommitmentandfirsttime($mycon, $member_id, $recommitment, $firsttime)
    {
        $sql = "SELECT d.*, dr.* FROM `donations` d INNER JOIN `donationsreceivable` dr On dr.member_id = d.member_id 
        WHERE d.`donation_ph` != '' AND dr.`member_id` = :member_id AND d.`recommitment` = :recommitment AND d.`firsttime` = :firsttime ORDER BY d.`createdon` ASC";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->bindValue(":recommitment", $recommitment);
        $myrec->bindValue(":firsttime", $firsttime);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

     //get all active donations with staus of 5 and 3 with recommitment of 2
     function donations_getidmemberwithrecommitmentone($mycon, $member_id, $recommitment)
     {
         $sql = "SELECT d.*, dr.* FROM `donations` d INNER JOIN `donationsreceivable` dr On dr.member_id = d.member_id 
         WHERE d.`donation_ph` != '' AND dr.`member_id` = :member_id AND d.`recommitment` = :recommitment ORDER BY d.`createdon` ASC LIMIT 1";
         $myrec = $mycon->prepare($sql);
         $myrec->bindValue(":member_id", $member_id);
         $myrec->bindValue(":recommitment", $recommitment);
         $myrec->execute();
         
         if ($myrec->rowCount() < 1) return false;

         return $myrec->fetch(PDO::FETCH_ASSOC);
     }

     //get all active donations with staus of 5 and 3 with recommitment of 2
     function donations_getidmemberwithrecommitmentonedonation($mycon, $donation_id, $type)
     {
         $sql = "SELECT d.*, dr.* FROM `donations` d INNER JOIN `donationsreceivable` dr On dr.member_id = d.member_id 
         WHERE d.`type` = :type AND d.`donation_id` = :donation_id ORDER BY d.`createdon` ASC LIMIT 1";
         $myrec = $mycon->prepare($sql);
         $myrec->bindValue(":donation_id", $donation_id);
         $myrec->bindValue(":type", $type);
         $myrec->execute();
         
         if ($myrec->rowCount() < 1) return false;

         return $myrec->fetch(PDO::FETCH_ASSOC);
     }

    //get all active donations with staus of 5 and 3
    function donations_getidmember_limit($mycon, $member_id)
    {
        $sql = "SELECT d.*, dr.* FROM `donations` d INNER JOIN `donationsreceivable` dr On dr.member_id = d.member_id WHERE d.`donation_ph` != '' AND dr.`member_id` = :member_id ORDER BY d.`createdon` DESC LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->execute();
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }


    //get the details of the make donations
    function donations_gh_getall($mycon)
    {
        $sql = "SELECT d.*, d.`createdon` as donationcreatedon, d.status as donationstatus,m.username, m.firstname, m.lastname, m.email, m.member_id as memberid, pm.* FROM `donations` d LEFT JOIN `members` m ON m.member_id = d.member_id LEFT JOIN `accountdetails` pm ON pm.accountdetail_id = d.accountdetail_id WHERE d.donation_gh != 0";
        $myrec = $mycon->prepare($sql);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

    //get active donations donation by user
    function donation_getbyidreferral($mycon,$member_id)
    {
        $sql = "SELECT d.*, pm.*, m.firstname, m.role, m.lastname, m.username, m.status as memberstatus FROM `members` m LEFT JOIN `donations` d ON m.member_id = d.member_id LEFT JOIN `accountdetails` pm ON pm.accountdetail_id = d.accountdetail_id WHERE d.`member_id` = :member_id OR m.referral_id = d.member_id AND d.`status` != 0.00 ORDER BY d.donation_id DESC";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

    //get active donations donation by user
    function donation_getallrandom($mycon, $limit)
    {
        $sql = "SELECT d.*, d.createdon as donationcreated, pm.*, m.firstname, m.lastname, m.username, m.picturestatus, m.role, m.status as memberstatus FROM `members` m INNER JOIN `donations` d ON m.member_id = d.member_id LEFT JOIN `accountdetails` pm ON pm.accountdetail_id = d.accountdetail_id WHERE d.`status` != 0.00 AND d.`donation_ph` != '' ORDER BY RAND() LIMIT ".$limit;
        $myrec = $mycon->prepare($sql);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }


     //get the donation request by id
    function donationrequest_getbyid($mycon, $donation_id)
    {
        $sql = "SELECT * FROM `donations` WHERE `donation_id` = :donation_id AND `status` = :status LIMIT 1 ";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":donation_id", $donation_id);
        $myrec->bindValue(":status", 5);// 5 signifies active donations
        $myrec->execute();
        
        if ($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);

    }

     //get the donation request by id
     function donationreceivable_getbyid($mycon, $member_id)
     {
         $sql = "SELECT * FROM `donationsreceivable` WHERE `member_id` = :member_id LIMIT 1 ";
         $myrec = $mycon->prepare($sql);
         $myrec->bindValue(":member_id", $member_id);
         $myrec->execute();
         
         if ($myrec->rowCount() < 1) return false;
         
         return $myrec->fetch(PDO::FETCH_ASSOC);
 
     }

     //get the donation request by id
    function donationrequest_getbyidstatus($mycon, $donation_id, $status)
    {
        $sql = "SELECT * FROM `donations` WHERE `donation_id` = :donation_id AND `status` = :status LIMIT 1 ";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":donation_id", $donation_id);
        $myrec->bindValue(":status", $status);// 5 signifies active donations
        $myrec->execute();
        
        if ($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);

    }

    //get active donations donation by user
    function donation_getallrandomconfirmed($mycon, $limit)
    {
        $sql = "SELECT d.*, d.createdon as donationcreated, pm.*, m.firstname, m.lastname, m.username, m.role, m.random_match, m.status as memberstatus, m.picturestatus FROM `members` m INNER JOIN `donations` d ON m.member_id = d.member_id LEFT JOIN `accountdetails` pm ON pm.accountdetail_id = d.accountdetail_id WHERE d.`status` = 0 AND d.`donation_ph` != '' AND m.`random_match` = 0 ORDER BY RAND(".$limit.")";
        $myrec = $mycon->prepare($sql);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

    //get active donations donation by user
    function donation_getall_admin($mycon)
    {
        $sql = "SELECT d.*, d.createdon as donationcreatedon, pm.*, m.firstname, m.lastname, m.username, m.email, m.status as memberstatus, m.picturestatus FROM `members` m INNER JOIN `donations` d ON m.member_id = d.member_id LEFT JOIN `accountdetails` pm ON pm.accountdetail_id = d.accountdetail_id WHERE d.`status` = 5 AND d.`donation_ph` != '' ORDER BY d.`createdon` ASC";
        $myrec = $mycon->prepare($sql);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

    //get active donations donation by user
    function donation_getall_gh_admin($mycon)
    {
        $sql = "SELECT d.*, d.createdon as donationcreatedon, pm.*, m.firstname, m.lastname, m.username, m.email, m.status as memberstatus, m.picturestatus FROM `members` m INNER JOIN `donations` d ON m.member_id = d.member_id LEFT JOIN `accountdetails` pm ON pm.accountdetail_id = d.accountdetail_id WHERE d.`status` = 0 AND d.`donation_gh` != '' ORDER BY d.`createdon` ASC";
        $myrec = $mycon->prepare($sql);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

    //get active donations donation by user with status
    function donations_getbyidstatus($mycon,$member_id, $status, $limit = '')
    {
        $sql = "SELECT d.*, m.firstname, m.lastname, m.username, m.status as memberstatus FROM `donations` d LEFT JOIN `members` m ON m.member_id = d.member_id WHERE d.`member_id` = :member_id AND d.`status` = :status ORDER BY d.createdon DESC ".$limit;
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->bindValue(":status", $status);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

     //get the referral bonus for a particular member
    function referraldonations_getbyid($mycon,$member_id)
    {
        $sql = "SELECT d.*, d.createdon as donationcreatedon, m.referral_id, m.firstname, m.lastname, m.username, m.email, m.status as memberstatus FROM `members` m LEFT JOIN `donations` d ON m.member_id = d.member_id  WHERE m.`referral_id` = :member_id AND d.`recommitment` = 2 AND d.`donation_ph` != '' ORDER BY d.createdon DESC";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }


     //get the referral bonus for a particular member
    function referraldonations_getbyidandfirsttime($mycon,$member_id, $firsttime)
    {
        $sql = "SELECT d.*, d.createdon as donationcreatedon, m.referral_id, m.firstname, m.lastname, m.username, m.email, m.status as memberstatus FROM `members` m 
        LEFT JOIN `donations` d ON m.member_id = d.member_id  WHERE m.`referral_id` = :member_id AND d.`recommitment` = 2 AND d.`donation_ph` != '' AND d.`firsttime` = :firsttime ORDER BY d.createdon DESC";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->bindValue(":firsttime", $firsttime);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }


 //get the referral bonus for a particular member
    function referraldonations_getbyidmember($mycon,$member_id)
    {
        $sql = "SELECT d.*, m.referral_id, m.firstname, m.lastname, m.username, m.email, m.status as memberstatus FROM `members` m LEFT JOIN `donations` d ON m.member_id = d.member_id  WHERE m.`referral_id` = :member_id LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->execute();
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

      //get the referral bonus for a particular member
    function referraldonations_getall($mycon)
    {
        $sql = "SELECT d.*, m.referral_id, m.firstname, m.lastname, m.username, m.email, m.status as memberstatus FROM `members` m LEFT JOIN `donations` d ON m.member_id = d.member_id  WHERE m.`referral_id` = :member_id";
        $myrec = $mycon->prepare($sql);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

    //get active donations donation by user
    function donation_getbyid($mycon,$member_id)
    {
        $sql = "SELECT d.*, d.createdon as donationcreatedon, m.firstname, m.role, m.lastname, m.username, m.email, m.status as memberstatus FROM `members` m LEFT JOIN `donations` d ON m.member_id = d.member_id WHERE d.`member_id` = :member_id OR m.referral_id = d.member_id AND d.`status` != 0.00 ORDER BY d.donation_id DESC";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }


    //get active donations donation by user
    function donation_getallbystatus($mycon, $status)
    {
        $sql = "SELECT d.*, m.firstname, m.lastname, m.username, m.email, m.status as memberstatus FROM `members` m LEFT JOIN `donations` d ON m.member_id = d.member_id WHERE d.`member_id` = :member_id AND d.`status` = :status ORDER BY d.donation_id DESC";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":status", $status);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

    //get active donations donation by user
    function donationsreceivable_getbyidmember($mycon, $member_id)
    {
        $sql = "SELECT d.*, m.firstname, m.lastname, m.username, m.referral_id, m.email, m.status as memberstatus FROM `members` m 
        LEFT JOIN `donationsreceivable` d ON m.member_id = d.member_id 
        WHERE d.`member_id` = :member_id LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->execute();
        
        if ($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    //get active donations donation by user
    function donationsreceivable_getbyidreferral($mycon, $referral_id)
    {
        $sql = "SELECT d.*, m.firstname, m.lastname, m.username, m.referral_id, m.email, m.status as memberstatus FROM `members` m LEFT JOIN `donationsreceivable` d ON m.referral_id = d.member_id WHERE m.`referral_id` = :referral_id LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":referral_id", $referral_id);
        $myrec->execute();
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    //get active donations donation by user
    function donations_getbyidreferral($mycon, $referral_id)
    {
        $sql = "SELECT d.*, m.firstname, m.lastname, m.username, m.referral_id, m.email, m.status as memberstatus FROM `members` m LEFT JOIN `donations` d ON m.referral_id = d.member_id WHERE m.`referral_id` = :referral_id AND d.`donation_ph` != '' LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":referral_id", $referral_id);
        $myrec->execute();
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    //get active donations donation by user
    function donations_getbyidreferralwithrecommitment($mycon, $referral_id, $recommitment)
    {
        $sql = "SELECT d.*, m.firstname, m.lastname, m.username, m.referral_id, m.email, m.status as memberstatus FROM `members` m 
        LEFT JOIN `donations` d ON m.referral_id = d.member_id WHERE m.`referral_id` = :referral_id AND d.`donation_ph` != '' AND d.`recommitment` = :recommitment LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":referral_id", $referral_id);
        $myrec->bindValue(":recommitment", $recommitment);
        $myrec->execute();
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }
    
    function donations_getbyidreferralwithrecommitmentfirsttime($mycon, $referral_id, $recommitment, $firsttime)
    {
        $sql = "SELECT d.*, m.firstname, m.lastname, m.username, m.referral_id, m.email, m.status as memberstatus FROM `members` m 
        LEFT JOIN `donations` d ON m.referral_id = d.member_id WHERE m.`referral_id` = :referral_id AND d.`donation_ph` != '' AND d.`recommitment` = :recommitment AND d.`firsttime` = :firsttime LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":referral_id", $referral_id);
        $myrec->bindValue(":recommitment", $recommitment);
        $myrec->bindValue(":firsttime", $firsttime);
        $myrec->execute();
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

     //get the new donation request with status of 5 and matchedstatus of 0 and member status equal to 0
    function receiefundrequestall($mycon, $status, $matchedstatus, $memberstatus)
    {
        $sql = "SELECT d.*, m.firstname, m.lastname, m.username, m.email, m.status as memberstatus FROM `members` m LEFT JOIN `donations` d ON m.member_id = d.member_id WHERE d.`status` = :status AND d.`matchedstatus` = :matchedstatus AND d.`donation_gh` != '' AND m.`status` = :mstatus ORDER BY d.`createdon` ASC";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":status", $status);
        $myrec->bindValue(":matchedstatus", $matchedstatus);
        $myrec->bindValue(":mstatus", $memberstatus);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

     //get the new donation request with status of 5 and matchedstatus of 0 and member status equal to 0
     function providehelp_all($mycon, $status, $matchedstatus, $memberstatus, $recommit = 0)
     {
         $sql = "SELECT d.*, m.firstname, m.lastname, m.username, m.email, m.status as memberstatus FROM `members` m 
         LEFT JOIN `donations` d ON m.member_id = d.member_id 
         WHERE d.`status` = :status AND d.`matchedstatus` = :matchedstatus AND d.`donation_ph` != '' 
         AND m.`status` = :mstatus AND d.`recommitment` = :recommitment ORDER BY d.`createdon` ASC";
         $myrec = $mycon->prepare($sql);
         $myrec->bindValue(":status", $status);
         $myrec->bindValue(":matchedstatus", $matchedstatus);
         $myrec->bindValue(":recommitment", $recommit);
         $myrec->bindValue(":mstatus", $memberstatus);
         $myrec->execute();
         
         return $myrec->fetchAll(PDO::FETCH_ASSOC);
     }

     //get the new donation request with status of 5 and matchedstatus of 0 and member status equal to 0
     function providehelp_allwithoutrecommitment($mycon, $status, $matchedstatus, $memberstatus, $recommit)
     {
         $sql = "SELECT d.*, m.firstname, m.lastname, m.username, m.email, m.status as memberstatus FROM `members` m 
         LEFT JOIN `donations` d ON m.member_id = d.member_id 
         WHERE d.`status` = :status AND d.`matchedstatus` = :matchedstatus AND d.`donation_ph` != '' 
         AND m.`status` = :mstatus AND d.`recommitment` != :recommitment ORDER BY d.`createdon` ASC";
         $myrec = $mycon->prepare($sql);
         $myrec->bindValue(":status", $status);
         $myrec->bindValue(":matchedstatus", $matchedstatus);
         $myrec->bindValue(":recommitment", $recommit);
         $myrec->bindValue(":mstatus", $memberstatus);
         $myrec->execute();
         
         return $myrec->fetchAll(PDO::FETCH_ASSOC);
     }

      //get the new donation request with status of 5 and matchedstatus of 0 and member status equal to 0
      function providehelp_allfirst($mycon, $status, $matchedstatus, $memberstatus, $firsttime, $recommitment, $recommit)
      {
          $sql = "SELECT d.*, m.firstname, m.lastname, m.username, m.email, m.status as memberstatus, m.role FROM `members` m 
          LEFT JOIN `donations` d ON m.member_id = d.member_id 
          WHERE d.`status` = :status AND d.`matchedstatus` = :matchedstatus AND d.`donation_ph` != '' 
          AND m.`status` = :mstatus AND d.`firsttime` = :firsttime AND d.`recommitment` = :recommitment AND d.`recommit` = :recommit ORDER BY d.`createdon` ASC";
          $myrec = $mycon->prepare($sql);
          $myrec->bindValue(":status", $status);
          $myrec->bindValue(":matchedstatus", $matchedstatus);
          $myrec->bindValue(":firsttime", $firsttime);
          $myrec->bindValue(":mstatus", $memberstatus);
          $myrec->bindValue(":recommitment", $recommitment);
          $myrec->bindValue(":recommit", $recommit);
          $myrec->execute();
          
          return $myrec->fetchAll(PDO::FETCH_ASSOC);
      }

     //get the new donation request with status of 5 and matchedstatus of 0 and member status equal to 0
     function gethelp_all($mycon, $status, $matchedstatus, $memberstatus, $recommit)
     {
         $sql = "SELECT d.*, m.firstname, m.lastname, m.username, m.email, m.status as memberstatus FROM `members` m 
         LEFT JOIN `donations` d ON m.member_id = d.member_id 
         WHERE d.`status` = :status AND d.`matchedstatus` = :matchedstatus AND d.`donation_gh` != '' 
         AND m.`status` = :mstatus AND d.`recommitment` = :recommitment ORDER BY d.`createdon` ASC";
         $myrec = $mycon->prepare($sql);
         $myrec->bindValue(":status", $status);
         $myrec->bindValue(":matchedstatus", $matchedstatus);
         $myrec->bindValue(":recommitment", $recommit);
         $myrec->bindValue(":mstatus", $memberstatus);
         $myrec->execute();
         
         return $myrec->fetchAll(PDO::FETCH_ASSOC);
     }

     //get the new donation request with status of 5 and matchedstatus of 0 and member status equal to 0
     function checklast_gethelpfirst($mycon, $status, $matchedstatus, $memberstatus, $recommitment)
     {
         $sql = "SELECT d.*, m.firstname, m.lastname, m.username, m.email, m.status as memberstatus FROM `members` m 
         LEFT JOIN `donations` d ON m.member_id = d.member_id 
         WHERE d.`status` = :status AND d.`matchedstatus` = :matchedstatus AND d.`donation_gh` != '' 
         AND m.`status` = :mstatus AND d.`recommitment` = :recommitment ORDER BY d.`createdon` ASC";
         $myrec = $mycon->prepare($sql);
         $myrec->bindValue(":status", $status);
         $myrec->bindValue(":matchedstatus", $matchedstatus);
         $myrec->bindValue(":mstatus", $memberstatus);
         $myrec->bindValue(":recommitment", $recommitment);
         $myrec->execute();
         
         return $myrec->fetchAll(PDO::FETCH_ASSOC);
     }

     //get the new donation request with status of 5 and matchedstatus of 0 and member status equal to 0
     function checklast_gethelpfirstdesc($mycon, $status, $matchedstatus, $memberstatus, $recommitment)
     {
         $sql = "SELECT d.*, m.firstname, m.lastname, m.username, m.email, m.status as memberstatus FROM `members` m 
         LEFT JOIN `donations` d ON m.member_id = d.member_id 
         WHERE d.`status` = :status AND d.`matchedstatus` = :matchedstatus AND d.`donation_gh` != '' 
         AND m.`status` = :mstatus AND d.`recommitment` = :recommitment ORDER BY d.`createdon` DESC";
         $myrec = $mycon->prepare($sql);
         $myrec->bindValue(":status", $status);
         $myrec->bindValue(":matchedstatus", $matchedstatus);
         $myrec->bindValue(":mstatus", $memberstatus);
         $myrec->bindValue(":recommitment", $recommitment);
         $myrec->execute();
         
         return $myrec->fetchAll(PDO::FETCH_ASSOC);
     }

    //get the new donation request with status of 5 and matchedstatus of 0 and member status equal to 0
     function checklast_providehelpfirstdesc($mycon, $status, $matchedstatus, $memberstatus, $recommitment)
     {
         $sql = "SELECT d.*, m.firstname, m.lastname, m.username, m.email, m.status as memberstatus FROM `members` m 
         LEFT JOIN `donations` d ON m.member_id = d.member_id 
         WHERE d.`status` = :status AND d.`matchedstatus` = :matchedstatus AND d.`donation_ph` != '' 
         AND m.`status` = :mstatus AND d.`recommitment` = :recommitment ORDER BY d.`createdon` DESC";
         $myrec = $mycon->prepare($sql);
         $myrec->bindValue(":status", $status);
         $myrec->bindValue(":matchedstatus", $matchedstatus);
         $myrec->bindValue(":mstatus", $memberstatus);
         $myrec->bindValue(":recommitment", $recommitment);
         $myrec->execute();
         
         return $myrec->fetchAll(PDO::FETCH_ASSOC);
     }

    //get the new donation request with status of 5 and matchedstatus of 0 and member status equal to 0
    function gethelp_all_admin($mycon, $status, $matchedstatus, $memberstatus, $recommit)
    {
        $sql = "SELECT d.*, m.firstname, m.lastname, m.username, m.email, m.status as memberstatus FROM `members` m 
        LEFT JOIN `donations` d ON m.member_id = d.member_id 
        WHERE d.`status` = :status AND d.`matchedstatus` != :matchedstatus AND d.`donation_gh` != '' 
        AND m.`status` = :mstatus AND d.`recommitment` = :recommitment ORDER BY d.`createdon` DESC";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":status", $status);
        $myrec->bindValue(":matchedstatus", $matchedstatus);
        $myrec->bindValue(":recommitment", $recommit);
        $myrec->bindValue(":mstatus", $memberstatus);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

     //get the new donation request with status of 5 and matchedstatus of 0 and member status equal to 0
    function receivefundrequestall($mycon)
    {
        $sql = "SELECT d.*, m.firstname, m.lastname, m.username, m.email, m.status as memberstatus FROM `members` m LEFT JOIN `donations` d ON m.member_id = d.member_id WHERE d.`donation_gh` != '' ORDER BY d.createdon ASC";
        $myrec = $mycon->prepare($sql);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

     //get the list of all the  active donations that are at least 3 days and matched status of 0, status of 5 and memberstatus of 0
    function activetransferdonations($mycon, $status, $matchedstatus, $memberstatus, $member_id)
    {
        $sql = "SELECT d.*, m.firstname, m.lastname, m.username, m.email, m.status as memberstatus FROM `members` m LEFT JOIN `donations` d ON m.member_id = d.member_id WHERE d.`status` = :status AND d.`matchedstatus` = :matchedstatus AND d.`donation_ph` != '' AND m.`status` = :mstatus AND d.`member_id` != :member_id ORDER BY d.`createdon` ASC";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":status", $status);
        $myrec->bindValue(":matchedstatus", $matchedstatus);
        $myrec->bindValue(":mstatus", $memberstatus);
        $myrec->bindValue(":member_id", $member_id);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

     //get the list of all the  active donations that are at least 3 days and matched status of 0, status of 5 and memberstatus of 0
    function matching_transfer_getbyidmemeber($mycon, $matchedstatus, $transfer_id)
    {
        $sql = "SELECT ma.*, ma.`status` as matchingstatus, d.* FROM `donations` d RIGHT JOIN `matching` ma ON d.`donation_id` = ma.`transferfund_id` WHERE `transfer_id` = :transfer_id AND d.`matchedstatus` = :matchedstatus AND d.`donation_ph` != '' AND d.`donation_gh` = '' AND d.`donation_id` = ma.`transferfund_id` ORDER BY ma.`thedate` DESC";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":matchedstatus", $matchedstatus, PDO::PARAM_STR);
        $myrec->bindValue(":transfer_id", $transfer_id, PDO::PARAM_STR);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

     //get the list of all the  active donations that are at least 3 days and matched status of 0, status of 5 and memberstatus of 0
    function matching_transfer_getbyidmemeber_admin($mycon)
    {
        $sql = "SELECT ma.*, ma.`status` as matchingstatus, d.* FROM `donations` d RIGHT JOIN `matching` ma ON d.`donation_id` = ma.`transferfund_id` WHERE d.`donation_ph` != '' AND d.`donation_gh` = '' AND d.`donation_id` = ma.`transferfund_id` ORDER BY ma.`thedate` DESC";
        $myrec = $mycon->prepare($sql);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

     //get the list of all the  active donations that are at least 3 days and matched status of 0, status of 5 and memberstatus of 0
    function matching_receive_getbyidmemeber($mycon, $matchedstatus, $receive_id)
    {
        $sql = "SELECT ma.*, ma.`status` as matchingstatus, d.* FROM `donations` d RIGHT JOIN `matching` ma ON d.`donation_id` = ma.`receivefund_id` WHERE `receive_id` = :receive_id AND d.`matchedstatus` = :matchedstatus AND d.`donation_gh` != '' AND d.`donation_ph` = '' AND d.`donation_id` = ma.`receivefund_id` ORDER BY ma.`thedate` DESC";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":matchedstatus", $matchedstatus, PDO::PARAM_STR);
        $myrec->bindValue(":receive_id", $receive_id, PDO::PARAM_STR);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

     //get the list of all the  active donations that are at least 3 days and matched status of 0, status of 5 and memberstatus of 0
    function matching_receive_getbyidmemeber_admin($mycon)
    {
        $sql = "SELECT ma.*, ma.`status` as matchingstatus, d.* FROM `donations` d RIGHT JOIN `matching` ma ON d.`donation_id` = ma.`receivefund_id` WHERE d.`donation_gh` != '' AND d.`donation_ph` = '' AND d.`donation_id` = ma.`receivefund_id` ORDER BY ma.`thedate` DESC";
        $myrec = $mycon->prepare($sql);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }


     //get the list of all the  active donations that are at least 3 days and matched status of 0, status of 5 and memberstatus of 0
    function matching_transfer_getall($mycon, $transfer_id)
    {
        $sql = "SELECT ma.`accountdetail_id` as account_id, ma.*, m.`username`, ma.`status` as matchingstatus, d.* FROM `matching` ma LEFT JOIN `donations` d ON d.`member_id` = ma.`transfer_id` LEFT JOIN `members` m ON m.`member_id` = ma.`transfer_id` WHERE `transfer_id` = :transfer_id AND d.`donation_ph` != '' ORDER BY ma.`thedate` DESC";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":transfer_id", $transfer_id);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

     //get the list of all the  active donations that are at least 3 days and matched status of 0, status of 5 and memberstatus of 0
    function matching_receive_getall($mycon, $receive_id)
    {
        $sql = "SELECT ma.`accountdetail_id` as account_id, ma.*, m.`username`, ma.`status` as matchingstatus, d.* FROM `matching` ma LEFT JOIN `donations` d ON d.`member_id` = ma.`receive_id` LEFT JOIN `members` m ON m.`member_id` = ma.`receive_id` WHERE `receive_id` = :receive_id AND d.`donation_gh` != '' ORDER BY ma.`thedate` DESC";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":receive_id", $receive_id);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

     //get the list of all the  active donations that are at least 3 days and matched status of 0, status of 5 and memberstatus of 0
    function matching_getbyid($mycon, $matching_id)
    {
        $sql = "SELECT * FROM `matching` WHERE `matching_id` = :matching_id LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":matching_id", $matching_id);
        $myrec->execute();
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }


      //get the list of all the  active donations that are at least 3 days and matched status of 0, status of 5 and memberstatus of 0
    function matching_transfer_getbyidmatchingstatus($mycon, $matchingstatus, $transfer_id, $limit)
    {
        $sql = "SELECT  ma.`accountdetail_id` as account_id, ma.*, m.`username`, ma.`status` as matchingstatus, d.*, m.`role` FROM `matching` ma 
        LEFT JOIN `donations` d ON d.`donation_id` = ma.`transferfund_id` 
        LEFT JOIN `members` m ON m.`member_id` = ma.`transfer_id` WHERE `transfer_id` = :transfer_id AND ma.`status` = :status AND d.`donation_ph` != '' ORDER BY ma.`thedate` DESC ".$limit;
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":status", $matchingstatus);
        $myrec->bindValue(":transfer_id", $transfer_id);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }
    
    function matching_transfer_getbyidmatchingstatusall($mycon, $transfer_id, $limit)
    {
        $sql = "SELECT  ma.`accountdetail_id` as account_id, ma.*, m.`username`, ma.`status` as matchingstatus, d.*, m.`role` FROM `matching` ma 
        LEFT JOIN `donations` d ON d.`donation_id` = ma.`transferfund_id` 
        LEFT JOIN `members` m ON m.`member_id` = ma.`transfer_id` WHERE `transfer_id` = :transfer_id AND d.`donation_ph` != '' ORDER BY d.`createdon` DESC ".$limit;
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":transfer_id", $transfer_id);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }
      //get the list of all the  active donations that are at least 3 days and matched status of 0, status of 5 and memberstatus of 0
    function matching_receive_getbyidmatchingstatus($mycon, $matchingstatus, $receive_id, $limit)
    {
        $sql = "SELECT  ma.`accountdetail_id` as account_id, ma.*, m.`username`, ma.`status` as matchingstatus, d.*, m.`role` 
        FROM `matching` ma LEFT JOIN `donations` d ON d.`donation_id` = ma.`receivefund_id` 
        LEFT JOIN `members` m ON m.`member_id` = ma.`receive_id` WHERE `receive_id` = :receive_id AND ma.`status` = :status AND d.`donation_gh` != '' ORDER BY ma.`thedate` DESC ".$limit;
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":status", $matchingstatus);
        $myrec->bindValue(":receive_id", $receive_id);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }
    
    function matching_receive_getbyidmatchingstatusall($mycon, $receive_id, $limit)
    {
        $sql = "SELECT  ma.`accountdetail_id` as account_id, ma.*, m.`username`, ma.`status` as matchingstatus, d.*, m.`role` 
        FROM `matching` ma LEFT JOIN `donations` d ON d.`donation_id` = ma.`receivefund_id` 
        LEFT JOIN `members` m ON m.`member_id` = ma.`receive_id` WHERE `receive_id` = :receive_id AND d.`donation_gh` != '' ORDER BY d.`createdon` DESC ".$limit;
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":receive_id", $receive_id);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

      //get the list of all the  active donations that are at least 3 days and matched status of 0, status of 5 and memberstatus of 0
    function matching_transfer_getbyidmatching($mycon, $matching_id, $transfer_id)
    {
        $sql = "SELECT ma.*, ma.`status` as matchingstatus, d.* FROM `matching` ma LEFT JOIN `donations` d ON d.`member_id` = ma.`transfer_id` WHERE `transfer_id` != :transfer_id AND ma.`matching_id` = :matching_id AND d.`donation_ph` != '' LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":matching_id", $matching_id);
        $myrec->bindValue(":transfer_id", $transfer_id);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }
      //get the list of all the  active donations that are at least 3 days and matched status of 0, status of 5 and memberstatus of 0
    function matching_receive_getbyidmatching($mycon, $matching_id, $receive_id)
    {
        $sql = "SELECT ma.*, ma.`status` as matchingstatus, d.* FROM `matching` ma LEFT JOIN `donations` d ON d.`member_id` = ma.`receive_id` WHERE `receive_id` != :receive_id AND ma.`matching_id` = :matching_id AND d.`donation_gh` != '' LIMIT 1";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":matching_id", $matching_id);
        $myrec->bindValue(":receive_id", $receive_id);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }


     //get the list of all the  active donations that are at least 3 days and matched status of 0, status of 5 and memberstatus of 0
    function matching_getbyidmemberandreceiver($mycon, $member_id, $receiver_id)
    {
        $sql = "SELECT * FROM `matching` WHERE `receive_id` = :receive_id AND `receivefund_id` = :receivefund_id";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":receive_id", $member_id);
        $myrec->bindValue(":receivefund_id", $receiver_id);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

    function matching_getbyidstatus($mycon, $member_id)
    {
      $sql = "SELECT ma.*, ma.`status` as matchingstatus, d.* FROM `matching` ma LEFT JOIN `donations` d ON d.`member_id` = ma.`receive_id` LEFT JOIN `donations` da ON d.`member_id` = ma.`transfer_id` WHERE d.`donation_ph` != '' AND (ma.`receive_id` != :receive_id OR ma.`transfer_id` = :transfer_id) ORDER BY ma.`thedate` DESC";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":transfer_id", $member_id);
        $myrec->bindValue(":receive_id", $member_id);
        $myrec->execute();
        
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }


    function receivefundsallrandom($mycon, $limit)
    {
      $sql = "SELECT ma.*, ma.thedate as matchingcreatedon, m.firstname, m.lastname, m.username, m.email, m.picturestatus, m.role, m.random_match, m.status as memberstatus FROM `members` m LEFT JOIN `matching` ma ON ma.receive_id = m.member_id WHERE ma.`status` = 0 ORDER BY RAND(".$limit.")";
      $myrec = $mycon->prepare($sql);
      $myrec->execute();
      
      return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }



    //function to get the account details of the member 
    function accountdetails_getbyidnumber($mycon, $accountdetail_id, $bankaccountnumber)
    {
      $sql = "SELECT * FROM `accountdetails` WHERE `accountdetail_id` != $accountdetail_id AND `bankaccountnumber` != :bankaccountnumber LIMIT 1";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":accountdetail_id", $accountdetail_id, PDO::PARAM_STR);
      $myrec->bindValue(":bankaccountnumber", $bankaccountnumber, PDO::PARAM_STR);
      $myrec->execute();

      if ($myrec->rowCount() < 1) return false;

      return $myrec->fetch(PDO::FETCH_ASSOC);
    }
    

    //get donations details by id
    function donations_getbyiddonation($mycon, $donation_id)
    {
      $sql = "SELECT * FROM `donations` WHERE `donation_id` = :donation_id LIMIT 1";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":donation_id", $donation_id, PDO::PARAM_STR);
      $myrec->execute();

      if ($myrec->rowCount() < 1) return false;

      return $myrec->fetch(PDO::FETCH_ASSOC);
    }


    //function to find all leftover with an amount of the leftover
    function leftover_getall($mycon)
    {
      $sql = "SELECT * FROM `donations` WHERE `leftover` != 0 AND `leftover_id` = 1";
      $myrec = $mycon->prepare($sql);
      $myrec->execute();

      return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }


    //get the details of the news by id
    function news_getbyid($mycon, $new_id)
    {
      $sql = "SELECT * FROM `news` WHERE `new_id` = :new_id LIMIT 1";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":new_id", $new_id);
      $myrec->execute();

      return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    //get all the matching details  with active 
    function matching_getallactivestatus($mycon, $status)
    {
      $sql = "SELECT * FROM `matching` WHERE `status` = :status";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":status", $status, PDO::PARAM_STR);
      $myrec->execute();

      return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }
}
?>