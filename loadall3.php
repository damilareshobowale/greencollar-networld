<?php
require_once('admin/config.php');
require_once('admin/inc_dbfunctions.php');

$dataRead = New DataRead();
$dataWrite = New DataWrite();
$mycon = databaseConnect();
$currentuserid = getCookie("userid");

$memberdetails = $dataRead->member_getbyid($mycon, $currentuserid);

//get the list of the those merged to pay
$limit = $_POST['mylimit'];
$mergeddonations = $dataRead->matching_transfer_getbyidmatchingstatus($mycon, '3', $memberdetails['member_id'], $limit);
$mergeddonations_receive = $dataRead->matching_receive_getbyidmatchingstatus($mycon, '3', $memberdetails['member_id'],  $limit);

if ($mergeddonations_receive != null)
{
    $mergeddonations = array_merge($mergeddonations, $mergeddonations_receive);
}
function cmp($a, $b)
{
    if ($a["donation_id"] == $b["donation_id"]) {
        return 0;
    }
    return ($a["donation_id"] > $b["donation_id"]) ? -1 : 1;//sorts in decending order by ratings..
}
usort($mergeddonations,"cmp");
$mergeddonations = array_values(array_map("unserialize", array_unique(array_map("serialize", $mergeddonations))));
$count = 0;
foreach($mergeddonations as $row)
{
                                                $transferdetails = $dataRead->member_getbyid($mycon, $row['transfer_id']);
                                                $receiverdetails = $dataRead->member_getbyid($mycon, $row['receive_id']); 
                                                $receiverbankaccoutdetails = $dataRead->bankaccountdetails_getbyid($mycon, $receiverdetails['accountdetail_id']);
                                                if ($receiverdetails['username'] == 'superadmin')
                                                {
                                                    $receiverbankaccoutdetails = $dataRead->bankaccountdetails_getbyid($mycon, $row['account_id']);
                                                }
                                            ?>
                                           <div class='portlet' id='matchingfund'>
                                    <div class="portlet-heading <?php if ($row['matchingstatus'] == '5' || $row['matchingstatus'] == '3') echo 'bg-primary'; else if($row['matchingstatus'] == '4') echo "bg-danger"; else echo 'bg-success'; ?>">
                                        <h3 class='portlet-title'>
                                        <?php if ($row['matchingstatus'] == '5' && $row['recommitment'] == 0) echo "New"; else if($row['matchingstatus'] == '4') echo "Flagged"; else echo $row['type']; ?> Match
                                        </h3>
                                        <div class="portlet-widgets">
                                            <a href="javascript:void(0);" onclick="refreshMatchingFund(<?php echo $row['matching_id'] ?>)" data-toggle="reload"><i class="ion-refresh"></i></a>
                                            <span class="divider"></span>
                                            <a data-toggle="collapse" data-parent="#accordion1" href="#bg-primary<?php echo $row['matching_id'] ?>"><i class="ion-minus-round"></i></a>
                                        </div>
                                        <div class='clearfix'></div>
                                    </div>
                                    <div id='bg-primary<?php echo $row['matching_id'] ?>' class='panel-collapse collapse in'>
                                        <div class='portlet-body'>
                                            <?php 
                                            if ($row['matchingstatus'] == '5') 
                                            {
                                                ?>
                                            <p>Status: No Evidence Uploaded</p>
                                            <div class="text-center">
                                                 <div class="progress">
                                                <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 25%;">
                                                    <span class="sr-only">25% Complete</span>
                                                </div>
                                            </div>
                                            </div>
                                            <?php
                                            }
                                            else if ($row['matchingstatus'] == '3') 
                                            {
                                                ?>
                                            <p>Status: <a href='evidence/<?php echo $row['matching_id'] ?>.jpg' target='_blank' style='color: #FF0000; text-decoration: underline'> View Evidence </a> </p>
                                            <div class="text-center">
                                                 <div class="progress">
                                                <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;">
                                                    <span class="sr-only">50% Complete</span>
                                                </div>
                                            </div>
                                            </div>
                                            <?php
                                                }
                                            else if ($row['matchingstatus'] == '0')
                                            {
                                                ?>
                                            <p>Status: Confirmed</p>
                                            <div class="text-center">
                                                 <div class="progress">
                                                <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                                    <span class="sr-only">100% Complete</span>
                                                </div>
                                            </div>
                                            </div>
                                            <?php
                                            }
                                            else if ($row['matchingstatus'] == '4')
                                            {
                                                ?>
                                            <p>Status: Flagged</p>
                                            <div class="text-center">
                                                 <div class="progress">
                                                <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%;">
                                                    <span class="sr-only">10% Complete</span>
                                                </div>
                                            </div>
                                            </div>
                                            <?php
                                            } 
                                            ?>
                                            <div class="chat-conversation">
                                            <ul class="conversation-list nicescroll">
                                                <li class="clearfix">
                                                <div class="chat-avatar">
                                                    <img src="member_image/<?php if ($transferdetails['picturestatus'] != '1') echo 'avatar.png'; else echo $transferdetails['username'].'.jpg' ?>" alt="<?php echo $transferdetails['username'] ?>">
                                                    <i><?php echo formatDate($row['thedate']) ?></i>
                                                </div>
                                                <div class="conversation-text">
                                                    <div class="ctext-wrap">
                                                        <i><?php echo $transferdetails['lastname']." ".$transferdetails['firstname'] ?></i>
                                                        <?php
                                                            if ($row['matchingstatus'] == '5') 
                                                            {
                                                        ?>
                                                        <p> Your new <?php echo $row['type']; ?> has been processed. </p>
<p>
                                                           Kindly make a transfer of <?php echo $row['amount'] ?> to <?php echo $receiverdetails['lastname']." ".$receiverdetails['firstname']; ?>.
</p>
                                                        <?php
                                                            }
                                                            else if ($row['matchingstatus'] == '3') {
                                                        ?>
                                                         <p>
                                                            Your payment of <?php echo $row['amount']; ?> has been transferred to the 
                                                            bank account you provided. </p>
                                                        <p> Kindly confirm payment immediately you receive funds.</p>
                                                        <?php
                                                            }
                                                            else if ($row['matchingstatus'] == '0') {
                                                        ?>
                                                        <p>
                                                            The payment of <?php echo $row['amount']; ?> has been transferred to your bank account. </p>
                                                        <?php
                                                            }

                                                            else if ($row['matchingstatus'] == '4') {
                                                            ?>
                                                        <p>
                                                            The payment of <?php echo $row['amount']; ?> has been flagged. </p>
                                                        <?php
                                                            }
                                                            ?>
                                                        <p> 
                                                            Phone number: <?php echo $transferdetails['phonenumber'] ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </li>
                                             <li class="clearfix odd">
                                                <div class="chat-avatar">
                                                    <img src="member_image/<?php if ($receiverdetails['picturestatus'] != '1') echo 'avatar.png'; else echo $receiverdetails['username'].'.jpg' ?>" alt="<?php echo $receiverdetails['username'] ?>">
                                                    <i><?php echo formatDate($row['thedate']) ?></i>
                                                </div>
                                                <div class="conversation-text">
                                                    <div class="ctext-wrap">
                                                        <i><?php echo $receiverdetails['lastname']." ".$receiverdetails['firstname'] ?></i>
                                                        <p>
                                                           <br>
                                                          Account Name: <?php echo $receiverbankaccoutdetails['bankaccountname'] ?><br>
                                                          Bank Name: <?php echo $receiverbankaccoutdetails['bankname'] ?><br>
                                                          Bank Account Number: <?php echo $receiverbankaccoutdetails['bankaccountnumber'] ?>
                                                        </p>
                                                        <p> Phonenumber: <?php echo $receiverdetails['phonenumber'] ?> </p>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                        <?php
                                        if ($row['matchingstatus'] == '5')
                                        {
                                        ?>
                                         <div class="text-left">
                                             <h5 style="color: #FF0000; font-weight: bolder">Expire: 
                                        <?php
                                        if (strtotime($row['expirydate'] >= strtotime("Y-m-d H:i:s")) &&  $transferdetails['role'] == 1)   {
                                            echo 'Awating...';
                                        }
                                        else {
                                        $expirydate = formatDate(date("Y-m-d H:i:s", strtotime($row['expirydate']) - strtotime("Y-m-d H:i:s")), "yes");
                                                //Calculate difference
                                            $diff= strtotime($expirydate)-time();//time returns current time in seconds
                                            $days=floor($diff/(60*60*24));//seconds/minute*minutes/hour*hours/day)
                                            $hours=round(($diff-$days*60*60*24)/(60*60)); 
                                            echo $days." days. " .$hours. " hours";
                                            ?></p>
                                        
                                        </div>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                            <hr>
                                            <p class='text-right'>
                                               <button class='btn btn-danger btn-md waves-effect waves-light' data-toggle='modal' data-target=".r<?php echo $row['matching_id'] ?>">Details</button>
                                            </div>
                                        </div>
                                        <div class='modal fade r<?php echo $row['matching_id'] ?>' tabindex='-1' role='dialog' aria-labelledby='mySmallModalLabel' aria-hidden='true' style='display: none;'>
                                        <div class='modal-dialog modal-lg'>
                                            <div class='modal-content'>
                                                <div class='modal-header'>
                                                    <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
                                                    <h4 class='modal-title' id='mySmallModalLabel'>New <?php echo $row['type']; ?> Details</h4>
                                            </div>
                                                <div class='modal-body'>
                                                <p><?php echo $transferdetails['lastname']." ".$transferdetails['firstname'] ?> has transferred <?php echo $row['amount']; ?> to 
                                                        <?php echo $receiverdetails['lastname']." ".$receiverdetails['firstname'] ?>, Kindly confirm payment immediately you receive funds.</p>
                                                    <p><span style='color: #FF0000; font-weight: bold'><i class='md md-file-upload'></i> <?php echo $transferdetails['lastname']." ".$transferdetails['firstname'] ?>'s details:</span> <br />
                                                        Fullname: <?php echo $transferdetails['lastname']." ".$transferdetails['firstname'] ?><br />
                                                        Phonenumber: <?php echo $transferdetails['phonenumber'] ?><br />
                                                        Email: <?php echo $transferdetails['email'] ?><br />
                                                    </p>
                                                    <p><span style='color: #FF0000; font-weight: bold'><i class='md md-file-download'></i> <?php echo $receiverdetails['lastname']." ".$receiverdetails['firstname'] ?>'s details:</span> <br />
                                                        Fullname: <?php echo $receiverdetails['lastname']." ".$receiverdetails['firstname'] ?><br />
                                                        Account Name: <?php echo $receiverdetails['bankaccountname'] ?><br/>
                                                        Bank Name: <?php echo $receiverdetails['bankname'] ?><br />
                                                        Bank Account Number: <?php echo $receiverdetails['bankaccountnumber'] ?><br />
                                                        Phonenumber: <?php echo $receiverdetails['phonenumber'] ?><br />
                                                    </p>
                                                    </p>
                                                    <p style='color: #FF0000'>
                                                        <?php if (isset($expirydate) && $row['matchingstatus'] != '5' && $row['matchingstatus'] != '4') echo "Expiry: ".$expirydate; ?></p>
                                                  <p>Status: <?php if ($row['matchingstatus'] == '5') echo "No Evidence Uploaded"; else if ($row['matchingstatus'] == '3') echo "<a href='evidence/".$row['matching_id'].".jpg' target='_blank' style='color: #FF0000; font-decoration: underline'> view Evidence </a>";
                                                  else if ($row['matchingstatus'] =='0') echo 'Confirmed'; else echo 'Flagged' ?></p><br><br>
                                                    <?php if ($row['transfer_id'] == $currentuserid && $row['matchingstatus'] == '5')
                                                    {
                                                    ?>
                                                    <iframe name="actionframe" id="actionframe" width="1px" height="1px" frameborder="0"></iframe> 
                                                    <form action='admin/actionmanager.php' method='post' id='evidenceform' target="actionframe" enctype="multipart/form-data">
                                                        <div class="matchingpaid" id="<?php echo $row['matching_id'] ?>"></div>
                                                        <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group paid error" id="amountpaiddiv"> 
                                                                <label for="amountpaid" class="control-label">Amount Paid*</label> 
                                                                <input type="text" class="form-control" name="amountpaid" id="amountpaid" placeholder="Enter amount">
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group paid error" id="uploadevidencediv"> 
                                                                <label for="uploadevidence" class="control-label">Upload Evidence*</label> 
                                                                <input type="file" class="form-control" name="uploadevidence" id="uploadevidence">
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer"> 
                                                        <button type="submit" class="btn btn-success waves-effect waves-light">Save</button>
                                                        <input type='hidden' name='command' id='command' value='evidence_add'>
                                                        <input type='hidden' name='matching_id' id='matching_id' value="<?php echo $row['matching_id'] ?>">
                                                        <button type="button" class="btn btn-danger waves-effect" id="paidresetbutton">Reset</button> 
                                                        <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal">Close</button>
                                                    </div> 
                                                    </form>
                                                    <?php
                                                    }
                                                    else if ($row['receive_id'] == $currentuserid && $row['matchingstatus'] != '0' && $row['matchingstatus'] != '4')
                                                    {
                                                    ?>
                                                     
                                                    <div id="matchingextend<?php echo $row['matching_id'] ?>" ></div>
                                                    <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group extend error" id="extendpassworddiv<?php echo $row['matching_id'] ?>"> 
                                                                <label for="extendpassword" class="control-label">Enter your password*</label> 
                                                                <input type="password" class="form-control" name="extendpassword" id="extendpassword<?php echo $row['matching_id'] ?>" placeholder="Enter password to save changes">
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    <div class='text-center'>
                                                        <div class="button-list">
                                                            <button type="button" class="btn btn-custom btn-rounded waves-effect waves-light" id="confirmbutton<?php echo $row['matching_id'] ?>" onclick="confirm(<?php echo $row['matching_id'] ?>);"><i class='fa fa-check'></i> Confirm Payment</button>    
                                                            <?php 

                                                            if ($row['role'] == 1)
                                                            {
                                                            ?>
                                                            <button type="button" class="btn btn-primary btn-custom btn-rounded waves-effect" id="extendbutton<?php echo $row['matching_id'] ?>" onclick="extend(<?php echo $row['matching_id'] ?>);"><i class='fa fa-sign-out'></i> Extend by 6 hours</button>
                                                            <button type="button" class="btn btn-danger btn-custom btn-rounded waves-effect waves-light" id="falsepaymentbutton<?php echo $row['matching_id'] ?>" onclick="falsePayment(<?php echo $row['matching_id'] ?>);">
                                                                <i class='fa fa-times'></i> False Payment</button>
                                                            <?php
                                                            }
                                                            ?>
                                                                <button type="button" class="btn btn-info btn-custom btn-rounded waves-effect waves-light" data-dismiss="modal">Dismiss</button>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    }
                                                    else
                                                    {
                                                     ?>
                                                     <button type="button" class="btn btn-info btn-custom btn-rounded waves-effect waves-light" data-dismiss="modal">Dismiss</button>
                                                     <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
    }
                                    ?>