<?php
require_once('admin/config.php');
require_once('admin/inc_dbfunctions.php');

$dataRead = New DataRead();
$dataWrite = New DataWrite();
$mycon = databaseConnect();
$currentuserid = getCookie("userid");


$memberdetails = $dataRead->member_getbyid($mycon, $currentuserid);



$donationdetailsall = $dataRead->donation_getallrandom($mycon, '10');

$donationdetailsallconfirmed = $dataRead->donation_getallrandomconfirmed($mycon, '10');

//get the list of the those merged to pay
$limit = 'LIMIT 1';
$mergeddonations = $dataRead->matching_transfer_getbyidmatchingstatusall($mycon, $memberdetails['member_id'], $limit);
$mergeddonations_receive = $dataRead->matching_receive_getbyidmatchingstatusall($mycon,$memberdetails['member_id'],  $limit);
 
if ($mergeddonations_receive != null)
{
    $mergeddonations = array_merge($mergeddonations, $mergeddonations_receive);
}
function cmp($a, $b)
{
    if ($a["donation_id"] == $b["donation_id"]) {
        return 0;
    }
    return ($a["donation_id"] > $b["donation_id"]) ? -1 : 1;//sorts in decending order by ratings..
}
usort($mergeddonations,"cmp");
$mergeddonations = array_values(array_map("unserialize", array_unique(array_map("serialize", $mergeddonations))));

$donationdetails = $dataRead->donations_getbyid($mycon,$memberdetails['member_id'], $limit);

//get all the received funds from the database
$receivedfundsall = $dataRead->receivefundsallrandom($mycon, '1');

//get the news details from the admin
$newsdetails = $dataRead->news_getall($mycon);

//get the account details to pay to
$activationfeedetails = $dataRead->bankaccountdetails_one($mycon,'1');

//get the account amount
$amountaccountdetails = $dataRead->amountaccountdetails_one($mycon);

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Greencollar Networld Community - Earn 30% ROI after 7 days on every fund you invest.">
        <meta name="author" content="Greencollar Networld">

        <link rel="shortcut icon" href="img/logo/logowfg.ico">

        <title>Dashboard - <?php echo pageTitle(); ?></title>


        <link href="assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
        
        <link href="assets/plugins/custombox/css/custombox.css" rel="stylesheet">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="css/style.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>


    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

               <?php include_once('inc_header.php') ?>



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">Dashboard</h4>
                                <p class="text-dark page-title-alt">Welcome <?php echo getCookie("fullname") ?>!</p>
                            </div>
                        </div>

                        <?php

                        if ($newsdetails != null)
                        {
                        ?>
                    <!--    <div class="alert alert-warning" id="accountdetails_new">-->
                    <!--          You have a new message from the Admin, Greencollar Networld-->
                    <!--          <div class="button-list">-->

                    <!--     Full width modal -->
                    <!--    <button class="btn btn-custom waves-effect waves-light" onclick="document.location.href='#news_section'">Click here to go news from admin section</button>-->
                    <!--</div>-->
                    <!--      </div>-->
                      <?php
                      }
                      ?>
                        <?php

                        if ($memberdetails['picturestatus'] == '0')
                        {
                        ?>
                        <div class="alert alert-info" id="accountdetails_new">
                              You need to set up your profile picture.
                              <div class="button-list">

                        <!-- Full width modal -->
                        <button class="btn btn-custom waves-effect waves-light" onclick="document.location.href='profile.php'">Go to my personal account</button>
                    </div>
                          </div>
                      <?php
                      }
                      ?>
                         <?php
                        if ($memberdetails['memberstatus'] == '10')
                        {
                            ?>
                            <div class="card-box bg-danger text-white">
                                 <h4 class="text-white header-title m-t-0">Activation Fee Request</h4>
                                 <p class="text-white m-b-30 font-15">
                                           Dear <?php echo $memberdetails['firstname'] ?>,
   
                                       </p>
                                       <p class="text-white m-b-30 font-15">
                                          Kindly make the payment of &#x20a6;<?php echo $amountaccountdetails['amount'] ?> to the account below for account activation. 
   
                                       </p>
                                       <p class="text-white m-b-30 font-15">
                                          For complaints and clearification, contact <span style='text-decoration: underline;font-weight: bold;'>support@greencollarnetworld.com</span> or use our live chat support.
   
                                       </p>
                                       <p class="text-white m-b-30 font-15 text-uppercase">
                                          Account Name: <?php echo $activationfeedetails['bankaccountname'] ?> <br /><br />
   
                                          Account Number: <?php echo $activationfeedetails['bankaccountnumber'] ?> <br /><br />
   
                                          Bank Name: <?php echo $activationfeedetails['bankaccountname'] ?> <br /><br />
   
                                       </p>
                                       <?php
                                           if ($memberactivationdetails['message'] != null)
                                           {
                                       ?>
                                         <h3><?php echo $memberactivationdetails['message']; ?></h3>
                                       <?php
                                           }
                                           else {
                                             ?>
                                             <button class="btn btn-custom waves-effect waves-light" data-toggle="modal" data-target="#upload-pop-model">Upload POP</button>
                                             <?php
                                           }
                                               ?>
                                             
                                           <p class="text-white m-b-30 m-t-30 font-15">
                                          After verification and confirmation, you will be able to continue to use your account. 
   
                                       </p>
                                           <div id="upload-pop-model" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                           <div class="modal-dialog"> 
                                               <div class="modal-content"> 
   
                                                   <div class="modal-header"> 
                                                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                       <h4 class="modal-title">Upload POP</h4> 
                                                   </div> 
                                                   <iframe name="actionframe" id="actionframe" width="1px" height="1px" frameborder="0"></iframe> 
                                                   <form action='admin/actionmanager.php' method='post' id='evidenceform' target="actionframe" enctype="multipart/form-data">
                                                   <div class="modal-body">        
                                                   <div class="matchingpaid" id="<?php echo $row['matching_id'] ?>"></div>
                                                           <div class="row"> 
                                                           <div class="col-md-12"> 
                                                               <div class="form-group paid error" id="amountpaiddiv"> 
                                                                   <label for="amountpaid" class="control-label">Amount Paid*</label> 
                                                                   <input type="text" class="form-control" name="amountpaid" id="amountpaid" placeholder="Enter amount">
                                                               </div> 
                                                           </div>
                                                       </div>
                                                       <div class="row"> 
                                                           <div class="col-md-12"> 
                                                               <div class="form-group paid error" id="uploadevidencediv"> 
                                                                   <label for="uploadevidence" class="control-label">Upload Evidence*</label> 
                                                                   <input type="file" class="form-control" name="uploadevidence" id="uploadevidence">
                                                               </div> 
                                                           </div>
                                                       </div>
                                                       <div class="modal-footer"> 
                                                           <button type="submit" class="btn btn-custom waves-effect waves-light">Save</button>
                                                           <input type='hidden' name='command' id='command' value='activation_evidence_add'>
                                                           <button type="button" class="btn btn-danger waves-effect" id="paidresetbutton">Reset</button> 
                                                           <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal">Close</button>
                                                       </div> 
                                                   </div> 
                                                       </form>
                                               </div> 
                                           </div>
                                       </div><!-- /.modal -->
                                       </div>
                           <?php
                        }

                      else if ($memberdetails['accountdetail_id'] == '')
                      {
                      ?>
                       <div class="alert alert-info" id="accountdetails_new">
                                              You cannot transfer or receive any funds yet until you add your account details. 
                                              <div class="button-list">

                                        <!-- Full width modal -->
                                        <button class="btn btn-custom waves-effect waves-light" data-toggle="modal" data-target="#con-close-modal">Add account details</button>
                                    </div>
                                          </div>
                        <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog"> 
                                            <div class="modal-content"> 
                                                <div class="modal-header"> 
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                    <h4 class="modal-title">Add new bank account details</h4> 
                                                </div> 
                                                <div class="modal-body">
                                                    <p class="text-left text-primary is-required">* is required </p>
                                                    <div id="result"></div>
                                                    <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group error" id="namediv"> 
                                                                <label for="name" class="control-label">Account Name*</label> 
                                                                <input type="text" class="form-control" name="name" id="name" placeholder="Name"> 
                                                            </div> 
                                                        </div> 
                                                        <!-- <div class="col-md-6"> 
                                                            <div class="form-group error" id="surnamediv"> 
                                                                <label for="surname" class="control-label">Account Surname*</label> 
                                                                <input type="text" class="form-control" name="surname" id="surname" placeholder="Surname"> 
                                                            </div> 
                                                        </div> -->
                                                    </div> 
                                                    <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group error" id="banknamediv"> 
                                                                <label for="bankname" class="control-label">Bank Name*</label> 
                                                                <input type="text" class="form-control" name="bankname" id="bankname" placeholder="Bank Name"> 
                                                            </div> 
                                                        </div> 
                                                    </div> 
                                                    <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group error" id="bankaccountnumberdiv"> 
                                                                <label for="bankaccountnumber" class="control-label">Bank Account Number*</label> 
                                                                <input type="text" class="form-control" name="bankaccountnumber" id="bankaccountnumber" placeholder="Bank Account Number"> 
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group error" id="passworddiv"> 
                                                                <label for="password" class="control-label">Enter your password*</label> 
                                                                <input type="password" class="form-control" name="password" id="password" placeholder="Enter password to save changes"> 
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div> 
                                                <input type="hidden" name="type" value="0" />
                                                <div class="modal-footer"> 
                                                    <button type="button" class="btn btn-custom waves-effect waves-light" type="button" id="bankaccountsave">Save changes</button> 
                                                    <button type="button" class="btn btn-danger waves-effect" id="bankaccountreset">Reset</button> 
                                                    <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal">Close</button>
                                                </div> 
                                            </div> 
                                        </div>
                                    </div><!-- /.modal -->

                        <?php
                      }
                      else 
                      {
                      ?>
                      <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                <h4 class="header-title m-t-0" style="font-size: 22px;line-height: 23px;"><b>Deposit Fund/ Receive Payment </b></h4>
                                    <p class="text-muted m-b-30 font-17" style="font-size: 17px">
                                        Get started: To deposit funds, please click on 'Deposit Fund' Button and to Receive Payment please click on 'Receive Payment' Button.
                                    </p>
                                    
                                    <!-- sample modal content -->

                                    
                              <div id="transfer_funds" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog"> 
                                            <div class="modal-content"> 
                                                <div class="modal-header"> 
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                    <h4 class="modal-title">Create a new Deposit Fund request</h4> 
                                                </div> 
                                                <div class="modal-body">
                                                    <p class="text-left text-danger is-required">* is required </p>
                                                    <div id="transferresult"></div>

                                                    <div class="row">
                                                         <div class="col-sm-12 col-lg-12 col-md-12">
                                                                <div class="panel panel-border panel-custom">
                                                                    <div class="panel-heading">
                                                                        <h3 class="panel-title">Deposit Fund</h3>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                        <p class="header-title text-left text-capitalize" style="text-tranform: capitalize;color: #111111">
                                                                           To deposit Fund, select the amount,choose bank account then enter your password and the captcha code then click on save.
                                                                          
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group transfer error" id="amountdiv"> 
                                                                <label for="amount" class="control-label">Amount*</label> 
                                                                <select name='amount' class='form-control' id='amount' onclick='customAmount(this);'  onchange='customAmount(this);'>
                                                                    <option value=''>choose amount</option>
                                                                    <option value='5000'>5000</option>
                                                                    <option value='10000'>10000</option>
                                                                    <option value='20000'>20000</option>
                                                                    <option value='30000'>30000</option>
                                                                    <option value='50000'>50000</option>
                                                                </select>
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12"> 
                                                            <div class="form-group transfer error" id="bankaccountsdiv"> 
                                                                <label for="bankaccounts" class="control-label">Bank Account to use*</label>
                                                                <select name='bankaccounts' id='bankaccounts' class='form-control'>
                                                                    <option value=''>choose bank accounts</option>
                                                                <?php
                                                                 $bankaccountdetails = $dataRead->bankaccountdetails($mycon,$currentuserid);
                                                                foreach ($bankaccountdetails as $row)
                                                                {
                                                                ?>
                                                                <option value="<?php echo $row['accountdetail_id'] ?>"><?php echo $row['bankaccountname']." - ".$row['bankaccountnumber']. " (".$row['bankname']. ")"; ?></option>
                                                                <?php
                                                                }
                                                                ?>
                                                                </select>
                                                            </div> 
                                                        </div> 
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                                <div class="form-group transfer error" id="passworddiv">
                                                                 
                                                                <label for="password" class="control-label">Enter your password*</label> 
                                                                <input type="password" class="form-control" name="password" id="password" placeholder="Enter password to save changes"> 
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    <hr>

                                                    <div class="row"> 
                                                        <div class="col-md-12">
                                                            <div class="col-md-4">
                                                                <img src="captcha.php" height="35px" /><br>
                                                            </div><span class="visible-xs"><br></span>
                                                            <div class="col-md-8 form-group transfer error" id="captchadiv">
                                                                <input type="text" class="form-control" name="captcha" id="captcha" placeholder="Enter Captcha code*"> 
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div> 
                                                <div class="modal-footer"> 
                                                    <button type="button" class="btn btn-custom waves-effect waves-light" id="transfersave">Save</button> 
                                                    <button type="button" class="btn btn-danger waves-effect" id="transfersavereset">Reset</button> 
                                                    <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal">Close</button>
                                                </div> 
                                            </div> 
                                        </div>
                                    </div><!-- /.modal -->

                                    <div id="recommit_funds" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog"> 
                                            <div class="modal-content"> 
                                                <div class="modal-header"> 
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                    <h4 class="modal-title">Create a new Recomitment request</h4> 
                                                </div> 
                                                <div class="modal-body">
                                                    <p class="text-left text-danger is-required">* is required </p>
                                                    <div id="rtransferresult"></div>

                                                    <div class="row">
                                                         <div class="col-sm-12 col-lg-12 col-md-12">
                                                                <div class="panel panel-border panel-custom">
                                                                    <div class="panel-heading">
                                                                        <h3 class="panel-title">Deposit Fund</h3>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                        <p class="header-title text-left text-capitalize" style="text-tranform: capitalize;color: #111111">
                                                                           To pay for Deposit, select the amount, choose bank account, enter your password and the captcha code then click save.
                                                                          
                                                                        </p>
                                                                        <p class="header-title text-left text-capitalize" style="text-tranform: capitalize;color: #111111">
                                                                           Kindly choose a higher amount if you wish to move up to a higher package.
                                                                          
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group recommit error" id="ramountdiv"> 
                                                                <label for="ramount" class="control-label">Amount*</label> 
                                                                <select name='ramount' class='form-control' id='ramount' onclick='customAmount(this);'  onchange='customAmount(this);'>
                                                                    <option value=''>choose amount</option>
                                                                    <option value='5000'>5000</option>
                                                                    <option value='10000'>10000</option>
                                                                    <option value='20000'>20000</option>
                                                                    <option value='30000'>30000</option>
                                                                    <option value='50000'>50000</option>
                                                                </select>
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12"> 
                                                            <div class="form-group recommit error" id="rbankaccountsdiv"> 
                                                                <label for="rbankaccounts" class="control-label">Bank Account to use*</label>
                                                                <select name='rbankaccounts' id='rbankaccounts' class='form-control'>
                                                                    <option value=''>choose bank accounts</option>
                                                                <?php
                                                                 $bankaccountdetails = $dataRead->bankaccountdetails($mycon,$currentuserid);
                                                                foreach ($bankaccountdetails as $row)
                                                                {
                                                                ?>
                                                                <option value="<?php echo $row['accountdetail_id'] ?>"><?php echo $row['bankaccountname']." - ".$row['bankaccountnumber']. " (".$row['bankname']. ")"; ?></option>
                                                                <?php
                                                                }
                                                                ?>
                                                                </select>
                                                            </div> 
                                                        </div> 
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                                <div class="form-group recommit error" id="rpassworddiv">
                                                                 
                                                                <label for="rpassword" class="control-label">Enter your password*</label> 
                                                                <input type="password" class="form-control" name="rpassword" id="rpassword" placeholder="Enter password to save changes"> 
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    <hr>

                                                    <div class="row"> 
                                                        <div class="col-md-12">
                                                            <div class="col-md-4">
                                                                <img src="captcha.php" height="35px" /><br>
                                                            </div><span class="visible-xs"><br></span>
                                                            <div class="col-md-8 form-group recommit error" id="rcaptchadiv">
                                                                <input type="text" class="form-control" name="rcaptcha" id="rcaptcha" placeholder="Enter Captcha code*"> 
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div> 
                                                <div class="modal-footer"> 
                                                    <button type="button" class="btn btn-custom waves-effect waves-light" id="recommitmentsave">Save</button> 
                                                    <button type="button" class="btn btn-danger waves-effect" id="transfersavereset">Reset</button> 
                                                    <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal">Close</button>
                                                </div> 
                                            </div> 
                                        </div>
                                    </div><!-- /.modal -->

                                    <div id="receive_funds" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog"> 
                                            <div class="modal-content"> 
                                                <div class="modal-header"> 
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                    <h4 class="modal-title">Create a new Receive Payment request</h4> 
                                                </div> 
                                                <div class="modal-body">
                                                    <p class="text-left text-primary is-required">* is required </p>
                                                    <div id="receiveresult"></div>

                                                    <div class="row">
                                                         <div class="col-sm-12 col-lg-12 col-md-12">
                                                                <div class="panel panel-border panel-danger">
                                                                    <div class="panel-heading">
                                                                        <h3 class="panel-title text-danger">Receive Payment</h3>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                        <p class="header-title text-left" style="text-tranform: capitalize;color: #111111">
                                                                        To receive Payment, enter the amount you want to receive,choose bank account, enter your password and the captcha code then click save.<br />
                                                       </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group receive error" id="amountreceivediv"> 
                                                                <label for="amountreceive" class="control-label">Amount*</label> 
                                                                <input type="text" class="form-control" name="amountreceive" id="amountreceive" placeholder="Enter amount you want to recieve"> 
                                                                <br>
                                                            </div> 
                                                        </div>
                                                    </div> 
                                                    <div class="row">
                                                        <div class="col-md-12"> 
                                                            <div class="form-group transfer error" id="bankaccountsreceivediv"> 
                                                                <label for="bankaccountsreceive" class="control-label">Bank Account to use*</label>
                                                                <select name='bankaccountsreceive' id='bankaccountsreceive' class='form-control'>
                                                                    <option value=''>choose bank accounts</option>
                                                                <?php
                                                                 $bankaccountdetails = $dataRead->bankaccountdetails($mycon,$currentuserid);
                                                                foreach ($bankaccountdetails as $row)
                                                                {
                                                                ?>
                                                                <option value="<?php echo $row['accountdetail_id'] ?>"><?php echo $row['bankaccountname']." - ".$row['bankaccountnumber']. " (".$row['bankname']. ")"; ?></option>
                                                                <?php
                                                                }
                                                                ?>
                                                                </select>
                                                            </div> 
                                                        </div> 
                                                    </div>
                                                   
                                                    <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group receive error" id="passwordreceivediv"> 
                                                                <label for="passwordreceive" class="control-label">Enter your password*</label> 
                                                                <input type="password" class="form-control" name="passwordreceive" id="passwordreceive" placeholder="Enter password to save changes"> 
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    <hr>


                                                    <div class="row"> 
                                                        <div class="col-md-12">
                                                            <div class="col-md-4">
                                                                <img src="captcha.php" height="35px" /><br>
                                                            </div><span class="visible-xs"><br></span>
                                                            <div class="col-md-8 form-group receive error" id="captchareceivediv">
                                                                <input type="text" class="form-control" name="captchareceive" id="captchareceive" placeholder="Enter Captcha code*"> 
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div> 
                                                <div class="modal-footer"> 
                                                    <button type="button" class="btn btn-custom waves-effect waves-light" id="receivesave">Save</button> 
                                                    <button type="button" class="btn btn-danger waves-effect" id="receivesavereset">Reset</button> 
                                                    <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal">Close</button>
                                                </div> 
                                            </div> 
                                        </div>
                                    </div><!-- /.modal -->

                                    <div class="button-list text-center">
                                        <!-- Custom width modal -->
                                        
                                            <button class="btn btn-custom waves-effect waves-light" data-toggle="modal" data-target="#transfer_funds" style='height: 90px; font-size: 35px;font-weight:bold;'>Deposit Fund</button>
                                            <span class='visible-xs'><br><br></span>
                                            <button class="btn btn-danger waves-effect waves-light" data-toggle="modal" data-target="#receive_funds" style='height: 90px; font-size: 35px;font-weight:bold;'>Receive Payment</button>
                                    </div>
                                    <div class='row' style='padding-top: 40px'>
                                       <div id="fundresult"></div>
                                    <div class="col-sm-8 col-lg-8 col-md-8">
                                    <div class="panel panel-success panel-border">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">REQUEST PROCESSOR</h3>
                                        </div>
                                         <div class="form-group ">
                                        <div class='row'>
                                            <div class="col-xs-9 col-md-9 matching error" id="sortmatchingdiv">
                                            <select name="sortmatching" id="sortmatching" class="form-control">
                                                   <option value="5">Processed </option>
                                                   <option value="3">Pending</option>
                                                   <option value="0">Confirmed</option>
                                            </select>
                                            </div>
                                            <input type="hidden" name="mytotalvalue" id="mytotalvalue" value="LIMIT 1"> 
                                            <div class="col-md-3 col-xs-3 error">
                                                <button class="btn btn-primary btn-md waves-effect waves-light"  id="sortmatchingbutton" type="button">
                                                Sort!
                                                </button>
                                            </div>
                                        </div> 
                                    </div>
                                        <div class="panel-body" id='matching_id'>
                                            <?php
                                            $count = 0;
                                            $count = 0;
foreach($mergeddonations as $row)
                                            {
                                                if ($count < 1) {
                                                $transferdetails = $dataRead->member_getbyid($mycon, $row['transfer_id']);
                                                $receiverdetails = $dataRead->member_getbyid($mycon, $row['receive_id']); 
                                                $receiverbankaccoutdetails = $dataRead->bankaccountdetails_getbyid($mycon, $receiverdetails['accountdetail_id']);
                                                if ($receiverdetails['username'] == 'superadmin')
                                                {
                                                    $receiverbankaccoutdetails = $dataRead->bankaccountdetails_getbyid($mycon, $row['account_id']);
                                                }
                                            ?>

                                            <div class='portlet' id='matchingfund'>
                                    <div class="portlet-heading <?php if ($row['matchingstatus'] == '5' || $row['matchingstatus'] == '3') echo 'bg-primary'; else if($row['matchingstatus'] == '4') echo "bg-danger"; else echo 'bg-success'; ?>">
                                        <h3 class='portlet-title'>
                                           <?php if ($row['matchingstatus'] == '5' && $row['recommitment'] == 0) echo "New ".$row['type'];else if($row['matchingstatus'] == '4') echo "Flagged"; else echo $row['type']; ?> Match
                                        </h3>
                                        <div class="portlet-widgets">
                                            <a href="javascript:void(0);" onclick="refreshMatchingFund(<?php echo $row['matching_id'] ?>)" data-toggle="reload"><i class="ion-refresh"></i></a>
                                            <span class="divider"></span>
                                            <a data-toggle="collapse" data-parent="#accordion1" href="#bg-primary<?php echo $row['matching_id'] ?>"><i class="ion-minus-round"></i></a>
                                        </div>
                                        <div class='clearfix'></div>
                                    </div>
                                    <div id='bg-primary<?php echo $row['matching_id'] ?>' class='panel-collapse collapse in'>
                                        <div class='portlet-body'>
                                            <?php 
                                            if ($row['matchingstatus'] == '5') 
                                            {
                                                ?>
                                            <p>Status: No Evidence Uploaded</p>
                                            <div class="text-center">
                                                 <div class="progress">
                                                <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 25%;">
                                                    <span class="sr-only">25% Complete</span>
                                                </div>
                                            </div>
                                            </div>
                                            <?php
                                            }
                                            else if ($row['matchingstatus'] == '3') 
                                            {
                                                ?>
                                            <p>Status: <a href='evidence/<?php echo $row['matching_id'] ?>.jpg' target='_blank' style='color: #FF0000; text-decoration: underline'> View Evidence </a> </p>
                                            <div class="text-center">
                                                 <div class="progress">
                                                <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;">
                                                    <span class="sr-only">50% Complete</span>
                                                </div>
                                            </div>
                                            </div>
                                            <?php
                                                }
                                            else if ($row['matchingstatus'] == '0')
                                            {
                                                ?>
                                            <p>Status: Confirmed</p>
                                            <div class="text-center">
                                                 <div class="progress">
                                                <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                                    <span class="sr-only">100% Complete</span>
                                                </div>
                                            </div>
                                            </div>
                                            <?php
                                            }
                                            else if ($row['matchingstatus'] == '4')
                                            {
                                                ?>
                                            <p>Status: Flagged</p>
                                            <div class="text-center">
                                                 <div class="progress">
                                                <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%;">
                                                    <span class="sr-only">10% Complete</span>
                                                </div>
                                            </div>
                                            </div>
                                            <?php
                                            } 
                                            ?>
                                            <div class="chat-conversation">
                                            <ul class="conversation-list nicescroll">
                                                <li class="clearfix">
                                                <div class="chat-avatar">
                                                    <img src="member_image/<?php if ($transferdetails['picturestatus'] != '1') echo 'avatar.png'; else echo $transferdetails['username'].'.jpg' ?>" alt="<?php echo $transferdetails['username'] ?>">
                                                    <i><?php echo formatDate($row['thedate']) ?></i>
                                                </div>
                                                <div class="conversation-text">
                                                    <div class="ctext-wrap">
                                                        <i><?php echo $transferdetails['lastname']." ".$transferdetails['firstname'] ?></i>
                                                        <?php
                                                            if ($row['matchingstatus'] == '5') 
                                                            {
                                                        ?>
                                                        
                                                        <p> Your new <?php echo $row['type']; ?> has been processed. </p>
<p>
                                                           Kindly make a transfer of <?php echo $row['amount'] ?> to <?php echo $receiverdetails['lastname']." ".$receiverdetails['firstname']; ?> and upload proof of payment. Thanks.
</p>
                                                        <?php
                                                            }
                                                            else if ($row['matchingstatus'] == '3') {
                                                        ?>
                                                         <p>
                                                            Your payment of <?php echo $row['amount']; ?> has been transferred to the 
                                                            bank account you provided. </p>
                                                        <p> Kindly confirm payment immediately you receive funds.</p>
                                                        <?php
                                                            }
                                                            else if ($row['matchingstatus'] == '0') {
                                                        ?>
                                                        <p>
                                                            The payment of <?php echo $row['amount']; ?> has been transferred to your bank account. </p>
                                                        
                                                        <?php
                                                            }

                                                            else if ($row['matchingstatus'] == '4') {
                                                            ?>
                                                        <p>
                                                            The payment of <?php echo $row['amount']; ?> has been flagged. </p>
                                                        <?php
                                                            }
                                                            ?>
                                                        <p> 
                                                            Phone number: <?php echo $transferdetails['phonenumber'] ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </li>
                                             <li class="clearfix odd">
                                                <div class="chat-avatar">
                                                    <img src="member_image/<?php if ($receiverdetails['picturestatus'] != '1') echo 'avatar.png'; else echo $receiverdetails['username'].'.jpg' ?>" alt="<?php echo $receiverdetails['username'] ?>">
                                                    <i><?php echo formatDate($row['thedate']) ?></i>
                                                </div>
                                                <div class="conversation-text">
                                                    <div class="ctext-wrap">
                                                        <i><?php echo $receiverdetails['lastname']." ".$receiverdetails['firstname'] ?></i>
                                                        <p>
                                                           <br>
                                                          Account Name: <?php echo $receiverbankaccoutdetails['bankaccountname'] ?><br>
                                                          Bank Name: <?php echo $receiverbankaccoutdetails['bankname'] ?><br>
                                                          Bank Account Number: <?php echo $receiverbankaccoutdetails['bankaccountnumber'] ?>
                                                        </p>
                                                        <p> Phonenumber: <?php echo $receiverdetails['phonenumber'] ?> </p>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                        <?php
                                        if ($row['matchingstatus'] == '5')
                                        {
                                        ?>
                                        <div class="text-left">
                                             <p style="color: #FF0000; font-weight: bolder">Expire: 
                                        <?php
                                        if (strtotime($row['expirydate']) <= strtotime(date("Y-m-d H:i:s")) &&  $transferdetails['role'] == 1)   {
                                            echo 'Awating...';
                                        }
                                        else {
                                        $expirydate = formatDate(date("Y-m-d H:i:s", strtotime($row['expirydate']) - strtotime("Y-m-d H:i:s")), "yes");
                                                //Calculate difference
                                            $diff= strtotime($expirydate)-time();//time returns current time in seconds
                                            $days=floor($diff/(60*60*24));//seconds/minute*minutes/hour*hours/day)
                                            $hours=round(($diff-$days*60*60*24)/(60*60)); 
                                            echo $days." days. " .$hours. " hours";
                                            ?></p>
                                        
                                        </div>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                            <hr>
                                            <p class='text-right'>
                                               <button class='btn btn-danger btn-md waves-effect waves-light' data-toggle='modal' data-target=".r<?php echo ++$count ?>">Details</button>
                                            </div>
                                        </div>
                                        <div class="modal fade r<?php echo $count ?>" tabindex='-1' role='dialog' aria-labelledby='mySmallModalLabel' aria-hidden='true' style='display: none;'>
                                        <div class='modal-dialog modal-lg'>
                                            <div class='modal-content'>
                                                <div class='modal-header'>
                                                    <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
                                                    <h4 class='modal-title' id='mySmallModalLabel'>New <?php echo $row['type']; ?> Details</h4>
                                            </div>
                                                <div class='modal-body'>
                                                <?php
                                                    if ($row['matchingstatus'] == '5') 
                                                    {
                                                ?>
                                                 <p> Your new <?php echo $row['type']; ?> has been processed. </p>
                                                    <p><?php echo $transferdetails['lastname']." ".$transferdetails['firstname'] ?>, kindly make a transfer of
                                                <?php echo $row['amount'] ?> to <?php echo $receiverdetails['lastname']." ".$receiverdetails['firstname'] ?> and upload proof of payment. Thanks. </p>
                                                <?php
                                                    }
                                                    else if ($row['matchingstatus'] == '3') {
                                                ?>

                                                <p>
                                                <?php echo $transferdetails['lastname']." ".$transferdetails['firstname'] ?> has transferred <?php echo $row['amount']; ?> to 
                                                <?php echo $receiverdetails['lastname']." ".$receiverdetails['firstname'] ?>, Kindly confirm payment immediately you receive funds.</p>
                                                <?php 
                                                    } 
                                                else if ($row['matchingstatus'] == '3') {
                                                ?>

                                                <p>
                                                <p> The payment of <?php echo $row['amount']; ?> has been confirmed. </p>
                                                <?php 
                                                    }
                                                    else if ($row['matchingstatus'] == '4') {
                                                ?>
                                                <p>
                                                    The payment of <?php echo $row['amount']; ?> has been flagged. </p>
                                                    <?php
                                                        }
                                                    ?>
                                                    <p><span style='color: #FF0000; font-weight: bold'><i class='md md-file-upload'></i> <?php echo $transferdetails['lastname']." ".$transferdetails['firstname'] ?>'s details:</span> <br />
                                                        Full Name: <?php echo $transferdetails['lastname']." ".$transferdetails['firstname'] ?><br />
                                                        Phone Number: <?php echo $transferdetails['phonenumber'] ?><br />
                                                        Email: <?php echo $transferdetails['email'] ?><br />
                                                    </p>
                                                    <p><span style='color: #FF0000; font-weight: bold'><i class='md md-file-download'></i> <?php echo $receiverdetails['lastname']." ".$receiverdetails['firstname'] ?>'s details:</span> <br />
                                                        Account Name: <?php echo $receiverbankaccoutdetails['bankaccountname'] ?><br/>
                                                        Bank Name: <?php echo $receiverbankaccoutdetails['bankname'] ?><br />
                                                        Bank Account Number: <?php echo $receiverbankaccoutdetails['bankaccountnumber'] ?><br />
                                                        Phone Number: <?php echo $receiverdetails['phonenumber'] ?><br />
                                                    </p>
                                                    </p>
                                                    <p style='color: #FF0000'>
                                                        <?php if (isset($expirydate) && $row['matchingstatus'] != '5' && $row['matchingstatus'] != '4') echo "Expiry: ".$expirydate; ?></p>
                                                  <p>Status: <?php if ($row['matchingstatus'] == '5') echo "No Evidence Uploaded"; else if ($row['matchingstatus'] == '3') echo "<a href='evidence/".$row['matching_id'].".jpg' target='_blank' style='color: #FF0000; font-decoration: underline'> view Evidence </a>";
                                                  else if ($row['matchingstatus'] =='0') echo 'Confirmed'; else echo 'Flagged' ?></p><br><br>
                                                    <?php if ($row['transfer_id'] == $currentuserid && $row['matchingstatus'] == '5')
                                                    {
                                                    ?>
                                                    <iframe name="actionframe" id="actionframe" width="1px" height="1px" frameborder="0"></iframe> 
                                                    <form action='admin/actionmanager.php' method='post' id='evidenceform' target="actionframe" enctype="multipart/form-data">
                                                        <div class="matchingpaid" id="<?php echo $row['matching_id'] ?>"></div>
                                                        <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group paid error" id="amountpaiddiv"> 
                                                                <label for="amountpaid" class="control-label">Amount Paid*</label> 
                                                                <input type="text" class="form-control" name="amountpaid" id="amountpaid" placeholder="Enter amount">
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group paid error" id="uploadevidencediv"> 
                                                                <label for="uploadevidence" class="control-label">Upload Evidence*</label> 
                                                                <input type="file" class="form-control" name="uploadevidence" id="uploadevidence">
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer"> 
                                                        <button type="submit" class="btn btn-custom waves-effect waves-light">Save</button>
                                                        <input type='hidden' name='command' id='command' value='evidence_add'>
                                                        <input type='hidden' name='matching_id' id='matching_id' value="<?php echo $row['matching_id'] ?>">
                                                        <button type="button" class="btn btn-danger waves-effect" id="paidresetbutton">Reset</button> 
                                                        <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal">Close</button>
                                                    </div> 
                                                    </form>
                                                    <?php
                                                    }
                                                    else if ($row['receive_id'] == $currentuserid && ($row['matchingstatus'] != '0' || $row['matchingstatus'] != '4'))
                                                    {
                                                    ?>
                                                     
                                                    <div id="matchingextend<?php echo $row['matching_id'] ?>" ></div>
                                                    <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group extend error" id="extendpassworddiv<?php echo $row['matching_id'] ?>"> 
                                                                <label for="extendpassword" class="control-label">Enter your password*</label> 
                                                                <input type="password" class="form-control" name="extendpassword" id="extendpassword<?php echo $row['matching_id'] ?>" placeholder="Enter password to save changes">
                                                                <input type="hidden" class="form-control" name="command" id="command<?php echo $row['matching_id'] ?>" value="<?php echo $row['matching_id'] ?>">
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    <div class='text-center'>
                                                        <div class="button-list">
                                                            <button type="button" class="btn btn-success btn-rounded waves-effect waves-light text-white" style="color: #ffffff" id="confirmbutton<?php echo $row['matching_id'] ?>" onclick="confirm(<?php echo $row['matching_id'] ?>);"><i class='fa fa-check'></i> Confirm Payment</button>    
                                                            <?php 

                                                            if ($row['role'] == 1)
                                                            {
                                                            ?>
                                                            <button type="button" class="btn btn-primary btn-rounded waves-effect" id="extendbutton<?php echo $row['matching_id'] ?>" onclick="extend(<?php echo $row['matching_id'] ?>);"><i class='fa fa-sign-out'></i> Extend by 6 hours</button>
                                                            <button type="button" class="btn btn-danger btn-rounded waves-effect waves-light" id="falsepaymentbutton<?php echo $row['matching_id'] ?>" onclick="falsePayment(<?php echo $row['matching_id'] ?>);">
                                                                <i class='fa fa-times'></i> False Payment</button>
                                                            <?php

                                                            }
                                                            ?>
                                                            <button type="button" class="btn btn-info btn-rounded waves-effect waves-light" data-dismiss="modal">Dismiss</button>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    }
                                                    else
                                                    {
                                                     ?>
                                                     <button type="button" class="btn btn-info btn-custom btn-rounded waves-effect waves-light" data-dismiss="modal">Dismiss</button>
                                                     <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                ++$count;
                                                }
                                }
                                    ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-lg-4 col-md-4">
                                    <div class="panel panel-success panel-border">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">REQUEST STATUS</h3>
                                        </div>          
                                    <div class="form-group ">
                                        <div class='row'>
                                            <div class="col-xs-8 col-md-8 sort error" id="sortfunddiv">
                                            <select name="sortfund" id="sortfund" class="form-control">
                                                   <option value="5">Pending</option>
                                                   <option value="3">Processed </option>
                                                   <option value="0">Confirmed </option>
                                            </select>
                                            </div>
                                            <input type="hidden" name="fundvalue" id="fundvalue" value="LIMIT 1"> 
                                            <div class="col-md-4 col-xs-4 error">
                                                <button class="btn btn-primary btn-md waves-effect waves-light"  id="sortfundbutton" type="button">
                                                Sort!
                                                </button>
                                            </div>
                                        </div> 
                                    </div>
                                        <div class="panel-body"  id='order_id'>
                                            <?php
                                            $count = 0;
                                            foreach($donationdetails as $row)
                                            {
                                                if ($count == 0)
                                                {
                                            ?>
                                                <div class='portlet' id='transferfund<?php echo $row['donation_id'] ?>'>
                                                    <?php 
                                                        if ($row['status'] == '5')
                                                    {
                                                    ?>
                                    <div class='portlet-heading bg-primary'>
                                        <h3 class='portlet-title'>
                                           New <?php echo $row['type']; ?>
                                        </h3>
                                        <div class="portlet-widgets">
                                            <a href="javascript:void(0);" onclick="refreshTransferFund(<?php echo $row['donation_id'] ?>);" data-toggle="reload"><i class="ion-refresh"></i></a>
                                            <span class="divider"></span>
                                            <a data-toggle="collapse" data-parent="#accordion1" href="#bg-primary<?php echo $row['donation_id'] ?>"><i class="ion-minus-round"></i></a>
                                            <span class="divider"></span>
                                            <a href="javascript:void(0);" onclick="if (alert('Are you sure you want to delete this?')) deleteTransferFund(<?php echo $row['donation_id'] ?>);"><i class="ion-close-round"></i></a>
                                        </div>
                                        <div class='clearfix'></div>
                                    </div>
                                    <div id='bg-primary<?php echo $row['donation_id'] ?>' class='panel-collapse collapse in'>
                                        <div class='portlet-body'>
                                            <p> You created a new <?php echo $row['type']; ?> request order.</p>
                                            <p> Amount: <?php if ($row['donation_ph'] != '') echo $row['donation_ph']; else echo $row['donation_gh']; ?></p>
                                            <hr>
                                            <p> Status: Pending</p>
                                            <p class='text-right'>
                                               <button class='btn btn-danger btn-xs waves-effect waves-light' data-toggle='modal' data-target=".<?php echo $row['donation_id'] ?>">Details</button>
                                            </div>
                                        </div>
                                        <div class='modal fade <?php echo $row['donation_id'] ?>' tabindex='-1' role='dialog' aria-labelledby='mySmallModalLabel' aria-hidden='true' style='display: none;'>
                                        <div class='modal-dialog modal-sm'>
                                            <div class='modal-content'>
                                                <div class='modal-header'>
                                                    <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
                                                    <h4 class='modal-title' id='mySmallModalLabel'>New <?php echo $row['type']; ?> details</h4>
                                            </div>
                                                <div class='modal-body'>
                                                  <p>You have created a new <?php echo $row['type']; ?> request order</p>
                                                  <p>Amount: <?php if ($row['donation_ph'] != '') echo $row['donation_ph']; else echo $row['donation_gh']; ?></p>
                                                  <?php 

                                                  if ($row['donation_ph'] != '')
                                                  {
                                                      ?>
                                                    
                                                  <p>Kindly pay into the account details on your dashboard and upload proof of payment.</p>
                                                  <?php
                                                  }
                                                  ?>
                                                  <p>Status: Pending</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    else if ($row['status'] == '3')
                                    {
                                    ?>
                                    <div class='portlet-heading bg-danger'>
                                        <h3 class='portlet-title'>
                                          <?php echo $row['type'] ?> Processed Request
                                        </h3>
                                        <div class="portlet-widgets">
                                            <a href="javascript:void(0);" onclick="refreshTransferFund(<?php echo $row['donation_id'] ?>);" data-toggle="reload"><i class="ion-refresh"></i></a>
                                            <span class="divider"></span>
                                            <a data-toggle="collapse" data-parent="#accordion1" href="#bg-primary<?php echo $row['donation_id'] ?>"><i class="ion-minus-round"></i></a>
                                            <span class="divider"></span>
                                            <a href="javascript:void(0);" onclick="if (alert('Are you sure you want to delete this?')) deleteTransferFund(<?php echo $row['donation_id'] ?>);"><i class="ion-close-round"></i></a>
                                        </div>
                                        <div class='clearfix'></div>
                                    </div>
                                    <div id='bg-primary<?php echo $row['donation_id'] ?>' class='panel-collapse collapse in'>
                                        <div class='portlet-body'>
                                            <p> Your order has been processed.</p>
                                            <p> Amount: <?php if ($row['donation_ph'] != '') echo $row['donation_ph']; else echo $row['donation_gh']; ?></p>
                                            <?php
                                                if($row['donation_gh'] != '')
                                            {
                                              ?>
                                              <p>Please attend to your order request</p>
                                              <!-- <p>Kindly wait to receive your payment into your bank account details from 
                                              <?php 
                                              if ($memberdetails['member_id'] == $row['member_id'])
                                              {
                                                  //
                                                  echo "Greencollar Administrator";
                                              }
                                              else 
                                              {
                                                
                                            echo $row['lastname']. " ". $row['firstname'];
                                              } 
                                            ?> within 12 .</p> -->
                                              <?php
                                            }
                                            else {
                                            ?>
                                            <p>Please attend to your request</p>
                                            <?php
                                            
                                                
                                            }
                                            ?>
                                            <hr>
                                            <p> Status: Processed</p>
                                            <p class='text-right'>
                                               <button class='btn btn-danger btn-xs waves-effect waves-light' data-toggle='modal' data-target=".<?php echo $row['donation_id'] ?>">Details</button>
                                            </div>
                                        </div>
                                        <div class='modal fade <?php echo $row['donation_id'] ?>' tabindex='-1' role='dialog' aria-labelledby='mySmallModalLabel' aria-hidden='true' style='display: none;'>
                                        <div class='modal-dialog modal-sm'>
                                            <div class='modal-content'>
                                                <div class='modal-header'>
                                                    <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
                                                    <h4 class='modal-title' id='mySmallModalLabel'><?php echo $row['type']; ?> Request details</h4>
                                            </div>
                                                <div class='modal-body'>
                                                  <p>Your <?php echo $row['type']; ?> order has been processed.</p>
                                                  <p>Amount: <?php if ($row['donation_ph'] != '') echo $row['donation_ph']; else echo $row['donation_gh']; ?></p>
                                                    
                                                  <p>Status: Processed</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    else if ($row['status'] == '0')
                                    {
                                    ?>
                                    <div class='portlet-heading bg-success'>
                                        <h3 class='portlet-title'>
                                           <?php echo $row['type']; ?> Request
                                        </h3>
                                        <div class="portlet-widgets">
                                            <a href="javascript:void(0);" onclick="refreshTransferFund(<?php echo $row['donation_id'] ?>);" data-toggle="reload"><i class="ion-refresh"></i></a>
                                            <span class="divider"></span>
                                            <a data-toggle="collapse" data-parent="#accordion1" href="#bg-primary<?php echo $row['donation_id'] ?>"><i class="ion-minus-round"></i></a>
                                            <span class="divider"></span>
                                            <a href="javascript:void(0);" onclick="if (alert('Are you sure you want to delete this?')) deleteTransferFund(<?php echo $row['donation_id'] ?>);"><i class="ion-close-round"></i></a>
                                        </div>
                                        <div class='clearfix'></div>
                                    </div>
                                    <div id='bg-primary<?php echo $row['donation_id'] ?>' class='panel-collapse collapse in'>
                                        <div class='portlet-body'>
                                            <p> Your <?php echo $row['type']; ?> order has been confirmed.</p>
                                            <p> Amount: <?php if ($row['donation_ph'] != '') echo $row['donation_ph']; else echo $row['donation_gh']; ?></p>
                                           
                                            <hr>
                                            <p> Status: Confirmed</p>
                                            <div class='text-right'>
                                                <?php if ($row['testimonialstatus'] == '1' && $row['donation_gh'] != '') 
                                                {
                                                ?>
                                                <button class='btn btn-custom btn-xs waves-effect waves-light' data-toggle='modal' data-target=".t<?php echo $row['donation_id'] ?>"><i class='fa fa-warning'></i> Upload testimony</button>
                                                <?php
                                                }
                                                elseif ($row['testimonialstatus'] == '5' && $row['donation_gh'] != '')
                                                {
                                                ?>
                                                
                                                <button class='btn btn-custom btn-xs waves-effect waves-light'><i class='fa fa-check'></i> Testimony saved</button>
                                                <?php
                                                }
                                                ?>
                                               <button class='btn btn-danger btn-xs waves-effect waves-light' data-toggle='modal' data-target=".<?php echo $row['donation_id'] ?>">Details</button>
                                            </div>
                                        </div>
                                    </div>
                                        <div class='modal fade <?php echo $row['donation_id'] ?>' tabindex='-1' role='dialog' aria-labelledby='mySmallModalLabel' aria-hidden='true' style='display: none;'>
                                        <div class='modal-dialog modal-sm'>
                                            <div class='modal-content'>
                                                <div class='modal-header'>
                                                    <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
                                                    <h4 class='modal-title' id='mySmallModalLabel'><?php echo $row['type']; ?> request details</h4>
                                            </div>
                                                <div class='modal-body'>
                                                  <p>Your <?php  echo $row['type']; ?> order has been confirmed. </p>
                                                  <p>Amount: <?php if ($row['donation_ph'] != '') echo $row['donation_ph']; else echo $row['donation_gh']; ?></p>
                                                  <p>Status: Confirmed</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class='modal fade t<?php echo $row['donation_id'] ?>' tabindex='-1' role='dialog' aria-labelledby='mySmallModalLabel' aria-hidden='true' style='display: none;'>
                                        <div class='modal-dialog modal-lg'>
                                            <div class='modal-content'>
                                                <div class='modal-header'>
                                                    <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
                                                    <h4 class='modal-title' id='mySmallModalLabel'>Upload Testimony</h4>
                                            </div>
                                                <div class='modal-body'>
                                                  <h3> You received a sum of <?php echo $row['donation_gh'] ?> in your last receive payment order. Please write a testimony letter to help us grow this community.
                                                    A testimony letter should contain the following: </h3>
                                                    <p><ol>
                                                            <li>Your Name e.g Ben Charles</li>
                                                            <li>Your location e.g Lagos, Nigeria</li>
                                                            <li>The last date you made a transfer request order and the amount </li>
                                                            <li>The date you received payment. </li>
                                                            <li>And the amount received. </li>
                                                        </ol></p>
                                                    <p> For example, <br>
                                                        <span style='color: #FF0000; font-weight: bold'>My Name is Ben Charles and i live in Lagos, Nigeria. I made a transfer request order on the 20th April, 2019 with the sum of 5000 and I received
                                                        the sum of 10000 on the 27th of May, 2019. Thanks to Greencollar Networld! </span></p>
                                                        <div id="testimonyresult<?php echo $row['donation_id'] ?>" ></div>
                                                        <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group testimony error" id="testimonydiv<?php echo $row['donation_id'] ?>"> 
                                                                <label for="testimony" class="control-label">Testimony letter</label> 
                                                                <textarea class="form-control" name="testimnony" id="testimony<?php echo $row['donation_id'] ?>" rows="5" placeholder="Write testimony letter here..."></textarea>
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    <div class='text-center'>
                                                        <div class="button-list">
                                                            <button type="button" class="btn btn-success waves-effect waves-light" id="testimonybutton<?php echo $row['donation_id'] ?>" onclick="testimony(<?php echo $row['donation_id'] ?>);"><i class='fa fa-check'></i> Save</button> 
                                                            <button type="button" class="btn btn-info waves-effect waves-light" data-dismiss="modal">Cancel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                   </div>
                                            <?php
                                                $count++;
                                            }
                                            }
                                            ?>

                                </div>
                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            </div>
                        </div>
                      <?php
                        }
                        if ($memberdetails['memberstatus'] != '10')
                        {
                        ?>
                      <div class="row">
                       
                            <div class="col-lg-12" id="news_section">
                            <div class="card-box">
                              <h4 class="text-dark header-title m-t-0">News from Admin</h4>
                                <?php
                                            foreach($newsdetails as $row)
                                            {
                                            ?> 
                                    <div class="comment">
                                        <img src="assets/images/users/avatar-4.jpg" alt="Admin_photo" class="comment-avatar">
                                        <div class="comment-body">
                                            <div class="comment-text">
                                                <div class="comment-header">
                                                    <a href="#" title="">Administrator</a><span>on <?php echo formatDate($row['thedate'], "yes") ?></span>
                                                </div>
                                                <div class="m-t-15">
                                                    <p style="font-weight: bolder"><?php echo $row['title'] ?></p>
                                                    <p  style='font-weight: normal'><?php echo substr($row['content'], 0, 100) ?></p>
                                                    <button class="btn btn-custom waves-effect waves-light" data-toggle="modal" data-target="#news<?php echo $row['new_id'] ?>">View</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="news<?php echo $row['new_id'] ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog"> 
                                            <div class="modal-content"> 
                                                <div class="modal-header"> 
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                    <h4 class="modal-title" style="text-transform: uppercase"><?php echo $row['title'] ?></h4> 
                                                </div> 
                                                <div class="modal-body">
                                                    <p><?php echo $row['content'] ?>...</p>
                                                </div> 
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal">Close</button>
                                                </div> 
                                            </div> 
                                        </div>
                                    </div><!-- /.modal -->
                                    <?php
                                    }
                                    ?>
                                    </div>

                                    

                            </div>

                            </div>
                            <?php 
                            }

                            ?>
                        </div>


                    </div> <!-- container -->

                </div> <!-- content -->

               <?php 
               include_once('inc_footer.php'); 
               ?>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


  

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>

        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        <script src="assets/plugins/peity/jquery.peity.min.js"></script>

        <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

         <!-- Modal-Effect -->
        <script src="assets/plugins/custombox/js/custombox.min.js"></script>
        <script src="assets/plugins/custombox/js/legacy.min.js"></script>

        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
        <script src="js/custom.js"></script>




    </body>
</html>