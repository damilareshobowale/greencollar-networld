<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once("admin/inc_dbfunctions.php");

function databaseConnect()
{
    require("admin/connectionstrings.php");


    $mycon = new PDO("mysql:host=$MYSQL_Server;dbname=$MYSQL_Database;charset=utf8", "$MYSQL_Username", "$MYSQL_Password"); 
    $mycon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $mycon->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);    
    return $mycon;
}

function numToOrdinalWord($num)
{
    $first_word = array('eth','First','Second','Third','Fouth','Fifth','Sixth','Seventh','Eighth','Ninth','Tenth','Elevents',
    'Twelfth','Thirteenth','Fourteenth','Fifteenth','Sixteenth','Seventeenth','Eighteenth','Nineteenth','Twentieth');
    $second_word =array('','','Twenty','Thirty','Forty','Fifty');

    if($num <= 20)
        return $first_word[$num];

    $first_num = substr($num,-1,1);
    $second_num = substr($num,-2,1);

    return $string = str_replace('y-eth','ieth',$second_word[$second_num].'-'.$first_word[$first_num]);
}

function sendEmail($email,$subject,$message)
{
   require 'vendor/autoload.php';

	$message = "<!DOCTYPE html>
	<html>
			<head>
					<meta charset='utf-8'>
					<meta name='viewport' content='width=device-width, initial-scale=1.0'>
					<meta name='description' content='Greencollar Networld - .'>
					<meta name='author' content='Wealth Fund Global'>
	
					<link rel='shortcut icon' href='img/logo/logowfg.ico'>
	
					<title>Email Templates</title>
	
					<link href='http://greencollarnetworld.com/mod/assets/css/bootstrap.min.css' rel='stylesheet' type='text/css' />
					<link href='http://greencollarnetworld.com/mod/assets/css/core.css' rel='stylesheet' type='text/css' />
					<link href='http://greencollarnetworld.com/mod/assets/css/components.css' rel='stylesheet' type='text/css' />
					<link href='http://greencollarnetworld.com/mod/assets/css/icons.css' rel='stylesheet' type='text/css' />
					<link href='http://greencollarnetworld.com/mod/assets/css/pages.css' rel='stylesheet' type='text/css' />
					<link href='http://greencollarnetworld.com/mod/assets/css/responsive.css' rel='stylesheet' type='text/css' />
					<link href='http://greencollarnetworld.com/mod/css/style.css' rel='stylesheet' type='text/css' />
	
					
			</head>
	
	
			<body>
				<div style='max-width: 100vw; height: auto;padding: 5vw;background-color: rgb(3, 174, 67);'>
					<div style='background-color: #FFFFFF;padding: 5vw 2%'>
					<div style='display:flex;justify-content: center;text-align: center;align-content: center;align-items: center;flex-direction:row;'>
							<center><div>
								<center><img src='http://greencollarnetworld.com/mod/img/logo/logogcn.png' alt='GreenCollar_Logo' style='height: 100px;' /></center>
								<br/>
								<h1>Greencollar Networld</h1>
							</div>
							</center>
					</div>
					<hr />
					<div style='margin: 5%;'>
					". $message ."
					</div>
	
					<div style='margin: 5%;'>
						<hr />
						<center>
							<div style='display:flex;justify-content: center;text-align: center;align-content: center;align-items: center;flex-direction: row;'>
							<p>This email is automatically generated from Greencollar Networld Mailing Platform</p><br />
							<p>&copy; 2019. <a href='https://greencollarnetworld.com'>Greencollar Networld</a></p>
						</div>
						</center>
					</div>
	
					</div>
				</div>
	
			</body>
	</html>";

	$mail = new PHPMailer(true);

	try {
			//Server settings                                     // Enable verbose debug output
			$mail->isSMTP();                                            // Set mailer to use SMTP
			$mail->Host       = 'smtp.gmail.com';  // Specify main and backup SMTP servers
			$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
			$mail->Username   = 'greencollarnetworld@gmail.com';                     // SMTP username
			$mail->Password   = 'security01@';                               // SMTP password
			$mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
			$mail->Port       = 587;                                    // TCP port to connect to

			//Recipients
			$mail->setFrom('noreply@greencollarnetworld.com', 'Greencollar Networld');
			$mail->addAddress($email);     // Add a recipient
			$mail->addReplyTo('support@greencollarnetworld.com', 'Greencollar Networld Support');

			// Content
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = $subject;
			$mail->Body    = $message;
			$mail->AltBody = strip_tags($message);

			$mail->send();
			return true;
	} catch (Exception $e) {
			// echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
			return true;
	}
}



$mycon = databaseConnect();

function isWeekend($date) {
    return (date('N', strtotime($date)) >= 6);
}


Fundgrowth();

ProvideHelp();

//FirstReCommitmentCheck();

GetHelp();

failedParticipants();

NotifyUserReceivePayment();


function ProvideHelp()
{
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();


    //get all the fund Deposit Fund request with first time status = 0 and status of 5 and member status equal to 0
    $recommitmenttype = 5;
    $providehelp_all = $dataRead->providehelp_all($mycon, '5', '0', '0', $recommitmenttype);
    

    foreach($providehelp_all as $row)
    {
        $providehelpdonation_id = $row['donation_id'];
        // 1. automatically create an associated GH for the system admin account
        // get the donation account with the type as 2 which signifes the account meant for all PH
        $type = 2;
        $bankaccountdetails = $dataRead->bankaccountdetails_one($mycon,$type);
        $member_id = $bankaccountdetails['member_id'];

        //calculate 3 days from now to transfer fund request again
        $readydonation_ph = date("Y-m-d H:i:s", strtotime("-7 days"));
        $readydonation_gh = date("Y-m-d H:i:s");
        
        //add to the donation table
        $amount = $row['donation_ph'];
        $bankaccounts = $bankaccountdetails['accountdetail_id'];
        $firsttime = 0;
        $donation_id = $dataWrite->donation_add_gh($mycon,$amount,$member_id,$readydonation_ph,$readydonation_gh,$bankaccounts,$firsttime, 'Receive Payment');
        
        if (!$donation_id)
        {
            var_dump($donation_id);
            return;
        }
        //2. get the donation details for the admin

        //3. merge the user with the admin, automatically.
        //update the matching table
        //check if the matching falls on  weekend
        if (date('D') == 'Fri' || date('D') == 'Sat')
        $expirydate = date("Y-m-d H:i:s", strtotime("+ 12 hours"));
        else $expirydate = date("Y-m-d H:i:s", strtotime("+ 12 hours"));

        //matched the particiapants to its donations
        $mycon->beginTransaction();
        $matching_id = $dataWrite->matching_add($mycon,$member_id,$row['member_id'],$donation_id,$row['donation_id'],$amount,$bankaccounts,$expirydate);
        if (!$matching_id)
        {
            $mycon->rollBack();
            var_dump($matching_id);
           return;
        }

        //change the status of the transfer particiapants to status of 3 and matched status to 5 and leftover id to 1
        $receiveupdatestatus = $dataWrite->donationsupdatestatus($mycon, $donation_id, '3', '5');

        //updates the donation id of the person that transfered
        $transferupdatestatus = $dataWrite->donationsupdatestatus($mycon, $row['donation_id'], '3', '5');
        if (!$receiveupdatestatus || !$transferupdatestatus)
        {
            $mycon->rollBack();
            var_dump($transferupdatestatus. " ----- \n \n". $receiveupdatestatus);
           return;
        }

        // add donation receivable to the admin
        //find the receive donation by member
        //check if the member already exists in the donations receivable table
        $membercheck = $dataRead->donationsreceivable_getbyidmember($mycon, $member_id);
        
        if (!$membercheck)
        {
            //add new member to the donations receivable column
            $addmember = $dataWrite->donationsreceivable_add($mycon, $member_id, '0', '0', '0');
            if (!$addmember) 
            {
                var_dump($addmember);
                return;
            }
        }

        //update the amount
        $updateamount = $dataWrite->donationsreceivable_updatewithdrawn($mycon, $member_id, 
        $amount, 0);

        if (!$receiveupdatestatus || !$transferupdatestatus)
        {
            $mycon->rollBack();
            var_dump($transferupdatestatus. " ----- \n \n". $receiveupdatestatus);
           return;
        }

        $donationsreceivable = $dataRead->donationsreceivable_getbyidmember($mycon, $member_id);

        //remove from the fund receivable
        $updatewithdrawn = $dataWrite->donationsreceivable_updatewithdrawn($mycon,$member_id,
        $amount - $amount , '0');
        if (!$updatewithdrawn)
        {
            $mycon->rollBack();
            var_dump($updatewithdrawn);
            return;
        }

        $amount = $row['donation_ph'];

        //get the name of the receiver 
        $receiverdetails = $dataRead->member_getbyid($mycon, $member_id);
        $transfermessage = "<div class='container'>
                            <p>Dear ".$row['username'].",</p>
                            <p>Kindly make a deposit to only the account details provided on your dashboard and upload the 
                            proof of payment. Amount placed is ".$amount." </p>
                            <p> Thank you.</p>
                            <p style='margin-top: 100px'><small><em>This message is auto-generated, please do not reply via your email.</em></small></p>
                        </div>";
        $receivermessage = "<div class='container'>
                            <p>Dear Admin".$receiverdetails['firstname']. " ". $receiverdetails['lastname']. ", </p>
                            <p>You have a new order to receive payment. The sender information is on your dashboard when you login. 
                            Amount to receive is ".$amount." </p>
                            <p style='margin-top: 100px'><small><em>This message is auto-generated, please do not reply via your email.</em></small></p>
                        </div>";
        if(sendEmail($row['email'],"Deposit Fund Matched - Greencollar Networld", $transfermessage) && sendEmail($receiverdetails['email'], "Receive Payment Matched - Greencollar Networld", $receivermessage)) {
            $mycon->commit();
            echo "done!";
            echo "Deposit Fund Request Matched";
        } 
        else {
            $mycon->rollBack();
            var_dump($receiverdetails);
        }
    }
}

function Fundgrowth()
{
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();



    //find all donations ph with rcommmitment status as 2
    $recommitment = 2;
    $findalldonations = $dataRead->donations_getallwithrecommitment($mycon, $recommitment);
    foreach ($findalldonations as $row)
    {
        //$timedifference = (strtotime($row['createdon']) - strtotime(date("Y-m-d, H:i:s")));
        //var_dump($timedifference);
        //strtotime(date("Y-m-d H:i:s"), strtotime("+ 30 days"));
         if ((strtotime($row['readydonation_gh']) >= strtotime(date("Y-m-d H:i:s"))))
            {
                $countdays = intval(abs(strtotime($row['readydonation_gh']) - strtotime(date("Y-m-d H:i:s")))/86400);
                $datenow = date("d") + date("d", strtotime($row['createdon']));
                $totaldonation_ph = ($row['donation_ph'] + (($row['donation_ph'] * (0.3) * (7 - $countdays))/7));
                //update the growth colum for the donation table
                $growthupdate = $dataWrite->donations_growthupdate($mycon, $row['donation_id'], ceil($totaldonation_ph));
                if (!$growthupdate)
                {
                    echo false;
                }
                else echo true;
                
            }
            else
            {
                $totaldonation_ph = ($row['donation_ph'] * 0.3) + $row['donation_ph'];
                //update the growth colum for the donation table
                $growthupdate = $dataWrite->donations_growthupdate($mycon, $row['donation_id'], $totaldonation_ph);
                
            }

    }

}
 
function FirstReCommitmentCheck()
{
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();
    //1. check all the donations with status of recommit =  0 and donation status = 3 and matched status = 5


    $memberstatus = 0;
    $status = 5;
    $firsttime = 1; //FIRST PH
    $recommitmenttype = 2; //FIRST RECOMMIT
    $recommit = 0;
    //check for all the first recommitment
    $providehelp_all = $dataRead->providehelp_allfirst($mycon, '0', '5', '0', $firsttime, $recommitmenttype, $recommit);

    foreach($providehelp_all as $row)
    {
        //get the cycle of the user
        $getCycle = $dataRead->cycle_getbyid($mycon, $row['member_id']);
        if ($row['role'] != 1 && $getCyctle['cycle'] == 0)
        {
            //check if the date of donation is 5 days or more
            $recommitmentdate = strtotime(date("Y-m-d H:i:s", strtotime("now")));
            if (strtotime($row['readydonation_ph']) <= $recommitmentdate) // the ready donation ph is equal to NULL
            {
                $amount = $row['donation_ph'];
                $member_id = $row['member_id'];
    
                //calculate 5 days from for recommitment and 7 days for receiving fund
                $readydonation_ph = date("Y-m-d H:i:s", strtotime("+9 days"));
                $readydonation_gh = date("Y-m-d H:i:s", strtotime("+2 days"));
                $accountdetail_id = $row['accountdetail_id'];
                $firsttime = 0;
                // status 3 for recommitment not available to GH
                $recommitmenttype = 3;
    
    
                $mycon->beginTransaction();
                //create a new PH for the member for recommitment
                $firsttime = 1; //shows 2nd time PH
                $firststatus = 5;
                $recommitment_ph = $dataWrite->donation_add($mycon,$amount,
                $member_id,$readydonation_ph,$readydonation_gh,
                $accountdetail_id,$firsttime, $firststatus, $recommitmenttype, 'Recommitment');
                if (!$recommitment_ph)
                {
                    $mycon->rollBack();
                    return;
                }
    
                $providehelpdonation_id = $recommitment_ph;
                // 1. automatically create an associated GH for the system admin account
                // get the donation account with the type as 2 which signifes the account meant for all PH
                $readydonation_ph = date("Y-m-d H:i:s", strtotime("-9 days"));
                $readydonation_gh = date("Y-m-d H:i:s");
                $type = 2;
                $bankaccountdetails = $dataRead->bankaccountdetails_one($mycon,$type);
                $member_id = $bankaccountdetails['member_id'];
    
                //calculate 3 days from now to transfer fund request again
                $readydonation_ph = date("Y-m-d H:i:s", strtotime("-3 days"));
                $readydonation_gh = date("Y-m-d H:i:s");
                
                //add to the donation table
                $bankaccounts = $bankaccountdetails['accountdetail_id'];
                $firsttime = 0;
                $donation_id = $dataWrite->donation_add_gh($mycon,$amount,$member_id,$readydonation_ph,$readydonation_gh,$bankaccounts,$firsttime, 'Receive Payment');
                
                if (!$donation_id)
                {
                    $mycon->rollBack();
                    return;
                }
                //2. get the donation details for the admin
    
                //3. merge the user with the admin, automatically.
                //update the matching table
                //check if the matching falls on  weekend
                if (date('D') == 'Fri' || date('D') == 'Sat')
                $expirydate = date("Y-m-d H:i:s", strtotime("+ 12 hours"));
                else $expirydate = date("Y-m-d H:i:s", strtotime("+ 12 hours"));
    
                //matched the particiapants to its donations
                $matching_id = $dataWrite->matching_add($mycon,$member_id,$row['member_id'],$donation_id,$recommitment_ph,$amount,$bankaccounts,$expirydate);
                if (!$matching_id)
                {
                    $mycon->rollBack();
                    var_dump($matching_id);
                    return;
                }
    
                //update the recommit column of the donation
                $donationsupdate_recommit = $dataWrite->donationsupdate_recommit($mycon, $row['donation_id'], 5);
                if (!$donationsupdate_recommit)
                {
                    $mycon->rollBack();
                    var_dump($donationsupdate_recommit);
                    return;
                }
    
                //change the status of the transfer particiapants to status of 3 and matched status to 5 and leftover id to 1
                $receiveupdatestatus = $dataWrite->donationsupdatestatus($mycon, $donation_id, '3', '5');
    
                //updates the donation id of the person that transfered
                $transferupdatestatus = $dataWrite->donationsupdatestatus($mycon, $providehelpdonation_id, '3', '5');
                if (!$receiveupdatestatus || !$transferupdatestatus)
                {
                    $mycon->rollBack();
                    var_dump($transferupdatestatus. " ----- \n \n". $receiveupdatestatus);
                    return;
                }
    
                // add donation receivable to the admin
                //find the receive donation by member
                //check if the member already exists in the donations receivable table
                $membercheck = $dataRead->donationsreceivable_getbyidmember($mycon, $member_id);
                if (!$membercheck)
                {
                    //add new member to the donations receivable column
                    $addmember = $dataWrite->donationsreceivable_add($mycon, $member_id, '0', '0', '0');
                    if (!$addmember) 
                    {
                        var_dump($addmember);
                        return;
                    }
                }
    
                //update the amount
                $updateamount = $dataWrite->donationsreceivable_updatewithdrawn($mycon, $member_id, 
                $amount, 0);
    
                if (!$receiveupdatestatus || !$transferupdatestatus)
                {
                    $mycon->rollBack();
                    var_dump($transferupdatestatus. " ----- \n \n". $receiveupdatestatus);
                return;
                }
    
                $donationsreceivable = $dataRead->donationsreceivable_getbyidmember($mycon, $member_id);
    
                //remove from the fund receivable
                $updatewithdrawn = $dataWrite->donationsreceivable_updatewithdrawn($mycon,$member_id,
                $amount - $amount, '0');
                if (!$updatewithdrawn)
                {
                    $mycon->rollBack();
                    var_dump($updatewithdrawn);
                    return;
                }
    
                //get the name of the receiver 
                $receiverdetails = $dataRead->member_getbyid($mycon, $member_id);
                $transfermessage = "<div class='container'>
                                    <p>Dear ".$row['username'].",</p>
                                    <p>This is to notify you that you are due to pay recommitment. <br />
                                    Kindly click on the Recommitment Button on your dashboard. </p>
                                    <p> Thank you.</p>
                                    <p style='margin-top: 100px'><small><em>This message is auto-generated, please do not reply via your email.</em></small></p>
                                </div>";
                $receivermessage = "<div class='container'>
                                    <p>Dear admin,  </p>
                                    <p>This is to notify you that you are due to pay recommitment. <br />
                                    Kindly click on the Recommitment Button on your dashboard.  </p>
                                    <p style='margin-top: 100px'><small><em>This message is auto-generated, please do not reply via your email.</em></small></p>
                                </div>";
                if(sendEmail($row['email'],"Recommitment Matched - Greencollar Networld", $transfermessage) && sendEmail($receiverdetails['email'], "Receive Payment (Recommitment) Matched - Greencollar Networld", $receivermessage)) {
                    $mycon->commit();
                    echo "done!";
                    echo "Recommitment Request Matched";
                } 
                else {
                    $mycon->rollBack();
                    var_dump($receiverdetails);
                }
            }

        }
    }
}

function GetHelp()
{
    // 1. get all help that is ready to gh, means it has provided means within the last two days 
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();


    //get all the fund Deposit Fund request with first time status = 0 and status of 5 and member status equal to 0
    $memberstatus = 0;
    $firsttime = 0;
    $status = 5;
    $gethelp_all = $dataRead->gethelp_all($mycon, '5', '0', '0', '0');
    if (isWeekend(date('Y-m-d H:i:s')) !== 1) {
        foreach($gethelp_all as $row)
        {
            $providehelpdonation_id = $row['donation_id'];
            // 1. automatically create an associated PH for the system admin account
            // get the donation account with the type as 2 which signifes the account meant for all PH
            $type = 2;
            $bankaccountdetails = $dataRead->bankaccountdetails_one($mycon,$type);
            $member_id = $bankaccountdetails['member_id'];

            //calculate 3 days from now to transfer fund request again
            $readydonation_ph = date("Y-m-d H:i:s", strtotime("-3 days"));
            $readydonation_gh = date("Y-m-d H:i:s");
            
            //add to the donation table
            $amount = $row['donation_gh'];
            $bankaccounts = $bankaccountdetails['accountdetail_id'];
            $firsttime = 0;
            $growth = $row['donation_gh'] * 2;
            $mycon->beginTransaction();
            $donation_id = $dataWrite->donation_add_ph($mycon,$amount,$member_id,$readydonation_ph,$readydonation_gh,$bankaccounts,$firsttime, 'Deposit Fund', $growth);
            
            if (!$donation_id)
            {
                var_dump($donation_id);
                return;
            }
            //2. get the donation details for the admin

            //3. merge the user with the admin, automatically.
            //update the matching table
            //check if the matching falls on  weekend
            if (date('D') == 'Fri' || date('D') == 'Sat')
            $expirydate = date("Y-m-d H:i:s", strtotime("+ 12 hours"));
            else $expirydate = date("Y-m-d H:i:s", strtotime("+ 12 hours"));

            //matched the particiapants to its donations
            
            $matching_id = $dataWrite->matching_add($mycon,$row['member_id'],$member_id,$row['donation_id'],$donation_id,$amount,$bankaccounts,$expirydate);
            if (!$matching_id)
            {
                $mycon->rollBack();
                var_dump($matching_id);
            return;
            }

            //change the status of the transfer particiapants to status of 3 and matched status to 5 and leftover id to 1
            $receiveupdatestatus = $dataWrite->donationsupdatestatus($mycon, $donation_id, '3', '5');

            //updates the donation id of the person that transfered
            $transferupdatestatus = $dataWrite->donationsupdatestatus($mycon, $row['donation_id'], '3', '5');
            if (!$receiveupdatestatus || !$transferupdatestatus)
            {
                $mycon->rollBack();
                var_dump($transferupdatestatus. " ----- \n \n". $receiveupdatestatus);
            return;
            }

            //update the recommit column of the donation
            $donationsupdate_recommit = $dataWrite->donationsupdate_recommit($mycon, $row['donation_id'], 5);
            if (!$donationsupdate_recommit)
            {
                $mycon->rollBack();
                var_dump($donationsupdate_recommit);
                return;
            }

            // add donation receivable to the admin
            //find the receive donation by member
            //check if the member already exists in the donations receivable table
            $membercheck = $dataRead->donationsreceivable_getbyidmember($mycon, $member_id);
            
            if (!$membercheck)
            {
                //add new member to the donations receivable column
                $addmember = $dataWrite->donationsreceivable_add($mycon, $member_id, '0', '0', '0');
                if (!$addmember) 
                {
                    var_dump($addmember);
                    return;
                }
            }

            //update the amount
            $updateamount = $dataWrite->donationsreceivable_updatewithdrawn($mycon, $member_id, 
            $amount, 0);

            if (!$receiveupdatestatus || !$transferupdatestatus)
            {
                $mycon->rollBack();
                var_dump($transferupdatestatus. " ----- \n \n". $receiveupdatestatus);
            return;
            }

            //update the recommitment to 3
            $recommitment = 0;
            $recommitmentupdate = $dataWrite->updateMemberDonationRecommitment($mycon, $providehelpdonation_id, $recommitment);
            if (!$recommitmentupdate)
            {
                $mycon->rollBack();
            return;
            }
            $donationsreceivable = $dataRead->donationsreceivable_getbyidmember($mycon, $member_id);

            //remove from the fund receivable
            $updatewithdrawn = $dataWrite->donationsreceivable_updatewithdrawn($mycon,$member_id,
            $amount - $amount , '0');
            if (!$updatewithdrawn)
            {
                $mycon->rollBack();
                var_dump($updatewithdrawn);
                return;
            }

            $amount = $row['donation_gh'];

            //get the name of the receiver 
            $receiverdetails = $dataRead->member_getbyid($mycon, $member_id);
            $getCycle = $dataRead->cycle_getbyid($mycon, $row['member_id']);
            $receivermessage = "<div class='container'>
                                <p>Dear ".$row['username'].",</p>
                                <p>Your Payment is on its way to your account. Please  check your dashboard for more information
                                Amount to receive is ".$amount." </p>
                                <p> Thank you.</p>
                                <p style='margin-top: 100px'><small><em>This message is auto-generated, please do not reply via your email.</em></small></p>
                            </div>";
            $transfermessage = "<div class='container'>
                                <p>Dear ".$receiverdetails['firstname']. " ". $receiverdetails['lastname']. ", </p>
                                <p>There is a new order to send payment. The receiver information is on your dashboard when you login. 
                                Amount to deposit is ".$amount." </p>
                                <p style='margin-top: 100px'><small><em>This message is auto-generated, please do not reply via your email.</em></small></p>
                            </div>";
            $mycon->commit();
            echo "done!";
            echo "Receive Payment Request Matched";
            // if(sendEmail($row['email'],"Receive Payment Matched - Greencollar Networld", $receivermessage) && sendEmail($receiverdetails['email'], "Deposit Fund Matched - Greencollar Networld", $transfermessage)) {
            //     $mycon->commit();
            //     echo "done!";
            //     echo "Receive Payment Request Matched";
            // } 
            // else {
            //     $mycon->rollBack();
            //     var_dump($receiverdetails);
            // }
        }
    }
}

//block participants who failed to pay and make a remerge
function failedParticipants()
{
    $mycon = databaseConnect();
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();

    //find all the matching details
    $todaydate = date("Y-m-d H:i:s");
    $matchingdetails = $dataRead->matching_getallactivestatus($mycon, '5');
    $mycon->beginTransaction();
    foreach ($matchingdetails as $row)
    {
       if (strtotime($row['expirydate']) < strtotime($todaydate) && $row['status'] == 5)
       {
           //get the transfer details and block the participants
           $transferdetails = $dataRead->member_getbyid($mycon, $row['transfer_id']);
           if ($transferdetails['role'] != 1)
           {
                //update the participants status to 10 to show blocked member
                $updatestatus = $dataWrite->members_updatestatus($mycon, $transferdetails['member_id'], '10');
                if (!$updatestatus)
                {
                    $mycon->rollBack();
                    echo "failed update 1";
                }
    
                //send email to participants to inform their present status
                $message = "<div class='container'>
                                    <p>Dear ".$transferdetails['username'].",</p>
                                    <p>We are sorry to inform you that the system has automatically suspended your account for failure to pay within the specified time. </p>
                                    <p>If you are ready to participate mutually and wants your accounts running again, please login and pay the penalty fee 
                                    to reopen your account again. </p>
                                    
                                    <p>For more information, please contact support at <a href='mailto:support@greencollarnetworld.com'>support@greencollarnetworld.com</a> or use the livechat.</p>
                                    <p>Regards.</p>
                                    <p><small><em>This message is auto-generated, please do not reply via your email.</em></small></p>
                                </div>";
                if (sendEmail($transferdetails['email'], "Account Suspended - Greencollar Metworld", $message))
                {
                    //delete the donation both for the admin and the PH
                    $deletetransfer_donation = $dataWrite->donation_delete($mycon, $row['transferfund_id']);
                    if (!$deletetransfer_donation)
                    {
                        $mycon->rollBack();
                        echo "Undeleted Donation 1";
                    }
    
                    //delete the donation for the admin
                    $deletetransfer_donation = $dataWrite->donation_delete($mycon, $row['receivefund_id']);
                    if (!$deletetransfer_donation)
                    {
                        $mycon->rollBack();
                        echo "Deleted Donation 2";
                    }
    
                    //delete the matching 
                    $deletematching = $dataWrite->matching_deletebyid ($mycon, $row['matching_id']);
                    if (!$deletematching)
                    {
                        $mycon->rollBack();
                        echo "Deleted Matching";
                    }

                }
 
            }
       }

                
    }
    $mycon->commit();     

}


//return the cycle type
function returnCycle($cycle, $member_id)
{
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();
    if ($cycle == 0) 
    {
        $index = 0;
        //update the table
        $updatecycle = $dataWrite->cycle_updatelevel($mycon, $member_id, $index, 0);
        return $index;
    }
    else if (fmod($cycle, 4) == 0)
    {
        $index = 4;
        //update the table
        $updatecycle = $dataWrite->cycle_updatelevel($mycon, $member_id, $index, 0);
        return $index;
    }
    else if (fmod($cycle, 4) == 2)
    {
        $index = 2;
        //update the table
        $updatecycle = $dataWrite->cycle_updatelevel($mycon, $member_id, $index, 0);
        return $index;
    }
    else if (fmod($cycle, 4) == 3)
    {
        $index = 3;
        //update the table
        $updatecycle = $dataWrite->cycle_updatelevel($mycon, $member_id, $index, 1);
        return $index;
    }
    else 
    {
        $index = 1;
        //update the table
        $updatecycle = $dataWrite->cycle_updatelevel($mycon, $member_id, $index, 0);
        return $index;
    }
}

function NotifyUserReceivePayment()
{
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();

    $memberstatus = 0;
    $status = 5;
    $recommitmenttype = 0;
    //check for all the Recommitment
    $providehelp_all = $dataRead->checklast_providehelpfirstdesc($mycon, '0', '5', '0', $recommitmenttype);
    
    foreach($providehelp_all as $row)
    {
        //get the cycle of the user 
		$getCycle = $dataRead->cycle_getbyid($mycon, $row['member_id']);
		
        if ($getCycle)
        {
            if (strtotime($row['readydonation_gh']) <= strtotime(date("Y-m-d H:i:s"))) // notify the receives add if the donation to PH again is more than the current time
            {
				
                //get the name of the receiver 
                $amount = $row['donation_ph'];
                $transfermessage = "<div class='container'>
                                    <p>Dear ".$row['username'].",</p>
                                    <p>This is to notify you that your funds is now available to withdraw. <br />
                                    Kindly check on the Receive Fund Button on your dashboard.
                                    </p>
                                    <p> Thank you.</p>
                                    <p style='margin-top: 100px'><small><em>This message is auto-generated, please do not reply via your email.</em></small></p>
                                </div>";
                if(sendEmail($row['email'],"Notification for Recommitment - Greencollar Networld", $transfermessage)) {
                    //update the status of the matched request
                    $updatestatus = $dataRead->updateDonationMessageStatus($mycon, $row['donation_id'], '1');
                    echo "Receive Payment Notification Sent <br>";
                    return;
                } 
                else {
                    echo "There was a problem sending your message";
                    return;  
                }
            }

        }
    }
}
?>