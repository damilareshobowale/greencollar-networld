<?php
require_once("admin/config.php");
require_once("admin/inc_dbfunctions.php");

$mycon = databaseConnect();
$mycon = databaseConnect();
$dataRead = New DataRead();


?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Greencollar Networld - Earn 30% ROI after 7 days on every fund you invest.">
        <meta name="author" content="Greencollar Networld">

        <link rel="shortcut icon" href="img/logo/logowfg.ico">

        <title>System Rules - <?php echo pageTitle(); ?></title>


        <link href="assets/plugins/custombox/css/custombox.css" rel="stylesheet">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="css/style.css" rel="stylesheet" type="text/css" />


        <link href="assets/plugins/bootstrap-table/css/bootstrap-table.min.css" rel="stylesheet" type="text/css" />


        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>
        
    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

               <?php include_once('inc_header.php') ?>

               <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">System Rules</h4>
                                <p class="text-muted page-title-alt">Welcome <?php echo getCookie("fullname") ?>!</p>
                            </div>
                        </div>
                        <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <h4 class="m-t-0 header-title"><b>Greencollar Networld System Rules</b></h4>
                            <p class="text-muted font-13 m-b-30">
                                Read our system rules for all participants on this global community</code>.
                            </p>
                            <div class="col-sm-12">
                                <div class="panel panel-border panel-custom">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Read the following carefully before participating...</h3>
                                    </div>
                                    <div class="panel-body">
                                        <p>
                                           Greencollar Networld is an investment platform that is built on the foundation of love, trust and humanity. It brings individuals together to help one another by putting our little resources together to help grow our finances under the safest conditions. Hence, GN is not a bank. It has no specific place where participants' money are kept. It simply involves depositing funds and receiving 30% increase within a period of 7days. Only the participants determines the growth of this community.  
                                            <br><span style='color: #FF0000'>Participants are to participates mutually with their spare cash only.</span>
                                        </p>
                                        <p> The following rules applies to all individuals on this global network. Make sure you read these rules and understand it before participating mutually.</p>
                                      
                                          <p><span class='text-primary'>1. </span> PAY ONLY TO THE ACCOUNT DETAILS ON YOUR DASHBOARD</p>
                                        <p><span class='text-primary'>2. </span> 12 HOURS PAYMENT WINDOW TO CONCLUDE ALL TRANSACTIONS</p>
                                         <p><span class='text-primary'>3. </span> NO ROOM FOR MULTIPLE ACCOUNTS </p>
                                         <p><span class='text-primary'>4. </span> NO ROOM FOR DOWNLINE DEFAULT AS UPLINE WILL BE PENALIZED </p>
                                        <p><span class='text-primary'>5. </span> NO REACTIVATION OF DEFAULTED ACCOUNT, YOU CAN ONLY CREATE NEW ACCOUNT AND START AFRESH. </p>
                                        <p><span class='text-primary'>6. </span> COMPULSORY TESTIMONY FOR ALL RECEIVERS AND DEPOSITORS </p>
                                        <p><span class='text-primary'>7. </span> DONT DEFAULT PAYMENT AS ANY ACCOUNT BLOCKED IS FOREVER </p>
                                        <p><span class='text-primary'>8. </span> CARRY OUT COMMUNITY TASK BY TELLING FRIENDS AND TRUSTED FAMILIES ABOUT US</p>
                                        <!-- 
                                        <p><span class='text-primary'>10. </span>You don't make receive funds request anyhow, there is room for recommitment. Failure to recommit that is make another transfer funds request after 30 days will lead to automatic block of your accounts all bonus and downlines will be deleted from your fund wallet. All our participants should ensure to recommit, that is the only way this
                                            system is going to survive and last longer. </p> -->
                                        <div class="panel-heading">
                                        <h3 class="panel-title">WE RESERVE THE RIGHT TO REVISE OUR SYSTEM RULES FROM TIME TO TIME TO ENSURE THAT THIS PLATFORM GOES SMOOTHLY AND EVERYBODY BENEFITS. WE URGE ALL OUR PARTICIPANTS TO COMPLY AND OBEY THE SYSTEM RULES. I LOVE GREENCOLLAR NETWORLD. GREENCOLLAR NETWORLD IS HERE TO STAY FOREVER!</h3>
                                    </div>
                                    </div>
                                </div>
                            </div>
                           
                            </div>
                            </div>
         
            </div>
                </div> 

                </div>

                 <?php 
               include_once('inc_footer.php'); 
               ?>

               </div>
            </div>

                <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>

        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        <script src="assets/plugins/peity/jquery.peity.min.js"></script>

        <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
        <script src="js/custom.js"></script>


        <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>


        <!-- Modal-Effect -->
        <script src="assets/plugins/custombox/js/custombox.min.js"></script>
        <script src="assets/plugins/custombox/js/legacy.min.js"></script>

        <script type="text/javascript">
    $(document).ready(function () {
        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({keys: true});
        $('#datatable-responsive').DataTable();
        $('#datatable-colvid').DataTable({
            "dom": 'C<"clear">lfrtip',
            "colVis": {
                "buttonText": "Change columns"
            }
        });
    });

</script>
    </body>
</html>