<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once("admin/inc_dbfunctions.php");

function databaseConnect()
{
    require("admin/connectionstrings.php");


    $mycon = new PDO("mysql:host=$MYSQL_Server;dbname=$MYSQL_Database;charset=utf8", "$MYSQL_Username", "$MYSQL_Password"); 
    $mycon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $mycon->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);    
    return $mycon;
}

function numToOrdinalWord($num)
{
    $first_word = array('eth','First','Second','Third','Fouth','Fifth','Sixth','Seventh','Eighth','Ninth','Tenth','Elevents',
    'Twelfth','Thirteenth','Fourteenth','Fifteenth','Sixteenth','Seventeenth','Eighteenth','Nineteenth','Twentieth');
    $second_word =array('','','Twenty','Thirty','Forty','Fifty');

    if($num <= 20)
        return $first_word[$num];

    $first_num = substr($num,-1,1);
    $second_num = substr($num,-2,1);

    return $string = str_replace('y-eth','ieth',$second_word[$second_num].'-'.$first_word[$first_num]);
}

function sendEmail($email,$subject,$message)
{
   require 'vendor/autoload.php';

	$message = "<!DOCTYPE html>
	<html>
			<head>
					<meta charset='utf-8'>
					<meta name='viewport' content='width=device-width, initial-scale=1.0'>
					<meta name='description' content='Greencollar Networld - .'>
					<meta name='author' content='Wealth Fund Global'>
	
					<link rel='shortcut icon' href='img/logo/logowfg.ico'>
	
					<title>Email Templates</title>
	
					<link href='http://greencollarnetworld.com/mod/assets/css/bootstrap.min.css' rel='stylesheet' type='text/css' />
					<link href='http://greencollarnetworld.com/mod/assets/css/core.css' rel='stylesheet' type='text/css' />
					<link href='http://greencollarnetworld.com/mod/assets/css/components.css' rel='stylesheet' type='text/css' />
					<link href='http://greencollarnetworld.com/mod/assets/css/icons.css' rel='stylesheet' type='text/css' />
					<link href='http://greencollarnetworld.com/mod/assets/css/pages.css' rel='stylesheet' type='text/css' />
					<link href='http://greencollarnetworld.com/mod/assets/css/responsive.css' rel='stylesheet' type='text/css' />
					<link href='http://greencollarnetworld.com/mod/css/style.css' rel='stylesheet' type='text/css' />
	
					
			</head>
	
	
			<body>
				<div style='max-width: 100vw; height: auto;padding: 5vw;background-color: rgb(3, 174, 67);'>
					<div style='background-color: #FFFFFF;padding: 5vw 2%'>
					<div style='display:flex;justify-content: center;text-align: center;align-content: center;align-items: center;flex-direction:row;'>
							<center><div>
								<center><img src='http://greencollarnetworld.com/mod/img/logo/logogcn.png' alt='GreenCollar_Logo' style='height: 100px;' /></center>
								<br/>
								<h1>Greencollar Networld</h1>
							</div>
							</center>
					</div>
					<hr />
					<div style='margin: 5%;'>
					". $message ."
					</div>
	
					<div style='margin: 5%;'>
						<hr />
						<center>
							<div style='display:flex;justify-content: center;text-align: center;align-content: center;align-items: center;flex-direction: row;'>
							<p>This email is automatically generated from Greencollar Networld Mailing Platform</p><br />
							<p>&copy; 2019. <a href='https://greencollarnetworld.com'>Greencollar Networld</a></p>
						</div>
						</center>
					</div>
	
					</div>
				</div>
	
			</body>
	</html>";

	$mail = new PHPMailer(true);

	try {
			//Server settings                                     // Enable verbose debug output
			$mail->isSMTP();                                            // Set mailer to use SMTP
			$mail->Host       = 'smtp.gmail.com';  // Specify main and backup SMTP servers
			$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
			$mail->Username   = 'greencollarnetworld@gmail.com';                     // SMTP username
			$mail->Password   = 'security01@';                               // SMTP password
			$mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
			$mail->Port       = 587;                                    // TCP port to connect to

			//Recipients
			$mail->setFrom('noreply@greencollarnetworld.com', 'Greencollar Networld');
			$mail->addAddress($email);     // Add a recipient
			$mail->addReplyTo('support@greencollarnetworld.com', 'Greencollar Networld Support');

			// Content
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = $subject;
			$mail->Body    = $message;
			$mail->AltBody = strip_tags($message);

			$mail->send();
			return true;
	} catch (Exception $e) {
			echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
			return true;
	}
}



$mycon = databaseConnect();


NewReceiver();

addFundBonus();

GetCycle();

Fundgrowth();

ProvideHelp();

FirstReCommitmentCheck();

GetHelp();

// MonthlyDue('1');

addReceiveAmount();

// //matching();

// membersBlock();

// matchLeftover();


// sendEmailParticipants();

failedParticipants();

//amountReferralReceivable();

//GROWTH FOR FUND
function Fundgrowth()
{
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();



    //find all donations ph with rcommmitment status as 2
    $recommitment = 2;
    $findalldonations = $dataRead->donations_getallwithrecommitment($mycon, $recommitment);
    foreach ($findalldonations as $row)
    {
        //$timedifference = (strtotime($row['createdon']) - strtotime(date("Y-m-d, H:i:s")));
        //var_dump($timedifference);
        //strtotime(date("Y-m-d H:i:s"), strtotime("+ 30 days"));
         if ((strtotime($row['readydonation_gh']) >= strtotime(date("Y-m-d H:i:s"))))
            {
                $countdays = intval(abs(strtotime($row['readydonation_gh']) - strtotime(date("Y-m-d H:i:s")))/86400);
                $datenow = date("d") + date("d", strtotime($row['createdon']));
                $totaldonation_ph = ($row['donation_ph'] + (($row['donation_ph'] * 1 * (7 - $countdays))/7));
                //update the growth colum for the donation table
                $growthupdate = $dataWrite->donations_growthupdate($mycon, $row['donation_id'], ceil($totaldonation_ph));
                if (!$growthupdate)
                {
                    echo false;
                }
                else echo true;
                
            }
            else
            {
                $totaldonation_ph = ($row['donation_ph'] * 1) + $row['donation_ph'];
                //update the growth colum for the donation table
                $growthupdate = $dataWrite->donations_growthupdate($mycon, $row['donation_id'], $totaldonation_ph);
                
            }

    }

}


function NewReceiver()
{
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();

    //find all donations that has been confirmed with status of 0 with recommitment not equal to  3
    $recommitment = 3;
    $donationsall = $dataRead->donations_getallwithunequalrecommitment($mycon, $recommitment);
    $totalbalance = 0;
    $datenow = strtotime(date("Y-m-d H:i:s"));
    
    foreach($donationsall as $row)
    {
        
            $membercheck = $dataRead->donationsreceivable_getbyidmember($mycon, $row['memberid']);
            if (!$membercheck)
            {
            // check if it is time for the user to GH
            if ((strtotime($row['readydonation_gh']) <= strtotime(date("Y-m-d H:i:s"))))
            {
                //check if the member already exists in the donations receivable table
                    //add new member to the donations receivable column
                    $addmember = $dataWrite->donationsreceivable_add($mycon, $row['member_id'], '0', '0', '0');
                    if (!$addmember) 
                    {
                        echo false;
                    }
                }
        }
    }
    return;
}

function addFundBonus()
{
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();

    $membersall = $dataRead->member_getbyall($mycon);
    $totalamount = 0;
    foreach($membersall as $row)
    {
        //reset all to zero
        $zerobalance = $dataWrite->donationsreceivable_updatezero($mycon, $row['member_id'], '0', '0', '0');
        // var_dump($zerobalance);  
        // return;
    }
    
    
    foreach($membersall as $row)
    {
        //get each donation requests
        $donationrequests = $dataRead->donations_getidmemberwithrecommitment($mycon,$row['member_id'], '2');
        foreach($donationrequests as $donation)
        {
            if ((strtotime($donation['readydonation_gh']) <= strtotime(date("Y-m-d H:i:s"))))
                    {
            if ($donationrequests != null &&  $donation['status'] == '0') //for those status already confirmed and already set to receive help
            {
               //find the donations receivable
                $membercheck = $dataRead->donationsreceivable_getbyidmember($mycon, $row['member_id']);

                //update the fund in wallet
                if ($membercheck['amount'] >= 0)
                {
                   $balance = ($donation['donation_ph'] * 1) + $donation['donation_ph'] + $membercheck['amount'] - $membercheck['withdrawn'];
                    $updatefund = $dataWrite->donationsreceivable_update($mycon, $row['member_id'], ($donation['donation_ph'] * 1) + $donation['donation_ph'] + $membercheck['amount'], $balance);


                }
                

            } 
        }  
        }
        
    }

        foreach($membersall as $row)
            {
                $totalamountreferral = 0;
                $referralbonus = 0;
                $referralbalance = 0;
                //get each donation requests
                $recommitment = 2;
                $donationrequests = $dataRead->donations_getidmemberwithrecommitment($mycon,$row['referral_id'], $recommitment);
                foreach($donationrequests as $donation)
                {
                    if ((strtotime($donation['readydonation_gh']) <= strtotime(date("Y-m-d H:i:s"))))
                    {
                        if ($donationrequests != null &&  $donation['status'] == '0')
                        {
                            //find the donations receivable by referral of someone who has Recommitted with status of 2
                            $recommitment = 2;
                            $memberdonations = $dataRead->donations_getbyidreferralwithrecommitment($mycon, $row['member_id'], $recommitment);
                            
                            //update the fund in wallet
                            if ($memberdonations['donation_ph'] >= 0 && $memberdonations['donation_ph'] != null && count($memberdonations) > 5)
                            {
                                //find the donations receivable
                                $membercheck = $dataRead->donationsreceivable_getbyidmember($mycon, $row['member_id']); 
                                $referralbonus = 2500 + ((count($memberdonations) - 5) + 500) ;
                                // $referralbonus += ($memberdonations['donation_ph'] * 0.1);
                                // $referralbonus = 5000 + ((count($memberdonations) - 5) * 500) ;
                                $referralbalance +=  $membercheck['balance'] + $referralbonus;
                                // $updatefund = $dataWrite->donationsreceivable_update($mycon, $membercheck['referral_id'], $membercheck['amount'] + $referralbonus, $referralbalance);

                            }
                        }  
                    }  
                }
                if ($referralbonus >= 5000)
                {
                    $membercheck = $dataRead->donationsreceivable_getbyidmember($mycon, $row['member_id']); 
                    $updatefund = $dataWrite->donationsreceivable_update($mycon, $membercheck['referral_id'], $membercheck['amount'] + $referralbonus, $referralbalance);
                }
                
            }

}

function addReceiveAmount()
{
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();

    //get all the receive fund request that has been merged i,e status of 3 and matched status of 5
    $fundrequestall = $dataRead->receivefundrequestall($mycon);
    foreach($fundrequestall as $row)
    {
        //find the receive donation by member
        $memberfind = $dataRead->donationsreceivable_getbyidmember($mycon, $row['member_id']);
        //update the amount
        $updateamount = $dataWrite->donationsreceivable_updatewithdrawn($mycon, $row['member_id'], $memberfind['withdrawn'] + $row['donation_gh'], $memberfind['balance'] - $row['donation_gh']);

    }
}

function ProvideHelp()
{
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();


    //get all the fund Deposit Fund request with first time status = 0 and status of 5 and member status equal to 0
    $recommitmenttype = 5;
    $providehelp_all = $dataRead->providehelp_all($mycon, '5', '0', '0', $recommitmenttype);
    

    foreach($providehelp_all as $row)
    {
        $providehelpdonation_id = $row['donation_id'];
        // 1. automatically create an associated GH for the system admin account
        // get the donation account with the type as 2 which signifes the account meant for all PH
        $type = 2;
        $bankaccountdetails = $dataRead->bankaccountdetails_one($mycon,$type);
        $member_id = $bankaccountdetails['member_id'];

        //calculate 3 days from now to transfer fund request again
        $readydonation_ph = date("Y-m-d H:i:s", strtotime("-7 days"));
        $readydonation_gh = date("Y-m-d H:i:s");
        
        //add to the donation table
        $amount = $row['donation_ph'];
        $bankaccounts = $bankaccountdetails['accountdetail_id'];
        $firsttime = 0;
        $donation_id = $dataWrite->donation_add_gh($mycon,$amount,$member_id,$readydonation_ph,$readydonation_gh,$bankaccounts,$firsttime, 'Receive Payment');
        
        if (!$donation_id)
        {
            var_dump($donation_id);
            return;
        }
        //2. get the donation details for the admin

        //3. merge the user with the admin, automatically.
        //update the matching table
        //check if the matching falls on  weekend
        if (date('D') == 'Fri' || date('D') == 'Sat')
        $expirydate = date("Y-m-d H:i:s", strtotime("+ 12 hours"));
        else $expirydate = date("Y-m-d H:i:s", strtotime("+ 12 hours"));

        //matched the particiapants to its donations
        $mycon->beginTransaction();
        $matching_id = $dataWrite->matching_add($mycon,$member_id,$row['member_id'],$donation_id,$row['donation_id'],$amount,$bankaccounts,$expirydate);
        if (!$matching_id)
        {
            $mycon->rollBack();
            var_dump($matching_id);
           return;
        }

        //change the status of the transfer particiapants to status of 3 and matched status to 5 and leftover id to 1
        $receiveupdatestatus = $dataWrite->donationsupdatestatus($mycon, $donation_id, '3', '5');

        //updates the donation id of the person that transfered
        $transferupdatestatus = $dataWrite->donationsupdatestatus($mycon, $row['donation_id'], '3', '5');
        if (!$receiveupdatestatus || !$transferupdatestatus)
        {
            $mycon->rollBack();
            var_dump($transferupdatestatus. " ----- \n \n". $receiveupdatestatus);
           return;
        }

        // add donation receivable to the admin
        //find the receive donation by member
        //check if the member already exists in the donations receivable table
        $membercheck = $dataRead->donationsreceivable_getbyidmember($mycon, $member_id);
        
        if (!$membercheck)
        {
            //add new member to the donations receivable column
            $addmember = $dataWrite->donationsreceivable_add($mycon, $member_id, '0', '0', '0');
            if (!$addmember) 
            {
                var_dump($addmember);
                return;
            }
        }

        //update the amount
        $updateamount = $dataWrite->donationsreceivable_updatewithdrawn($mycon, $member_id, 
        $amount, 0);

        if (!$receiveupdatestatus || !$transferupdatestatus)
        {
            $mycon->rollBack();
            var_dump($transferupdatestatus. " ----- \n \n". $receiveupdatestatus);
           return;
        }

        $donationsreceivable = $dataRead->donationsreceivable_getbyidmember($mycon, $member_id);

        //remove from the fund receivable
        $updatewithdrawn = $dataWrite->donationsreceivable_updatewithdrawn($mycon,$member_id,
        $amount - $amount , '0');
        if (!$updatewithdrawn)
        {
            $mycon->rollBack();
            var_dump($updatewithdrawn);
            return;
        }

        $amount = $row['donation_ph'];

        //get the name of the receiver 
        $receiverdetails = $dataRead->member_getbyid($mycon, $member_id);
        $transfermessage = "<div class='container'>
                            <p>Dear ".$row['username'].",</p>
                            <p>Kindly make a deposit to only the account details provided on your dashboard and upload the 
                            proof of payment. Amount placed is ".$amount." </p>
                            <p> Thank you.</p>
                            <p style='margin-top: 100px'><small><em>This message is auto-generated, please do not reply via your email.</em></small></p>
                        </div>";
        $receivermessage = "<div class='container'>
                            <p>Dear Admin".$receiverdetails['firstname']. " ". $receiverdetails['lastname']. ", </p>
                            <p>You have a new order to receive payment. The sender information is on your dashboard when you login. 
                            Amount to receive is ".$amount." </p>
                            <p style='margin-top: 100px'><small><em>This message is auto-generated, please do not reply via your email.</em></small></p>
                        </div>";
        if(sendEmail($row['email'],"Deposit Fund Matched - Greencollar Networld", $transfermessage) && sendEmail($receiverdetails['email'], "Receive Payment Matched - Greencollar Networld", $receivermessage)) {
            $mycon->commit();
            echo "done!";
            echo "Deposit Fund Request Matched";
        } 
        else {
            $mycon->rollBack();
            var_dump($receiverdetails);
        }
    }
}

function FirstReCommitmentCheck()
{
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();
    //1. check all the donations with status of recommit =  0 and donation status = 3 and matched status = 5


    $memberstatus = 0;
    $status = 5;
    $firsttime = 1; //FIRST PH
    $recommitmenttype = 2; //FIRST RECOMMIT
    $recommit = 0;
    //check for all the first recommitment
    $providehelp_all = $dataRead->providehelp_allfirst($mycon, '0', '5', '0', $firsttime, $recommitmenttype, $recommit);

    foreach($providehelp_all as $row)
    {
        //get the cycle of the user
        $getCycle = $dataRead->cycle_getbyid($mycon, $row['member_id']);
        if ($row['role'] != 1 && $getCyctle['cycle'] == 0)
        {
            //check if the date of donation is 5 days or more
            $recommitmentdate = strtotime(date("Y-m-d H:i:s", strtotime("now")));
            if (strtotime($row['readydonation_ph']) <= $recommitmentdate) // the ready donation ph is equal to NULL
            {
                $amount = $row['donation_ph'];
                $member_id = $row['member_id'];
    
                //calculate 5 days from for recommitment and 7 days for receiving fund
                $readydonation_ph = date("Y-m-d H:i:s", strtotime("+9 days"));
                $readydonation_gh = date("Y-m-d H:i:s", strtotime("+2 days"));
                $accountdetail_id = $row['accountdetail_id'];
                $firsttime = 0;
                // status 3 for recommitment not available to GH
                $recommitmenttype = 3;
    
    
                $mycon->beginTransaction();
                //create a new PH for the member for recommitment
                $firsttime = 1; //shows 2nd time PH
                $firststatus = 5;
                $recommitment_ph = $dataWrite->donation_add($mycon,$amount,
                $member_id,$readydonation_ph,$readydonation_gh,
                $accountdetail_id,$firsttime, $firststatus, $recommitmenttype, 'Recommitment');
                if (!$recommitment_ph)
                {
                    $mycon->rollBack();
                    return;
                }
    
                $providehelpdonation_id = $recommitment_ph;
                // 1. automatically create an associated GH for the system admin account
                // get the donation account with the type as 2 which signifes the account meant for all PH
                $readydonation_ph = date("Y-m-d H:i:s", strtotime("-9 days"));
                $readydonation_gh = date("Y-m-d H:i:s");
                $type = 2;
                $bankaccountdetails = $dataRead->bankaccountdetails_one($mycon,$type);
                $member_id = $bankaccountdetails['member_id'];
    
                //calculate 3 days from now to transfer fund request again
                $readydonation_ph = date("Y-m-d H:i:s", strtotime("-3 days"));
                $readydonation_gh = date("Y-m-d H:i:s");
                
                //add to the donation table
                $bankaccounts = $bankaccountdetails['accountdetail_id'];
                $firsttime = 0;
                $donation_id = $dataWrite->donation_add_gh($mycon,$amount,$member_id,$readydonation_ph,$readydonation_gh,$bankaccounts,$firsttime, 'Receive Payment');
                
                if (!$donation_id)
                {
                    $mycon->rollBack();
                    return;
                }
                //2. get the donation details for the admin
    
                //3. merge the user with the admin, automatically.
                //update the matching table
                //check if the matching falls on  weekend
                if (date('D') == 'Fri' || date('D') == 'Sat')
                $expirydate = date("Y-m-d H:i:s", strtotime("+ 12 hours"));
                else $expirydate = date("Y-m-d H:i:s", strtotime("+ 12 hours"));
    
                //matched the particiapants to its donations
                $matching_id = $dataWrite->matching_add($mycon,$member_id,$row['member_id'],$donation_id,$recommitment_ph,$amount,$bankaccounts,$expirydate);
                if (!$matching_id)
                {
                    $mycon->rollBack();
                    var_dump($matching_id);
                    return;
                }
    
                //update the recommit column of the donation
                $donationsupdate_recommit = $dataWrite->donationsupdate_recommit($mycon, $row['donation_id'], 5);
                if (!$donationsupdate_recommit)
                {
                    $mycon->rollBack();
                    var_dump($donationsupdate_recommit);
                    return;
                }
    
                //change the status of the transfer particiapants to status of 3 and matched status to 5 and leftover id to 1
                $receiveupdatestatus = $dataWrite->donationsupdatestatus($mycon, $donation_id, '3', '5');
    
                //updates the donation id of the person that transfered
                $transferupdatestatus = $dataWrite->donationsupdatestatus($mycon, $providehelpdonation_id, '3', '5');
                if (!$receiveupdatestatus || !$transferupdatestatus)
                {
                    $mycon->rollBack();
                    var_dump($transferupdatestatus. " ----- \n \n". $receiveupdatestatus);
                    return;
                }
    
                // add donation receivable to the admin
                //find the receive donation by member
                //check if the member already exists in the donations receivable table
                $membercheck = $dataRead->donationsreceivable_getbyidmember($mycon, $member_id);
                if (!$membercheck)
                {
                    //add new member to the donations receivable column
                    $addmember = $dataWrite->donationsreceivable_add($mycon, $member_id, '0', '0', '0');
                    if (!$addmember) 
                    {
                        var_dump($addmember);
                        return;
                    }
                }
    
                //update the amount
                $updateamount = $dataWrite->donationsreceivable_updatewithdrawn($mycon, $member_id, 
                $amount, 0);
    
                if (!$receiveupdatestatus || !$transferupdatestatus)
                {
                    $mycon->rollBack();
                    var_dump($transferupdatestatus. " ----- \n \n". $receiveupdatestatus);
                return;
                }
    
                $donationsreceivable = $dataRead->donationsreceivable_getbyidmember($mycon, $member_id);
    
                //remove from the fund receivable
                $updatewithdrawn = $dataWrite->donationsreceivable_updatewithdrawn($mycon,$member_id,
                $amount - $amount, '0');
                if (!$updatewithdrawn)
                {
                    $mycon->rollBack();
                    var_dump($updatewithdrawn);
                    return;
                }
    
                //get the name of the receiver 
                $receiverdetails = $dataRead->member_getbyid($mycon, $member_id);
                $transfermessage = "<div class='container'>
                                    <p>Dear ".$row['username'].",</p>
                                    <p>You are due to pay for First Recommitment, Kindly make payment to only the account details on your dashboard and upload the 
                                    proof of payment. Amount placed is ".$amount." </p>
                                    <p> Thank you.</p>
                                    <p style='margin-top: 100px'><small><em>This message is auto-generated, please do not reply via your email.</em></small></p>
                                </div>";
                $receivermessage = "<div class='container'>
                                    <p>Dear ".$receiverdetails['firstname']. " ". $receiverdetails['lastname']. ", </p>
                                    <p>You have a new order to receive first recommitment . The sender information is on your dashboard when you login. 
                                    Amount to receive is ".$amount." </p>
                                    <p style='margin-top: 100px'><small><em>This message is auto-generated, please do not reply via your email.</em></small></p>
                                </div>";
                if(sendEmail($row['email'],"Recommitment Matched - Greencollar Networld", $transfermessage) && sendEmail($receiverdetails['email'], "Receive Payment (Recommitment) Matched - Greencollar Networld", $receivermessage)) {
                    $mycon->commit();
                    echo "done!";
                    echo "Recommitment Request Matched";
                } 
                else {
                    $mycon->rollBack();
                    var_dump($receiverdetails);
                }
            }

        }
    }
}

function SubsequentReCommitmentCheck()
{
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();
    //1. check all the donations with status of recommit =  0 and donation status = 3 and matched status = 5


    $memberstatus = 0;
    $status = 5;
    $recommitmenttype = 2;
    //check for all the first recommitment
    $providehelp_all = $dataRead->ç($mycon, '0', '5', '0', $recommitmenttype);

    foreach($providehelp_all as $row)
    {
        //check if the date of donation is 5 days or more
        $recommitmentdate = strtotime(date("Y-m-d H:i:s", strtotime("now")));
        if (strtotime($row['readydonation_ph']) <=  $recommitmentdate) // the ready donation ph is equal to NULL
        {
            $amount = $row['donation_ph'];
            $member_id = $row['member_id'];

            //calculate 5 days from for recommitment and 7 days for receiving fund
            $readydonation_ph = date("Y-m-d H:i:s", strtotime("+12 days"));
            $readydonation_gh = date("Y-m-d H:i:s", strtotime("+7 days"));
            $accountdetail_id = $row['accountdetail_id'];
            $firsttime = 0;
            // status 3 for recommitment not available to GH
            $recommitmenttype = 3;


            $mycon->beginTransaction();
            //create a new PH for the member for recommitment
            $firsttime = 1; //shows 2nd time PH
            $recommitment_ph = $dataWrite->donation_add($mycon,$amount,
            $member_id,$readydonation_ph,$readydonation_gh,
            $accountdetail_id,$firsttime, $firststatus, $recommitmenttype, 'Subsequent Recommitment');
            if (!$recommitment_ph)
            {
                $mycon->rollBack();
                return;
            }

            $providehelpdonation_id = $recommitment_ph;
            // 1. automatically create an associated GH for the system admin account
            // get the donation account with the type as 2 which signifes the account meant for all PH
            $type = 2;
            $bankaccountdetails = $dataRead->bankaccountdetails_one($mycon,$type);
            $member_id = $bankaccountdetails['member_id'];

            //calculate 3 days from now to transfer fund request again
            $readydonation_ph = date("Y-m-d H:i:s", strtotime("-3 days"));
            $readydonation_gh = date("Y-m-d H:i:s");
            
            //add to the donation table
            $bankaccounts = $bankaccountdetails['accountdetail_id'];
            $firsttime = 0;
            $donation_id = $dataWrite->donation_add_gh($mycon,$amount,$member_id,$readydonation_ph,$readydonation_gh,$bankaccounts,$firsttime, 'Receive Payment');
            
            if (!$donation_id)
            {
                $mycon->rollBack();
                return;
            }
            //2. get the donation details for the admin

            //3. merge the user with the admin, automatically.
            //update the matching table
            //check if the matching falls on  weekend
            if (date('D') == 'Fri' || date('D') == 'Sat')
            $expirydate = date("Y-m-d H:i:s", strtotime("+ 12 hours"));
            else $expirydate = date("Y-m-d H:i:s", strtotime("+ 12 hours"));

            //matched the particiapants to its donations
            $matching_id = $dataWrite->matching_add($mycon,$member_id,$row['member_id'],$donation_id,$recommitment_ph,$amount,$bankaccounts,$expirydate);
            if (!$matching_id)
            {
                $mycon->rollBack();
                var_dump($matching_id);
                return;
            }

            //change the status of the transfer particiapants to status of 3 and matched status to 5 and leftover id to 1
            $receiveupdatestatus = $dataWrite->donationsupdatestatus($mycon, $donation_id, '3', '5');

            //updates the donation id of the person that transfered
            $transferupdatestatus = $dataWrite->donationsupdatestatus($mycon, $recommitment_ph, '3', '5');
            if (!$receiveupdatestatus || !$transferupdatestatus)
            {
                $mycon->rollBack();
                var_dump($transferupdatestatus. " ----- \n \n". $receiveupdatestatus);
                return;
            }

            // add donation receivable to the admin
            //find the receive donation by member
            //check if the member already exists in the donations receivable table
            $membercheck = $dataRead->donationsreceivable_getbyidmember($mycon, $member_id);
            if (!$membercheck)
            {
                //add new member to the donations receivable column
                $addmember = $dataWrite->donationsreceivable_add($mycon, $member_id, '0', '0', '0');
                if (!$addmember) 
                {
                    var_dump($addmember);
                    return;
                }
            }

            //update the amount
            $updateamount = $dataWrite->donationsreceivable_updatewithdrawn($mycon, $member_id, 
            $amount, 0);

            if (!$receiveupdatestatus || !$transferupdatestatus)
            {
                $mycon->rollBack();
                var_dump($transferupdatestatus. " ----- \n \n". $receiveupdatestatus);
            return;
            }

            $donationsreceivable = $dataRead->donationsreceivable_getbyidmember($mycon, $member_id);

            //remove from the fund receivable
            $updatewithdrawn = $dataWrite->donationsreceivable_updatewithdrawn($mycon,$member_id,
            $amount - $amount, '0');
            if (!$updatewithdrawn)
            {
                $mycon->rollBack();
                var_dump($updatewithdrawn);
                return;
            }

            //get the name of the receiver 
            $receiverdetails = $dataRead->member_getbyid($mycon, $member_id);
            $transfermessage = "<div class='container'>
                                <p>Dear ".$row['username'].",</p>
                                <p>You are due to pay for First Recommitment, Kindly make payment to only the account details on your dashboard and upload the 
                                proof of payment. Amount placed is ".$amount." </p>
                                <p> Thank you.</p>
                                <p style='margin-top: 100px'><small><em>This message is auto-generated, please do not reply via your email.</em></small></p>
                            </div>";
            $receivermessage = "<div class='container'>
                                <p>Dear ".$receiverdetails['firstname']. " ". $receiverdetails['lastname']. ", </p>
                                <p>You have a new order to receive first recommitment . The sender information is on your dashboard when you login. 
                                Amount to receive is ".$amount." </p>
                                <p style='margin-top: 100px'><small><em>This message is auto-generated, please do not reply via your email.</em></small></p>
                            </div>";
            if(sendEmail($row['email'],"Recommitment Matched - Greencollar Networld", $transfermessage) && sendEmail($receiverdetails['email'], "Receive Payment (Recommitment) Matched - Greencollar Networld", $receivermessage)) {
                $mycon->commit();
                echo "done!";
                echo "Recommitment Request Matched";
            } 
            else {
                $mycon->rollBack();
                var_dump($receiverdetails);
            }
        }
    }
}

function GetHelp()
{
    // 1. get all help that is ready to gh, means it has provided means within the last two days 
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();


    //get all the fund Deposit Fund request with first time status = 0 and status of 5 and member status equal to 0
    $memberstatus = 0;
    $firsttime = 0;
    $status = 5;
    $gethelp_all = $dataRead->gethelp_all($mycon, '5', '0', '0', '0');
    foreach($gethelp_all as $row)
    {
        $providehelpdonation_id = $row['donation_id'];
        // 1. automatically create an associated PH for the system admin account
        // get the donation account with the type as 2 which signifes the account meant for all PH
        $type = 2;
        $bankaccountdetails = $dataRead->bankaccountdetails_one($mycon,$type);
        $member_id = $bankaccountdetails['member_id'];

        //calculate 3 days from now to transfer fund request again
        $readydonation_ph = date("Y-m-d H:i:s", strtotime("-3 days"));
        $readydonation_gh = date("Y-m-d H:i:s");
        
        //add to the donation table
        $amount = $row['donation_gh'];
        $bankaccounts = $bankaccountdetails['accountdetail_id'];
        $firsttime = 0;
        $growth = $row['donation_gh'] * 2;
        $mycon->beginTransaction();
        $donation_id = $dataWrite->donation_add_ph($mycon,$amount,$member_id,$readydonation_ph,$readydonation_gh,$bankaccounts,$firsttime, 'Deposit Fund', $growth);
        
        if (!$donation_id)
        {
            var_dump($donation_id);
            return;
        }
        //2. get the donation details for the admin

        //3. merge the user with the admin, automatically.
        //update the matching table
        //check if the matching falls on  weekend
        if (date('D') == 'Fri' || date('D') == 'Sat')
        $expirydate = date("Y-m-d H:i:s", strtotime("+ 12 hours"));
        else $expirydate = date("Y-m-d H:i:s", strtotime("+ 12 hours"));

        //matched the particiapants to its donations
        
        $matching_id = $dataWrite->matching_add($mycon,$row['member_id'],$member_id,$row['donation_id'],$donation_id,$amount,$bankaccounts,$expirydate);
        if (!$matching_id)
        {
            $mycon->rollBack();
            var_dump($matching_id);
           return;
        }

        //change the status of the transfer particiapants to status of 3 and matched status to 5 and leftover id to 1
        $receiveupdatestatus = $dataWrite->donationsupdatestatus($mycon, $donation_id, '3', '5');

        //updates the donation id of the person that transfered
        $transferupdatestatus = $dataWrite->donationsupdatestatus($mycon, $row['donation_id'], '3', '5');
        if (!$receiveupdatestatus || !$transferupdatestatus)
        {
            $mycon->rollBack();
            var_dump($transferupdatestatus. " ----- \n \n". $receiveupdatestatus);
           return;
        }

        //update the recommit column of the donation
        $donationsupdate_recommit = $dataWrite->donationsupdate_recommit($mycon, $row['donation_id'], 5);
        if (!$donationsupdate_recommit)
        {
            $mycon->rollBack();
            var_dump($donationsupdate_recommit);
            return;
        }

        // add donation receivable to the admin
        //find the receive donation by member
        //check if the member already exists in the donations receivable table
        $membercheck = $dataRead->donationsreceivable_getbyidmember($mycon, $member_id);
        
        if (!$membercheck)
        {
            //add new member to the donations receivable column
            $addmember = $dataWrite->donationsreceivable_add($mycon, $member_id, '0', '0', '0');
            if (!$addmember) 
            {
                var_dump($addmember);
                return;
            }
        }

        //update the amount
        $updateamount = $dataWrite->donationsreceivable_updatewithdrawn($mycon, $member_id, 
        $amount, 0);

        if (!$receiveupdatestatus || !$transferupdatestatus)
        {
            $mycon->rollBack();
            var_dump($transferupdatestatus. " ----- \n \n". $receiveupdatestatus);
           return;
        }

        //update the recommitment to 3
        $recommitment = 0;
        $recommitmentupdate = $dataWrite->updateMemberDonationRecommitment($mycon, $providehelpdonation_id, $recommitment);
        if (!$recommitmentupdate)
        {
            $mycon->rollBack();
           return;
        }
        $donationsreceivable = $dataRead->donationsreceivable_getbyidmember($mycon, $member_id);

        //remove from the fund receivable
        $updatewithdrawn = $dataWrite->donationsreceivable_updatewithdrawn($mycon,$member_id,
        $amount - $amount , '0');
        if (!$updatewithdrawn)
        {
            $mycon->rollBack();
            var_dump($updatewithdrawn);
            return;
        }

        $amount = $row['donation_gh'];

        //get the name of the receiver 
        $receiverdetails = $dataRead->member_getbyid($mycon, $member_id);
        $getCycle = $dataRead->cycle_getbyid($mycon, $row['member_id']);
        $receivermessage = "<div class='container'>
                            <p>Dear ".$row['username'].",</p>
                            <p>Your ".numToOrdinalWord($getCycle['cycle'] + 1)." Cycle Payment is on its way to your account. Please  check your dashboard for more information
                            Amount to receive is ".$amount." </p>
                            <p> Thank you.</p>
                            <p style='margin-top: 100px'><small><em>This message is auto-generated, please do not reply via your email.</em></small></p>
                        </div>";
        $transfermessage = "<div class='container'>
                            <p>Dear ".$receiverdetails['firstname']. " ". $receiverdetails['lastname']. ", </p>
                            <p>There is a new order to send payment. The receiver information is on your dashboard when you login. 
                            Amount to deposit is ".$amount." </p>
                            <p style='margin-top: 100px'><small><em>This message is auto-generated, please do not reply via your email.</em></small></p>
                        </div>";
        if(sendEmail($row['email'],"Receive Payment Matched - Greencollar Networld", $receivermessage) && sendEmail($receiverdetails['email'], "Deposit Fund Matched - Greencollar Networld", $transfermessage)) {
            $mycon->commit();
            echo "done!";
            echo "Receive Payment Request Matched";
        } 
        else {
            $mycon->rollBack();
            var_dump($receiverdetails);
        }
    }
}

function matching()
{
    $dataRead = New DataRead();
    $dataWrite = New DAtaWrite();
    $mycon = databaseConnect();

    //get the new donation request with status of 5 and matchedstatus of 0 and member status equal to 0
    // get the admin details for PH and Matching
    $receivefundall = $dataRead->receiefundrequestall($mycon, '5', '0', '0');

    $totalamountreceivable = 0;
    $amount = 0;
    $difference = 0;
    
    
    foreach ($receivefundall as $row)
    {
        $receivefundperson_id = $row['donation_id'];
        //get the amount in the receivable donations
        $donationsreceivable = $dataRead->donationsreceivable_getbyidmember($mycon, $row['member_id']);
        $totalamountreceivable = $donationsreceivable['withdrawn'];


        //get the list of all the  active donations that are at least 3 days and matched status of 0, status of 5 and memberstatus of 0
        $thedate = date("Y-m-d H:i:s", strtotime("- 5 days"));
        $activetransferdonations = $dataRead->activetransferdonations($mycon, '5', '0', '0', $row['member_id'], $row['country']);
        var_dump($activetransferdonations);

        //update the matching table
        //check if the matching falls on  weekend
        if (date('D') == 'Fri' || date('D') == 'Sat')
        $expirydate = date("Y-m-d H:i:s", strtotime("+ 12 hours"));
        else $expirydate = date("Y-m-d H:i:s", strtotime("+ 36 hours"));
        //$mycon->beginTransaction();
        foreach ($activetransferdonations as $transfer)
        {
            //get the amount in the receivable donations
            $donationsreceivable = $dataRead->donationsreceivable_getbyidmember($mycon, $row['member_id']);
            $totalamountreceivable = $donationsreceivable['withdrawn'];
            var_dump($totalamountreceivable);
            var_dump($amount);
            if ($amount != 0 || $totalamountreceivable > 0)
        {
           if (strtotime($transfer['createdon']) <= strtotime($thedate) && $totalamountreceivable != 0 && $transfer['status'] == '5')
            {
                    if ($totalamountreceivable == $transfer['donation_ph'] && $transfer['leftover_id'] == 0 && $transfer['status'] == '5')
                    {
                        //matched the participants immediately with its donation ph
                        //matched the particiapants to its donations
                        $mycon->beginTransaction();
                        $matching_id = $dataWrite->matching_add($mycon,$row['member_id'],$transfer['member_id'],$row['donation_id'],$transfer['donation_id'],$transfer['donation_ph'],$row['accountdetail_id'],$expirydate);
                        if (!$matching_id)
                        {
                            $mycon->rollBack();
                            echo "Error 1";
                        }

                         //change the status of the transfer particiapants to status of 3 and matched status to 5 and leftover id to 1
                        $receiveupdatestatus = $dataWrite->donationsupdatestatus($mycon, $row['donation_id'], '3', '5');
                         
                        $transferupdatestatus = $dataWrite->donationsupdatestatus($mycon, $transfer['donation_id'], '3', '5');
                        if (!$receiveupdatestatus || !$transferupdatestatus)
                        {
                            $mycon->rollBack();
                            echo "Error 2";
                        }

                        $donationsreceivable = $dataRead->donationsreceivable_getbyidmember($mycon, $row['member_id']);

                         //remove from the fund receivable
                        $updatewithdrawn = $dataWrite->donationsreceivable_updatewithdrawn($mycon,$row['member_id'],$donationsreceivable['withdrawn'] - $transfer['donation_ph'],$donationsreceivable['amount'] - ($donationsreceivable['withdrawn'] - $transfer['donation_ph']));
                        if (!$updatewithdrawn)
                        {
                            $mycon->rollBack();
                            echo "Error 3";
                        }
                        $amount = $transfer['donation_ph'];
                        $mycon->commit();
                        echo "done!";
                        echo "Fund raiser 1";

                    }
                else if ($totalamountreceivable == $transfer['leftover'] && $transfer['leftover_id'] == 1 && $transfer['status'] == '5')
                {
                    //matched the particiapants to its donations
                    $mycon->beginTransaction();
                        $matching_id = $dataWrite->matching_add($mycon,$row['member_id'],$transfer['member_id'],$row['donation_id'],$transfer['donation_id'],$transfer['leftover'],$row['accountdetail_id'],$expirydate);
                        if (!$matching_id)
                        {
                            $mycon->rollBack();
                            echo "Error 4";
                        }


                         //change the status of the transfer particiapants to status of 3 and matched status to 5 and leftover id to 1
                        $receiveupdatestatus = $dataWrite->donationsupdatestatus($mycon, $row['donation_id'], '3', '5');
                         
                        $transferupdatestatus = $dataWrite->donationsupdatestatus($mycon, $transfer['donation_id'], '3', '5');
                        if (!$receiveupdatestatus || !$transferupdatestatus)
                        {
                            $mycon->rollBack();
                            echo "Error 5";
                        }

                        $donationsreceivable = $dataRead->donationsreceivable_getbyidmember($mycon, $row['member_id']);

                         //remove from the fund receivable
                        $updatewithdrawn = $dataWrite->donationsreceivable_updatewithdrawn($mycon,$row['member_id'],$donationsreceivable['withdrawn'] - $transfer['leftover'],$donationsreceivable['amount'] - ($donationsreceivable['withdrawn'] - $transfer['leftover']));
                        if (!$updatewithdrawn)
                        {
                            $mycon->rollBack();
                            echo "Error 6";
                        }
                        $amount = $totalamountreceivable;
                        $mycon->commit();
                        echo "done!";
                        echo "Fund raiser 2";

                        $amount = $transfer['leftover'];
                }
                elseif ($totalamountreceivable > $transfer['donation_ph'] && $transfer['leftover_id'] == 0 && $transfer['status'] == '5')
                {
                     //matched the particiapants to its donations
                       $mycon->beginTransaction();
                        $matching_id = $dataWrite->matching_add($mycon,$row['member_id'],$transfer['member_id'],$row['donation_id'],$transfer['donation_id'],$transfer['donation_ph'],$row['accountdetail_id'],$expirydate);
                        if (!$matching_id)
                        {
                            $mycon->rollBack();
                            echo "Error 7";
                        }

                         //change the status of the transfer particiapants to status of 3 and matched status to 5 and leftover id to 1
                        //$receiveupdatestatus = $dataWrite->donationsupdatestatus($mycon, $row['donation_id'], '3', '5');
                         
                        $transferupdatestatus = $dataWrite->donationsupdatestatus($mycon, $transfer['donation_id'], '3', '5');
                        if (!$transferupdatestatus)
                        {
                            $mycon->rollBack();
                            echo "Error 8";
                        }

                        $donationsreceivable = $dataRead->donationsreceivable_getbyidmember($mycon, $row['member_id']);

                         //remove from the fund receivable
                        $updatewithdrawn = $dataWrite->donationsreceivable_updatewithdrawn($mycon,$row['member_id'],$totalamountreceivable - $transfer['donation_ph'],$donationsreceivable['amount'] - ($totalamountreceivable - $transfer['donation_ph']));
                        if (!$updatewithdrawn)
                        {
                            $mycon->rollBack();
                            echo "Error 9";
                        }
                        $amount += $transfer['donation_ph'];
                        $totalamountreceivable -= $transfer['donation_ph'];
                        $mycon->commit();
                        echo "done!";
                        echo "Fund raiser 3";
                }
                elseif ($totalamountreceivable > $transfer['leftover'] && $transfer['leftover_id'] == 1 && $transfer['status'] == '5')
                {
                    //matched the particiapants to its donations
                    $mycon->beginTransaction();
                    $matching_id = $dataWrite->matching_add($mycon,$row['member_id'],$transfer['member_id'],$row['donation_id'],$transfer['donation_id'],$transfer['leftover'],$row['accountdetail_id'],$expirydate);
                    if (!$matching_id)
                    {
                        $mycon->rollBack();
                        echo "Error 10";
                    }


                     //change the status of the transfer particiapants to status of 3 and matched status to 5 and leftover id to 1
                    //$receiveupdatestatus = $dataWrite->donationsupdatestatus($mycon, $row['donation_id'], '3', '5');
                     
                    $transferupdatestatus = $dataWrite->donationsupdatestatus($mycon, $transfer['donation_id'], '3', '5');
                    if (!$transferupdatestatus)
                    {
                        $mycon->rollBack();
                        echo "Error 11";
                    }

                    $donationsreceivable = $dataRead->donationsreceivable_getbyidmember($mycon, $row['member_id']);

                     //remove from the fund receivable
                    $updatewithdrawn = $dataWrite->donationsreceivable_updatewithdrawn($mycon,$row['member_id'],$totalamountreceivable - $transfer['leftover'],$donationsreceivable['amount'] - ($totalamountreceivable - $transfer['leftover']));
                    if (!$updatewithdrawn)
                    {
                        $mycon->rollBack();
                        echo "Error 12";
                    }
                    $amount += $transfer['leftover'];
                    $totalamountreceivable -= $transfer['leftover'];
                    $mycon->commit();
                    echo "done!";
                    echo "Fund raiser 4";

                    $amount += $transfer['leftover'];
                }
                else if($totalamountreceivable < $transfer['donation_ph'] && $transfer['leftover_id'] == 0 && $transfer['status'] == '5')
                {
                    //matched the particiapants to its donations
                    $mycon->beginTransaction();
                    $matching_id = $dataWrite->matching_add($mycon,$row['member_id'],$transfer['member_id'],$row['donation_id'],$transfer['donation_id'],$totalamountreceivable,$row['accountdetail_id'],$expirydate);
                    if (!$matching_id)
                    {
                        $mycon->rollBack();
                        echo "Error 13";
                    }

                     //change the status of the transfer particiapants to status of 3 and matched status to 5 and leftover id to 1
                    $receiveupdatestatus = $dataWrite->donationsupdatestatus($mycon, $row['donation_id'], '3', '5');
                     
                    //$transferupdatestatus = $dataWrite->donationsupdatestatus($mycon, $transfer['donation_id'], '3', '5');
                    if (!$receiveupdatestatus)
                    {
                        $mycon->rollBack();
                        echo "Error 14";
                    }

                    //update the leftover
                     $leftover = $transfer['donation_ph'] - $totalamountreceivable;
                     //update the leftover of the donation
                     $leftoverupdate = $dataWrite->leftoverupdate($mycon, $transfer['donation_id'], $leftover, '1');
                     var_dump($leftover);
                     if (!$leftover)
                     {
                        $mycon->rollBack();
                        echo "Error 15";
                     }
                    $donationsreceivable = $dataRead->donationsreceivable_getbyidmember($mycon, $row['member_id']);

                     //remove from the fund receivable
                    $updatewithdrawn = $dataWrite->donationsreceivable_updatewithdrawn($mycon,$row['member_id'],$donationsreceivable['withdrawn'] - $totalamountreceivable,$donationsreceivable['amount'] - ($donationsreceivable['withdrawn'] - $totalamountreceivable));
                    if (!$updatewithdrawn)
                    {
                        $mycon->rollBack();
                        echo "Error 16";
                    }
                    $amount += $totalamountreceivable;
                    var_dump($amount);
                    $mycon->commit();
                    echo "done!";
                    echo "Fund raiser 5";
                }
                else if ($totalamountreceivable < $transfer['leftover'] && $transfer['leftover_id'] == 1 && $transfer['status'] == '5')
                {
                    //matched the particiapants to its donations
                    $mycon->beginTransaction();
                    $matching_id = $dataWrite->matching_add($mycon,$row['member_id'],$transfer['member_id'],$row['donation_id'],$transfer['donation_id'],$totalamountreceivable,$row['accountdetail_id'],$expirydate);
                    if (!$matching_id)
                    {
                        $mycon->rollBack();
                        echo "Error 17";
                    }


                     //change the status of the transfer particiapants to status of 3 and matched status to 5 and leftover id to 1
                    $receiveupdatestatus = $dataWrite->donationsupdatestatus($mycon, $row['donation_id'], '3', '5');
                     
                    //$transferupdatestatus = $dataWrite->donationsupdatestatus($mycon, $transfer['donation_id'], '3', '5');
                    if (!$receiveupdatestatus)
                    {
                        $mycon->rollBack();
                        echo "Error 18";
                    }

                    //update the leftover
                     $leftover = $transfer['leftover'] - $totalamountreceivable;
                     var_dump($leftover);
                     //update the leftover of the donation
                     $leftoverupdate = $dataWrite->leftoverupdate($mycon, $transfer['donation_id'], $leftover, '1');
                     if (!$leftoverupdate)
                     {
                        $mycon->rollBack();
                        echo "Error 19";
                     }

                    $donationsreceivable = $dataRead->donationsreceivable_getbyidmember($mycon, $row['member_id']);

                     //remove from the fund receivable
                    $updatewithdrawn = $dataWrite->donationsreceivable_updatewithdrawn($mycon,$row['member_id'],$donationsreceivable['withdrawn'] - $totalamountreceivable,$donationsreceivable['amount'] - ($donationsreceivable['withdrawn'] - $totalamountreceivable));
                    if (!$updatewithdrawn)
                    {
                        $mycon->rollBack();
                        echo "Error 20";
                    }
                    $amount += $totalamountreceivable;
                    $totalamountreceivable -= $totalamountreceivable;
                    $mycon->commit();
                    echo "done!";
                    echo "Fund raiser 6";
                }
                
                //$amount = $totalamountreceivable;
            }
        }
    }

    }


}

function MonthlyDue($cycle)
{
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();
    $dueamount = 0.2;

    //get the cycle the user is in
    $members_all = $dataRead->member_getbyall($mycon);
    foreach ($members_all as $row)
    {
        $getcycle = $dataRead->cycle_getbyid($mycon, $row['member_id']);
        if ($getcycle['cycle'] == 3)
        {
            //get the donation details of the package used

        }
    }
    





    //0. Loop through all the members
    //1. Loop through all the donations in the last 30 days attached to the member
    //2. Get the Count of each donation GH
    //3. If GH count that is successfull is equal to 3, i.e the third recycle

}

//get the cycle
function GetCycle()
{
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();
    //count the number of GH with recommitment equal to 3
    //save the count to the database
    $recommitment = 0;
    $members_all = $dataRead->member_getbyall($mycon);
    foreach ($members_all as $row)
    {
        $getalldonations_withrecommitment = $dataRead->donationsgh_getallwithrecommitment($mycon, '0', '5', '0', $row['member_id'], $recommitment);
      
        $eachcycle = count($getalldonations_withrecommitment);
        $count = 1;
        $theeachcycle = 0;
        while($count <= $eachcycle)
        {
            $theeachcycle += 1;
            if ($theeachcycle >= 4)
            {
                $theeachcycle = 0;
            }
            $count++;

        }
        //update the cycle of each member
        if ($theeachcycle == 3)
        {
            //get the last donation made by the member_id
            $donations_getid = $dataRead->donations_getbyidrecentgh($mycon, $row['member_id']);
            //create a new PH request for the user
            $amount = $donations_getid['donation_ph'] * 0.2;
            $member_id = $donations_getid['member_id'];

            //calculate 5 days from for recommitment and 7 days for receiving fund
            $readydonation_ph = date("Y-m-d H:i:s", strtotime("+4 days"));
            $readydonation_gh = date("Y-m-d H:i:s", strtotime("+2 days"));
            $accountdetail_id = $donations_getid['accountdetail_id'];
            $firsttime = 0;
            $firststatus = 0;


            $mycon->beginTransaction();
            //create a new PH for the member for recommitment
            // status 2 for recommitment to GH
            $recommitment = 0;
            $monthlydue_ph = $dataWrite->donation_add($mycon,$amount,
            $member_id,$readydonation_ph,$readydonation_gh,
            $accountdetail_id,$firsttime, $firststatus, $recommitment, '20% Monthly Due');
            if (!$monthlydue_ph)
            {
                $mycon->rollBack();
                return;
            }

            $providehelpdonation_id = $monthlydue_ph;
            // 1. automatically create an associated GH for the system admin account
            // get the donation account with the type as 2 which signifes the account meant for all PH
            $type = 3;
            $bankaccountdetails = $dataRead->bankaccountdetails_one($mycon,$type);
            $member_id = $bankaccountdetails['member_id'];

            //calculate 3 days from now to transfer fund request again
            $readydonation_ph = date("Y-m-d H:i:s", strtotime("-3 days"));
            $readydonation_gh = date("Y-m-d H:i:s");
            
            //add to the donation table
            $bankaccounts = $bankaccountdetails['accountdetail_id'];
            $firsttime = 0;
            $donation_id = $dataWrite->donation_add_gh($mycon,$amount,$member_id,$readydonation_ph,$readydonation_gh,$bankaccounts,$firsttime, 'Receive Payment');
            
            if (!$donation_id)
            {
                $mycon->rollBack();
                return;
            }
            //2. get the donation details for the admin

            //3. merge the user with the admin, automatically.
            //update the matching table
            //check if the matching falls on  weekend
            if (date('D') == 'Fri' || date('D') == 'Sat')
            $expirydate = date("Y-m-d H:i:s", strtotime("+ 12 hours"));
            else $expirydate = date("Y-m-d H:i:s", strtotime("+ 12 hours"));

            //change the status of the transfer particiapants to status of 3 and matched status to 5 and leftover id to 1
            $receiveupdatestatus = $dataWrite->donationsupdatestatus($mycon, $donation_id, '3', '5');

            //updates the donation id of the person that transfered
            $transferupdatestatus = $dataWrite->donationsupdatestatus($mycon, $recommitment_ph, '3', '5');
            if (!$receiveupdatestatus || !$transferupdatestatus)
            {
                $mycon->rollBack();
                var_dump($transferupdatestatus. " ----- \n \n". $receiveupdatestatus);
                return;
            }

            // add donation receivable to the admin
            //find the receive donation by member
            //check if the member already exists in the donations receivable table
            $membercheck = $dataRead->donationsreceivable_getbyidmember($mycon, $member_id);
            if (!$membercheck)
            {
                //add new member to the donations receivable column
                $addmember = $dataWrite->donationsreceivable_add($mycon, $member_id, '0', '0', '0');
                if (!$addmember) 
                {
                    var_dump($addmember);
                    return;
                }
            }

            //update the amount
            $updateamount = $dataWrite->donationsreceivable_updatewithdrawn($mycon, $member_id, 
            $amount, 0);

            if (!$receiveupdatestatus || !$transferupdatestatus)
            {
                $mycon->rollBack();
                var_dump($transferupdatestatus. " ----- \n \n". $receiveupdatestatus);
            return;
            }

            $donationsreceivable = $dataRead->donationsreceivable_getbyidmember($mycon, $member_id);

            //remove from the fund receivable
            $updatewithdrawn = $dataWrite->donationsreceivable_updatewithdrawn($mycon,$member_id,
            $amount - $amount, '0');
            if (!$updatewithdrawn)
            {
                $mycon->rollBack();
                var_dump($updatewithdrawn);
                return;
            }

            //get the name of the receiver 
            $receiverdetails = $dataRead->member_getbyid($mycon, $member_id);
            $transfermessage = "<div class='container'>
                                <p>Dear ".$donations_getid['username'].",</p>
                                <p>You are due to pay for 20% Monthly of your current package. Kindly pay to only the 
                                account number provided on your dashboard and upload your proof of payment.</p>
                                <p> Thank you for staying with us</p>
                                <p> Amount placed is ".$amount." </p>
                                <p> Thank you.</p>
                                <p style='margin-top: 100px'><small><em>This message is auto-generated, please do not reply via your email.</em></small></p>
                            </div>";
            $receivermessage = "<div class='container'>
                                <p>Dear Admin".$receiverdetails['firstname']. " ". $receiverdetails['lastname']. ", </p>
                                <p>You have a new order to receive 20% month due from a participant. The sender information is on your dashboard when you login. 
                                Amount to receive is ".$amount." </p>
                                <p style='margin-top: 100px'><small><em>This message is auto-generated, please do not reply via your email.</em></small></p>
                            </div>";
            if(sendEmail($donations_getid['email'],"20% Monthly Due Matched - Greencollar Networld", $transfermessage) && sendEmail($receiverdetails['email'], "Receive Payment (20% Monthly Due) Matched - Greencollar Networld", $receivermessage)) {
                $mycon->commit();
                echo "done!";
                echo "20% Monthly Due Request Matched";
            } 
            else {
                $mycon->rollBack();
                var_dump($receiverdetails);
            }

        }
        $deletecycle = $dataWrite->cycle_update($mycon, $row['member_id'], $theeachcycle);

    }
}

//create a function that will reverse incomplete PH requests


//Create a function that will be checking if $amount is equal to $receivableamount
function checkAmount($amount, $member_id)
{
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();
    $donationsreceivable = $dataRead->donationsreceivable_getbyidmember($mycon, $member_id);
}


//create a function to update the matching table
function matchingUpdate($transfer_id, $receive_id, $amount)
{
   
}

//block members that has not made any donations since the day they joined
function membersBlock()
{
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();

    //first get all member details
    $membersall = $dataRead->member_getbyall($mycon);
    //find -3 days ago
    $daysago = date("Y-m-d H:i:s", strtotime("-3 days"));
    foreach ($membersall as $row)
    {
        //checked whether they have made any donations since joined
        $donationscheck = $dataRead->donations_getbyidrecent($mycon,$row['member_id']);
        if (!$donationscheck && (strtotime($row['createdon']) < strtotime($daysago)) && $row['status'] != '0')
        {
            //block the member by updating its status to 0
            $updatememberstatus = $dataWrite->members_updatestatus($mycon, $row['member_id'], '0');
            //send message to use that account has been verified
            $sentmessage = "<div class='container'>
                                <p>Hello ".$row['username'].",</p>
                                <p>You recently registered on WTG - Greencollar Networld three days ago. And since then you haven't participated mutually, because you did not make any transfer fund request
                                on the platform.</p>
                                <p> We are not happy that you were blocked, we still want you back so you can improve the community and get benefitted. Anytime you are ready
                                 to make transfer fund request, please write to us through our support email support@greencollarnetworld.com </p> 
                                <p><small><em>This message is auto-generated, please do not reply to this email</em>M/small></p>
                            </div>";
        if (sendEmail($row['email'], 'Account Blocked! - Greencollar Networld', $sentmessage) && sendEmail('support@greencollarnetworld.com', 'One Account Blocked! - Greencollar Networld', $sentmessage))
        {
            echo "sent";
        }
        }
    }

    echo $daysago;
}



//function to get all donations that has leftover and merged the left over with random selected members
function matchLeftover()
{
    $mycon = databaseConnect();
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();

    //get all leftovers
    $leftovers = $dataRead->leftover_getall($mycon);
    $thedate = date("Y-m-d H:i:s", strtotime("-6 days"));
    $mycon->beginTransaction();
    foreach ($leftovers as $row)
    {
        //get the amount in the receivable donations
        
        $memberdetails = $dataRead->member_getbyrandommatch($mycon);
    if ($row['status'] == '5')
    {
        if (date('D') == 'Fri' || date('D') == 'Sat')
        $expirydate = date("Y-m-d H:i:s", strtotime("+5 days"));
        else $expirydate = date("Y-m-d H:i:s", strtotime("+3 days"));
        //matched the particiapants to its donations
        //get member details at random
       

        
        $firsttime = 0;
        //check if its a first time donation
        $donation_firsttime = $dataRead->donation_getbyid($mycon, $memberdetails['member_id']);
        if ($donation_firsttime)
        {
            $firsttime = 1;
        }
        
        //calculate 3 days from now to transfer fund request again
        $readydonation_ph = date("Y-m-d H:i:s", strtotime("+3 days"));
        $readydonation_gh = date("Y-m-d H:i:s");

        //add to the donation table for the PH
        //calculate 4 days from for transfer funds and 15 days for receiving fund
        $readydonation_ph_admin = date("Y-m-d H:i:s", strtotime("-4 days"));
        $readydonation_gh_admin = date("Y-m-d H:i:s", strtotime("-12 days"));
        $createdon = date("Y-m-d H:i:s", strtotime("-11 days"));
        $donation_id = $dataWrite->donation_add_admin($mycon,$row['leftover'],$memberdetails['member_id'],$readydonation_ph_admin,$readydonation_gh_admin,$memberdetails['accountdetail_id'],$firsttime, '5', $createdon);
        //add to the donation table for the GH
        $donation_id = $dataWrite->donation_add_gh($mycon,$row['leftover'],$memberdetails['member_id'],$readydonation_ph,$readydonation_gh,$memberdetails['accountdetail_id'],$firsttime, 'Receive Payment');
        if (!$donation_id)
        {
            $mycon->rollBack();
            echo "Error 1";
        }

        $matching_id = $dataWrite->matching_add($mycon,$memberdetails['member_id'],$row['member_id'],$donation_id,$row['donation_id'],$row['leftover'],$memberdetails['accountdetail_id'],$expirydate);
        if (!$matching_id)
        {
            $mycon->rollBack();
            echo "Error 2";
        }


        addFundBonus();

        addReceiveAmount();

        $donationsreceivable = $dataRead->donationsreceivable_getbyidmember($mycon, $memberdetails['member_id']);
        $totalamountreceivable = $donationsreceivable['withdrawn'];
        var_dump($totalamountreceivable);

         //change the status of the transfer particiapants to status of 3 and matched status to 5 and leftover id to 1
        $receiveupdatestatus = $dataWrite->donationsupdatestatus($mycon, $donation_id, '3', '5');
         
        $transferupdatestatus = $dataWrite->donationsupdatestatus($mycon, $row['donation_id'], '3', '5');
        if (!$receiveupdatestatus || !$transferupdatestatus)
        {
            $mycon->rollBack();
            echo "Error 3";
        }

        $donationsreceivable = $dataRead->donationsreceivable_getbyidmember($mycon, $memberdetails['member_id']);

        
         //remove from the fund receivable
        $updatewithdrawn = $dataWrite->donationsreceivable_updatewithdrawn($mycon,$memberdetails['member_id'],$donationsreceivable['withdrawn'] - $row['leftover'],$donationsreceivable['amount'] - ($donationsreceivable['withdrawn'] - $row['leftover']));
        if (!$updatewithdrawn)
        {
            $mycon->rollBack();
            echo "Error 4";
        }
        //update the leftover
         //update the leftover of the donation
         $leftoverupdate = $dataWrite->leftoverupdate($mycon, $row['donation_id'], '0', '1');
         if (!$leftoverupdate)
         {
            $mycon->rollBack();
            echo "Error 5";
         }
        $amount = $totalamountreceivable;
        $mycon->commit();
        echo "done!";
        echo "Update merging";
    }
}

}

//send email to participants to inform them of their order
function sendEmailParticipants()
{
    $mycon = databaseConnect();
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();


    //get the details of all the members
    $memberdetails = $dataRead->member_getbyall($mycon);
    foreach($memberdetails as $row)
    {
        //get the matching details if available
        $matchingdetails = $dataRead->matching_transfer_getbyidmemeber($mycon, $row['status'], $row['member_id']);
        var_dump($memberdetails);
        foreach($memberdetails as $match)
        {
            if ($match['sendemail'] == 0)
            {
                 $transferdetails = $dataRead->member_getbyid($mycon, $match['transfer_id']);
            $receiverdetails = $dataRead->member_getbyid($mycon, $match['receive_id']);
            $transfermessage = "<div class='container'>
                                <p>Hello ".$transferdetails['username'].",</p>
                                <p>You have a new request update. Deposit the sum of ".$match['amount']." to ".$receiverdetails['firstname']." ".$receiverdetails['lastname']."..
                                Quickly login to your dashboard to see more details.</p>
                                <p>Ensure you full this request on or before the expiry time ".formatDate($match['expirydate'], "yes").". </p>
                                <p> For any complains or infomation, send us an email to support@greencollarnetworld.com or us our livechat installed on our website.</p>
                                 <p>please <a href='https://www.greencollarnetworld.com'>login to your office</a> to check. </p>
                                 <p>Regards</p>
                                <p><small><em>This message is auto-generated, please do not reply via your email.</em></small></p>
                           </div>";
            $receivemessage = "<div class='container'>
                                <p>Hello ".$receiverdetails['username'].",</p>
                                <p>You have a new request update. The sum of ".$match['amount']." from ".$transferdetails['firstname']." ".$transferdetails['lastname']."..
                                Quickly login to your dashboard to see more details.</p>
                                <p>Ensure you full this request on or before the expiry time ".formatDate($match['expirydate'], "yes").". </p>
                                <p> For any complains or infomation, send us an email to support@greencollarnetworld.com or us our livechat installed on our website.</p>
                                 <p>please <a href='https://www.greencollarnetworld.com'>login to your office</a> to check. </p>
                                 <p>Regards</p>
                                <p><small><em>This message is auto-generated, please do not reply via your email.</em></small></p>
                            </div>";
            if (sendEmail($transferdetails['email'], "Transfer Order Matched - Greencollar Networld", $transfermessage) && sendEmail($receiverdetails['email'], "Receive Order Matched - Greencollar Networld", $receivemessage))
            {
                //update the matching send email to 1
                $updateemail = $dataWrite->matching_updateemail($mycon, $match['matching_id'], '1');
                echo "Sent";
            }
            }
        }
    }
}


//block participants who failed to pay and make a remerge
function failedParticipants()
{
    $mycon = databaseConnect();
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();

    //find all the matching details
    $todaydate = date("Y-m-d H:i:s");
    $matchingdetails = $dataRead->matching_getallactivestatus($mycon, '5');
    $mycon->beginTransaction();
    foreach ($matchingdetails as $row)
    {
       if (strtotime($row['expirydate']) < strtotime($todaydate) && $row['status'] == 5)
       {
           //get the transfer details and block the participants
           $transferdetails = $dataRead->member_getbyid($mycon, $row['transfer_id']);
           if ($transferdetails['role'] != 1)
           {
                //update the participants status to 10 to show blocked member
                $updatestatus = $dataWrite->members_updatestatus($mycon, $transferdetails['member_id'], '10');
                if (!$updatestatus)
                {
                    $mycon->rollBack();
                    echo "failed update 1";
                }
    
                //send email to participants to inform their present status
                $message = "<div class='container'>
                                    <p>Deat ".$transferdetails['username'].",</p>
                                    <p>We are sorry to inform you that due to failure to complete your 
                                    Deposit Fund order details, hence the system has automatically suspended your account for failure to pay within the specified time. </p>
                                    <p>If you are ready to participate mutually and wants your accounts running again, please login and pay the penalty fee 
                                    to reopen your account again. </p>
                                    
                                    <p>For more information, please contact support at support@greencollarnetworld.com or use the livechat.</p>
                                    <p>Regards.</p>
                                    <p><small><em>This message is auto-generated, please do not reply via your email.</em></small></p>
                                </div>";
                if (sendEmail($transferdetails['email'], "Account Suspended - Greencollar Metworld", $message))
                {
                    //delete the donation both for the admin and the PH
                    $deletetransfer_donation = $dataWrite->donation_delete($mycon, $row['transferfund_id']);
                    if (!$deletetransfer_donation)
                    {
                        $mycon->rollBack();
                        echo "Undeleted Donation 1";
                    }
    
                    //delete the donation for the admin
                    $deletetransfer_donation = $dataWrite->donation_delete($mycon, $row['receivefund_id']);
                    if (!$deletetransfer_donation)
                    {
                        $mycon->rollBack();
                        echo "Deleted Donation 2";
                    }
    
                    //delete the matching 
                    $deletematching = $dataWrite->matching_deletebyid ($mycon, $row['matching_id']);
                    if (!$deletematching)
                    {
                        $mycon->rollBack();
                        echo "Deleted Matching";
                    }

                }
 
            }
       }

                
    }
    $mycon->commit();     

}





?>