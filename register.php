<?php

require_once("admin/config.php");
require_once("admin/inc_dbfunctions.php");

$mycon = databaseConnect();
$dataRead = New DataRead();


?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content=" Earn Up to 30%  ROI on Every Deposit Made after 7 days. Greencollar Networld">
		<meta name="author" content="Greencollar Networld">

		<link rel="shortcut icon" href="img/logo/logogcn.ico">

		<title>Become a Member | <?php echo pageTitle(); ?></title>

		<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="css/style.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>

	</head>
	<body>

		<div class="account-pages login-register-background"></div>
		<div class="clearfix"></div>
		<div class="wrapper-page">
			<div class=" card-box">
				<div class="panel-heading">
					<div class="text-center">
					<a href="../index.php"><img src="img/logo/logogcn.png" alt="" width="70px" height="70px"></a>
				</div>
					<h3 class="text-center"><span class="waves-effect waves-light" style="color: darkgreen"> Create an acount  </span></h3>
				</div>

				<div class="panel-body" id="step1">
						<p class="text-left text-primary is-required" > * is required </p>
						<div id="result"></div>
					<form class="form-horizontal m-t-20" action="admin/actionmanager.php" id="registerform">
						<div class="form-group">
							<div class="col-xs-12 col-md-12 error" id="referraldiv">
								<input class="form-control" name="referral" type="text" id="referral" placeholder="Referral Username or email" value=<?php if (getCookie("ref") != null) echo getCookie("ref"); ?>>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-12 error" id="firstnamediv">
								<input class="form-control" name="firstname" type="text" id="firstname" placeholder="Firstname*">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-12 error" id="lastnamediv">
								<input class="form-control" type="text" name="lastname" id="lastname" placeholder="Lastname*">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-12 error" id="usernamediv">
								<input class="form-control" type="text" name="username" id="username" placeholder="Username*">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-12 col-xs-12 error" id="phonenumberdiv">
								<input class="form-control" type="text" name="phonenumber" id="phonenumber" placeholder="Phone number*">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-12 error" id="emaildiv">
								<input class="form-control" type="email" name="email" id="email" placeholder="Email*">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-12 error" id="passworddiv">
								<input class="form-control" type="password" name="password" id="password" placeholder="Password*">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-12 error" id="confirmpassworddiv">
								<input class="form-control" type="password" name="confirmpassword" id="confirmpassword" placeholder="Repeat Password*">
							</div>
						</div>
						<div class="form-group ">
							<div class="col-xs-12">
								<div class="col-md-4">
									<img src="captcha.php" height="35px" /><br>
								</div><span class="visible-xs"><br></span>
								<div class="col-md-8 error" id="captchadiv">
									<input class="form-control" type="text"  name="captcha" id="captcha" placeholder="Enter captcha here">
								</div>
							</div> 
						</div>

						<div class="form-group text-center m-t-40">
							<div class="col-xs-12">
								<div class="col-md-8 col-xs-8">
									<button class="btn btn-custom btn-block text-uppercase waves-effect waves-light"  id="createaccountbutton" type="submit">
									Register
									</button>
								</div>
								<div class="col-md-4 col-xs-4">
									<button class="btn btn-danger btn-block text-uppercase waves-effect waves-light" id="resetbutton" type="button">
									Reset
									</button>
								</div>
							</div>
						</div>

					</form>

				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 text-center">
					<p class="login-footer" style="padding: 10px;">
						Already a member?<a href="login.php" class="text-primary m-l-5"><b>Log In</b></a>
					</p>
				</div>
			</div>

		</div>

		<script>
			var resizefunc = [];
		</script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5d128ba436eab972111933b2/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

		<!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>


        <script src="assets/js/jquery.core.js"></script>
        <script src="js/custom.js"></script>

	</body>
</html>