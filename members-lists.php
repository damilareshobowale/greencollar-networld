<?php
require_once("admin/config.php");
require_once("admin/inc_dbfunctions.php");

$mycon = databaseConnect();
$dataRead = New DataRead();

$currentuserid = getCookie("userid");

$allmemberdetails = $dataRead->member_getbyall($mycon);

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Greencollar Networld - Earn 30% ROI after 7 days on every fund you invest.">
        <meta name="author" content="Greencollar Networld">

        <link rel="shortcut icon" href="img/logo/logowfg.ico">

        <title>All Member Registered - <?php echo pageTitle(); ?></title>


        <link href="assets/plugins/custombox/css/custombox.css" rel="stylesheet">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="css/style.css" rel="stylesheet" type="text/css" />


        <link href="assets/plugins/bootstrap-table/css/bootstrap-table.min.css" rel="stylesheet" type="text/css" />


        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>
        
    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

               <?php include_once('inc_header.php') ?>

               <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">All Members Information</h4>
                                <p class="text-muted page-title-alt">Welcome <?php echo getCookie("fullname") ?>!</p>
                            </div>
                        </div>
                        <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <h4 class="m-t-0 header-title"><b>Members' Lists</b></h4>
                            <p class="text-muted font-13 m-b-30">
                                Lists of all registered members</code>.
                            </p>

                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Full Name (Username)</th>
                                    <th>Email</th>
                                    <th>Date Joined</th>
                                    <th>Phonenumber</th>
                                    <th>Status</th>
                                    <th>Action </th>
                                </tr>
                                </thead>


                                <tbody>
                                    <?php
                                    $count = 0;
                                    foreach($allmemberdetails as $row)
                                    {
                                    
                                    ?>
                                <tr>
                                    <td><?php echo ++$count ?></td>
                                    <td><a href="members-view.php?username=<?php echo $row['username']; ?>" >
                                    <?php echo $row['firstname']." ".$row['lastname']." (".$row['username'].")"; ?></a></td>
                                    <td><a href="members-view.php?username=<?php echo $row['username']; ?>" >
                                    <?php echo $row['email'] ?></a></td>
                                    <td><?php echo formatDate($row['createdon'], "yes") ?></td>
                                    <td><?php echo $row['phonenumber'] ?></td>
                                    <td style="font-weight: bold;"><?php if ($row['status'] == '0') echo "<span class='text-custom'>Active</span>"; else if ($row['status'] == '3') echo "<span class='text-default'>Paused</span>"; else if($row['status'] == '10') echo "<span class='text-primary'>Activation fee pending</span>"; else if($row['status'] == '5') echo "<span class='text-danger'>Suspend</span>"; ?></td>
                                    <td> <a href="#suspend-modal<?php echo $row['member_id'] ?>" class="btn btn-primary waves-effect waves-light" data-animation="fadein" data-plugin="custommodal" 
                                                        data-overlaySpeed="200" data-overlayColor="#36404a"><?php if($row['status'] == 10) echo "Unsuspend"; else echo "Suspend" ?> account </a> <br /><br />
                                                        <a href="#pause-modal<?php echo $row['member_id'] ?>" class="btn btn-default waves-effect waves-light" data-animation="fadein" data-plugin="custommodal" 
                                                        data-overlaySpeed="200" data-overlayColor="#36404a"><?php if($row['status'] == 3) echo "Unpause"; else echo "Pause" ?> account</a> <br /><br />
                                                        <a href="#delete-modal<?php echo $row['member_id'] ?>" class="btn btn-danger waves-effect waves-light" data-animation="fadein" data-plugin="custommodal" 
                                                        data-overlaySpeed="200" data-overlayColor="#36404a"><?php if($row['status'] == 8) echo "Undelete"; else echo "Delete" ?> account</a>
                                                        </td>
                                </tr>
                                <div id="suspend-modal<?php echo $row['member_id']; ?>" class="modal-demo">
                                <button type="button" class="close" onclick="Custombox.close();">
                                    <span>&times;</span><span class="sr-only">Close</span>
                                </button>
                                <h4 class="custom-modal-title bg-primary">Suspend account</h4>
                                <div class="custom-modal-text">
                                <div id="<?php if ($row['status'] == 10) echo 'unsuspend'; else echo 'suspend' ?>result<?php echo $row['member_id'] ?>"></div>
                                   Are you sure you want to <?php if ($row['status'] == 10) echo 'unsuspend'; else echo 'suspend' ?> <?php echo $row['firstname']; ?>'s account?
                                   <div class="form-group extend error" id="extendpassworddiv<?php echo $row['member_id'] ?>"> 
                                        <label for="extendpassword" class="control-label">Enter your password*</label> 
                                        <input type="password" class="form-control" name="extendpassword" id="<?php if ($row['status'] == 10) echo 'unsuspend'; else echo 'suspend' ?><?php echo $row['member_id'] ?>" placeholder="Enter password  ">
                                        <input type="hidden" class="form-control" name="command" id="command<?php echo $row['member_id'] ?>" value="<?php echo $row['member_id'] ?>">
                                    </div>
                                </div>
                                <div class="modal-footer"> 
                                    <button type="button" class="btn btn-custom waves-effect waves-light" type="button"  onclick="accountStatus(<?php echo $row['member_id'] ?>, '<?php if ($row['status'] == 10) echo 'unsuspend'; else echo 'suspend' ?>')">Yes</button>
                                    <button type="button" class="btn btn-danger waves-effect" onclick="Custombox.close();">No</button>
                                </div> 
                             </div>
                             <div id="pause-modal<?php echo $row['member_id']; ?>" class="modal-demo">
                                <button type="button" class="close" onclick="Custombox.close();">
                                    <span>&times;</span><span class="sr-only">Close</span>
                                </button>
                                <h4 class="custom-modal-title bg-default">Pause Account</h4>
                                <div class="custom-modal-text">
                                <div id="<?php if ($row['status'] == 3) echo 'unpause'; else echo 'pause' ?>result<?php echo $row['member_id'] ?>"></div>
                                   Are you sure you want to <?php if ($row['status'] == 3) echo 'unpause'; else echo 'pause' ?> <?php echo $row['firstname']; ?>'s account?
                                   <div class="form-group extend error" id="extendpassworddiv<?php echo $row['member_id'] ?>"> 
                                        <label for="extendpassword" class="control-label">Enter your password*</label> 
                                        <input type="password" class="form-control" name="extendpassword" id="<?php if ($row['status'] == 3) echo 'unpause'; else echo 'pause' ?><?php echo $row['member_id'] ?>" placeholder="Enter password to pause this account">
                                        <input type="hidden" class="form-control" name="command" id="command<?php echo $row['member_id'] ?>" value="<?php echo $row['member_id'] ?>">
                                    </div> 
                                </div>
                                <div class="modal-footer"> 
                                    <button type="button" class="btn btn-custom waves-effect waves-light" type="button"  onclick="accountStatus(<?php echo $row['member_id'] ?>, '<?php if ($row['status'] == 3) echo 'unpause'; else echo 'pause' ?>')" >Yes</button>
                                    <button type="button" class="btn btn-danger waves-effect" onclick="Custombox.close();">No</button>
                                </div> 
                             </div>
                             <div id="delete-modal<?php echo $row['member_id']; ?>" class="modal-demo">
                                <button type="button" class="close" onclick="Custombox.close();">
                                    <span>&times;</span><span class="sr-only">Close</span>
                                </button>
                                <h4 class="custom-modal-title bg-danger">Delete Account</h4>
                                <div class="custom-modal-text">
                                <div id="deleteresult<?php echo $row['member_id'] ?>"></div>
                                   Are you sure you want to delete <?php echo $row['firstname']; ?>'s account?
                                   <div class="form-group extend error" id="extendpassworddiv<?php echo $row['member_id'] ?>"> 
                                        <label for="extendpassword" class="control-label">Enter your password*</label> 
                                        <input type="password" class="form-control" name="extendpassword" id="delete<?php echo $row['member_id'] ?>" placeholder="Enter password to delete this account">
                                        <input type="hidden" class="form-control" name="command" id="command<?php echo $row['member_id'] ?>" value="<?php echo $row['member_id'] ?>">
                                    </div> 
                                </div>
                                <div class="modal-footer"> 
                                    <button type="button" class="btn btn-custom waves-effect waves-light" type="button"  onclick="accountStatus(<?php echo $row['member_id'] ?>, 'delete')">Yes</button>
                                    <button type="button" class="btn btn-danger waves-effect" onclick="Custombox.close();">No</button>
                                </div> 
                             </div>
                                <?php
                                } 
                                ?>
                                 <tr>
                                    <td>Total Members</td>
                                    <td><?php echo $count ?></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                            </div>
                            </div>
         
            </div>
                </div> 

                </div>

                 <?php 
               include_once('inc_footer.php'); 
               ?>

               </div>
            </div>

                <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>

        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        <script src="assets/plugins/peity/jquery.peity.min.js"></script>

        <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
        <script src="js/custom.js"></script>


        <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>


        <!-- Modal-Effect -->
        <script src="assets/plugins/custombox/js/custombox.min.js"></script>
        <script src="assets/plugins/custombox/js/legacy.min.js"></script>

        <script type="text/javascript">
    $(document).ready(function () {
        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({keys: true});
        $('#datatable-responsive').DataTable();
        $('#datatable-colvid').DataTable({
            "dom": 'C<"clear">lfrtip',
            "colVis": {
                "buttonText": "Change columns"
            }
        });
    });

</script>
    </body>
</html>