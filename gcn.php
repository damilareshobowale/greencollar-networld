<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once("admin/inc_dbfunctions.php");
$dataRead = New DataRead();

function databaseConnect()
{
    require("admin/connectionstrings.php");


    $mycon = new PDO("mysql:host=$MYSQL_Server;dbname=$MYSQL_Database;charset=utf8", "$MYSQL_Username", "$MYSQL_Password"); 
    $mycon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $mycon->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);    
    return $mycon;
}

function numToOrdinalWord($num)
{
    $first_word = array('eth','First','Second','Third','Fouth','Fifth','Sixth','Seventh','Eighth','Ninth','Tenth','Elevents',
    'Twelfth','Thirteenth','Fourteenth','Fifteenth','Sixteenth','Seventeenth','Eighteenth','Nineteenth','Twentieth');
    $second_word =array('','','Twenty','Thirty','Forty','Fifty');

    if($num <= 20)
        return $first_word[$num];

    $first_num = substr($num,-1,1);
    $second_num = substr($num,-2,1);

    return $string = str_replace('y-eth','ieth',$second_word[$second_num].'-'.$first_word[$first_num]);
}

function sendEmail($email,$subject,$message)
{
   require 'vendor/autoload.php';

	$message = "<!DOCTYPE html>
	<html>
			<head>
					<meta charset='utf-8'>
					<meta name='viewport' content='width=device-width, initial-scale=1.0'>
					<meta name='description' content='Greencollar Networld - .'>
					<meta name='author' content='Wealth Fund Global'>
	
					<link rel='shortcut icon' href='img/logo/logowfg.ico'>
	
					<title>Email Templates</title>
	
					<link href='http://greencollarnetworld.com/mod/assets/css/bootstrap.min.css' rel='stylesheet' type='text/css' />
					<link href='http://greencollarnetworld.com/mod/assets/css/core.css' rel='stylesheet' type='text/css' />
					<link href='http://greencollarnetworld.com/mod/assets/css/components.css' rel='stylesheet' type='text/css' />
					<link href='http://greencollarnetworld.com/mod/assets/css/icons.css' rel='stylesheet' type='text/css' />
					<link href='http://greencollarnetworld.com/mod/assets/css/pages.css' rel='stylesheet' type='text/css' />
					<link href='http://greencollarnetworld.com/mod/assets/css/responsive.css' rel='stylesheet' type='text/css' />
					<link href='http://greencollarnetworld.com/mod/css/style.css' rel='stylesheet' type='text/css' />
	
					
			</head>
	
	
			<body>
				<div style='max-width: 100vw; height: auto;padding: 5vw;background-color: rgb(3, 174, 67);'>
					<div style='background-color: #FFFFFF;padding: 5vw 2%'>
					<div style='display:flex;justify-content: center;text-align: center;align-content: center;align-items: center;flex-direction:row;'>
							<center><div>
								<center><img src='http://greencollarnetworld.com/mod/img/logo/logogcn.png' alt='GreenCollar_Logo' style='height: 100px;' /></center>
								<br/>
								<h1>Greencollar Networld</h1>
							</div>
							</center>
					</div>
					<hr />
					<div style='margin: 5%;'>
					". $message ."
					</div>
	
					<div style='margin: 5%;'>
						<hr />
						<center>
							<div style='display:flex;justify-content: center;text-align: center;align-content: center;align-items: center;flex-direction: row;'>
							<p>This email is automatically generated from Greencollar Networld Mailing Platform</p><br />
							<p>&copy; 2019. <a href='https://greencollarnetworld.com'>Greencollar Networld</a></p>
						</div>
						</center>
					</div>
	
					</div>
				</div>
	
			</body>
	</html>";

	$mail = new PHPMailer(true);

	try {
			//Server settings                                     // Enable verbose debug output
			$mail->isSMTP();                                            // Set mailer to use SMTP
			$mail->Host       = 'smtp.gmail.com';  // Specify main and backup SMTP servers
			$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
			$mail->Username   = 'greencollarnetworld@gmail.com';                     // SMTP username
			$mail->Password   = 'security01@';                               // SMTP password
			$mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
			$mail->Port       = 587;                                    // TCP port to connect to

			//Recipients
			$mail->setFrom('noreply@greencollarnetworld.com', 'Greencollar Networld');
			$mail->addAddress($email);     // Add a recipient
			$mail->addReplyTo('support@greencollarnetworld.com', 'Greencollar Networld Support');

			// Content
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = $subject;
			$mail->Body    = $message;
			$mail->AltBody = strip_tags($message);

			$mail->send();
			return true;
	} catch (Exception $e) {
			// echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
			return true;
	}
}



$mycon = databaseConnect();

function monthlyFeePecentate() {
    $mycon = databaseConnect();
    $dataRead = New DataRead();
    $monthlydue = $dataRead->getMonthlydue($mycon);
    $monthlyfee = $monthlydue['amount']/100;
    $monthlyfeepercentage = $monthlydue['amount'] .'%';

    return $monthlyfeepercentage;
}

function monthlyFee() {
    $mycon = databaseConnect();
    $dataRead = New DataRead();
    $monthlydue = $dataRead->getMonthlydue($mycon);
    $monthlyfee = $monthlydue['amount']/100;

    return $monthlyfee;
}





GetCycle();

NewReceiver();


addFundBonus();

addFundBonus2();

addFundBonus3();


addReceiveAmount();


function NewReceiver()
{
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();

    //find all donations that has been confirmed with status of 0 with recommitment not equal to  3
    $recommitment = 2;
    $donationsall = $dataRead->donations_getallwithunequalrecommitment($mycon, $recommitment);
    $totalbalance = 0;
    $datenow = strtotime(date("Y-m-d H:i:s"));
    
    foreach($donationsall as $row)
    {
        
            $membercheck = $dataRead->donationsreceivable_getbyidmember($mycon, $row['memberid']);
            if (!$membercheck)
            {
            // check if it is time for the user to GH
            if ((strtotime($row['readydonation_gh']) <= strtotime(date("Y-m-d H:i:s"))))
            {
                //check if the member already exists in the donations receivable table
                    //add new member to the donations receivable column
                    $addmember = $dataWrite->donationsreceivable_add($mycon, $row['member_id'], '0', '0', '0');
                    if (!$addmember) 
                    {
                        // echo false;
                    }
                }
        }
    }
    return;
}

function addFundBonus()
{
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();

    $membersall = $dataRead->member_getbyall($mycon);
    $totalamount = 0;
    foreach($membersall as $row)
    {
        //reset all to zero
        $zerobalance = $dataWrite->donationsreceivable_updatezero($mycon, $row['member_id'], '0', '0', '0');
        // // var_dump($zerobalance);  
        // return;
    }
}
 
 
 
 function addFundBonus2()
 {
     $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();
    $membersall = $dataRead->member_getbyall($mycon);
    foreach($membersall as $row)
    {
        //get each donation requests
        $donationrequests = $dataRead->donations_getidmemberwithrecommitment($mycon,$row['member_id'], '2');
        // var_dump($donationrequests);
        $recommitmentamount = 0;
        //get the donation made with recomment equal to 3
        $recommitmentrequest = $dataRead->donations_getidmemberwithrecommitmentone($mycon,$row['member_id'], '3');
        foreach($donationrequests as $donation)
        {
            if ((strtotime($donation['readydonation_gh']) <= strtotime(date("Y-m-d H:i:s"))))
                    {
            if ($donationrequests != null &&  $donation['status'] == '0') //for those status already confirmed and already set to receive help
            {
               //find the donations receivable
                $membercheck = $dataRead->donationsreceivable_getbyidmember($mycon, $row['member_id']);

                $monthlyfeepercentage = monthlyFeePecentate();
                
                //update the fund in wallet
                if ($membercheck['amount'] >= 0 && $donation['type'] != 'Monthly Due')
                {
                   $balance = ($donation['donation_ph'] * 1.3) - $membercheck['withdrawn'] - $donation['donation_gh'] + $recommitmentamount;
                    $updatefund = $dataWrite->donationsreceivable_update($mycon, $membercheck['member_id'], ($donation['donation_ph'] * 1.3) + $membercheck['amount'] + ($recommitmentamount * 2), $balance);


                }
                

            } 
        }  
        }
        
        
    }
}

function addFundBonus3() 
{
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();
    $membersall = $dataRead->member_getbyall($mycon);
    
    foreach($membersall as $row)
    {
        $totalamountreferral = 0;
        $referralbonus = 0;
        $referralbalance = 0;
        
    }
}

function addReceiveAmount()
{
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();

    $fundrequestall = $dataRead->receivefundrequestall($mycon);
    foreach($fundrequestall as $row)
    {
        $recommitmentamount = 0;
        $recommitmentrequest = $dataRead->donations_getidmemberwithrecommitmentonedonation($mycon,$row['donation_id'], 'Receive First Recommitment');
        if ($recommitmentrequest != false) 
        {   
            $recommitmentamount = $recommitmentrequest['donation_gh'];

        }
        $memberfind = $dataRead->donationsreceivable_getbyidmember($mycon, $row['member_id']);
        $updateamount = $dataWrite->donationsreceivable_updatewithdrawn($mycon, $row['member_id'], $row['donation_gh'] + $memberfind['withdrawn'], $memberfind['amount'] - $row['donation_gh'] - $memberfind['withdrawn']);

    }
}


//return the cycle type
function returnCycle($cycle, $member_id)
{
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();
    if ($cycle == 0) 
    {
        $index = 0;
        $updatecycle = $dataWrite->cycle_updatelevel($mycon, $member_id, $index, 0);
        return $index;
    }
    else if (fmod($cycle, 4) == 0)
    {
        $index = 4;
        $updatecycle = $dataWrite->cycle_updatelevel($mycon, $member_id, $index, 0);
        return $index;
    }
    else if (fmod($cycle, 4) == 2)
    {
        $index = 2;
        $updatecycle = $dataWrite->cycle_updatelevel($mycon, $member_id, $index, 0);
        return $index;
    }
    else if (fmod($cycle, 4) == 3)
    {
        $index = 3;
        $updatecycle = $dataWrite->cycle_updatelevel($mycon, $member_id, $index, 1);
        return $index;
    }
    else 
    {
        $index = 1;
        $updatecycle = $dataWrite->cycle_updatelevel($mycon, $member_id, $index, 0);
        return $index;
    }
}

function GetCycle()
{
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();
    $recommitment = 2;
    $members_all = $dataRead->member_getbyall($mycon);
    foreach ($members_all as $row)
    {
        $getalldonations_withrecommitment = $dataRead->donationsph_getallwithoutrecommitmentnumber($mycon, $row['member_id']);
      
        $eachcycle = count($getalldonations_withrecommitment);
        // echo $eachcycle;
        $count = 0;
        $theeachcycle = 0;
        while($count <= $eachcycle)
        {
            $theeachcycle += 1;
            
            $count++;

        }
    //  echo $eachcycle. "<br>";
    $updatecycle = $dataWrite->cycle_update($mycon, $row['member_id'], $theeachcycle);
    }

   
    $members_all = $dataRead->member_getbyall($mycon);
    foreach ($members_all as $row)
    {
    //get the cycle
    $getcycleofmember = $dataRead->cycle_getbyid($mycon, $row['member_id']);
    // var_dump($getcycleofmember);
    //update the cycle of each member
    $cyclenumber = returnCycle($getcycleofmember['cycle'], $row['member_id']);
    
        if ($row['role'] != 1 && $getcycleofmember['status'] != 0)
        {
            $monthlyfeepercentage = monthlyFeePecentate();
            
            //get the last donation made by the member_id
            $donations_getid = $dataRead->donations_getbyidrecentconfirmedph($mycon, $row['member_id']);
            if ($donations_getid['type'] !== 'Monthly Due' && $donations_getid != false && $donations_getid['status'] == 0)
            {
                // // echo 'this '.$cyclenumber;
                //create a new PH request for the user
            $monthlyfee = monthlyFee();
            $amount = $donations_getid['donation_ph'] * $monthlyfee;
            $member_id = $row['member_id'];

            //calculate 5 days from for recommitment and 7 days for receiving fund
            $readydonation_ph = date("Y-m-d H:i:s", strtotime("+4 days"));
            $readydonation_gh = date("Y-m-d H:i:s", strtotime("+2 days"));
            $accountdetail_id = $donations_getid['accountdetail_id'];
            $firsttime = 0;
            $firststatus = 0;
            


            $mycon->beginTransaction();
            //create a new PH for the member for recommitment
            // status 2 for recommitment to GH
            $recommitment = 0;
            $monthlyfeepercentage = monthlyFeePecentate();
            $monthlydue_ph = $dataWrite->donation_add($mycon,$amount,
            $member_id,$readydonation_ph,$readydonation_gh,
            $accountdetail_id,$firsttime, $firststatus, $recommitment, 'Monthly Due');
            if (!$monthlydue_ph)
            {
                $mycon->rollBack();
                return;
            }

            $providehelpdonation_id = $monthlydue_ph;
            // 1. automatically create an associated GH for the system admin account
            // get the donation account with the type as 2 which signifes the account meant for all PH
            $type = 3;
            $bankaccountdetails = $dataRead->bankaccountdetails_one($mycon,$type);
            $member_id = $bankaccountdetails['member_id'];

            //calculate 3 days from now to transfer fund request again
            $readydonation_ph = date("Y-m-d H:i:s", strtotime("-3 days"));
            $readydonation_gh = date("Y-m-d H:i:s");
            
            //add to the donation table
            $bankaccounts = $bankaccountdetails['accountdetail_id'];
            $firsttime = 0;
            $donation_id = $dataWrite->donation_add_gh($mycon,$amount,$member_id,$readydonation_ph,$readydonation_gh,$bankaccounts,$firsttime, 'Receive Payment');
            
            if (!$donation_id)
            {
                $mycon->rollBack();
                return;
            }
            //2. get the donation details for the admin

            //3. merge the user with the admin, automatically.
            
            //update the matching table
            //check if the matching falls on  weekend
            if (date('D') == 'Fri' || date('D') == 'Sat')
            $expirydate = date("Y-m-d H:i:s", strtotime("+ 12 hours"));
            else $expirydate = date("Y-m-d H:i:s", strtotime("+ 12 hours"));
            $matching_id = $dataWrite->matching_add($mycon,$member_id,$donations_getid['member_id'],$donation_id,$monthlydue_ph,$amount,$bankaccounts,$expirydate);
            if (!$matching_id)
            {
                $mycon->rollBack();
                // var_dump($matching_id);
               return;
            }
            //change the status of the transfer particiapants to status of 3 and matched status to 5 and leftover id to 1
            $receiveupdatestatus = $dataWrite->donationsupdatestatus($mycon, $donation_id, '3', '5');

            //updates the donation id of the person that transfered
            $transferupdatestatus = $dataWrite->donationsupdatestatus($mycon, $monthlydue_ph, '3', '5');
            if (!$receiveupdatestatus || !$transferupdatestatus)
            {
                $mycon->rollBack();
                // var_dump($transferupdatestatus. " ----- \n \n". $receiveupdatestatus);
                return;
            }

            // add donation receivable to the admin
            //find the receive donation by member
            //check if the member already exists in the donations receivable table
            $membercheck = $dataRead->donationsreceivable_getbyidmember($mycon, $member_id);
            if (!$membercheck)
            {
                //add new member to the donations receivable column
                $addmember = $dataWrite->donationsreceivable_add($mycon, $member_id, '0', '0', '0');
                if (!$addmember) 
                {
                    // var_dump($addmember);
                    return;
                }
            }

            $donationsreceivable = $dataRead->donationsreceivable_getbyidmember($mycon, $member_id);

            //remove from the fund receivable
            $updatewithdrawn = $dataWrite->donationsreceivable_updatewithdrawn($mycon,$member_id,
            $donationsreceivable['withdrawn'] + $amount , $donationsreceivable['amount'] - $donationsreceivable['withdrawn'] - $amount);
            if (!$updatewithdrawn)
            {
                $mycon->rollBack();
                // var_dump($updatewithdrawn);
                return;
            }

            //get the name of the receiver 
            $receiverdetails = $dataRead->member_getbyid($mycon, $member_id);
            $monthlyfeepercentage = monthlyFeePecentate();
            $transfermessage = "<div class='container'>
                                <p>Dear ".$donations_getid['username'].",</p>
                                <p>You are due to pay for ". $monthlyfeepercentage. " Monthly Due of your current package. Kindly pay to only the 
                                account number provided on your dashboard and upload your proof of payment.</p>
                                <p> Amount placed is ".$amount." </p>
                                <p> Thank you.</p>
                                <p style='margin-top: 100px'><small><em>This message is auto-generated, please do not reply via your email.</em></small></p>
                            </div>";
            $receivermessage = "<div class='container'>
                                <p>Dear Admin".$receiverdetails['firstname']. " ". $receiverdetails['lastname']. ", </p>
                                <p>You have a new order to receive '.$monthlyfeepercentage.' month due from a participant. The sender information is on your dashboard when you login. 
                                Amount to receive is ".$amount." </p>
                                <p style='margin-top: 100px'><small><em>This message is auto-generated, please do not reply via your email.</em></small></p>
                            </div>";
            if(sendEmail($row['email'],$monthlyfeepercentage." Monthly Due Matched - Greencollar Networld", $transfermessage) && sendEmail($receiverdetails['email'], "Receive Payment (".$monthlyfeepercentage." Monthly Due) Matched - Greencollar Networld", $receivermessage)) {
                $mycon->commit();
                // echo "done!";
                echo $monthlyfeepercentage." Monthly Due Request Matched";
            } 
            else {
                $mycon->rollBack();
                // var_dump($receiverdetails);
            }
            }
        }

    }
}

//create a function that will reverse incomplete PH requests


//Create a function that will be checking if $amount is equal to $receivableamount
function checkAmount($amount, $member_id)
{
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();
    $donationsreceivable = $dataRead->donationsreceivable_getbyidmember($mycon, $member_id);
}

?>