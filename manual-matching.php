<?php
require_once("admin/config.php");
require_once("admin/inc_dbfunctions.php");

$mycon = databaseConnect();
$dataRead = New DataRead();

$currentuserid = getCookie("userid");

//get all that is due to receive fund manually
$recommitment = 2;
$donationsall = $dataRead->donations_getallwithrecommitmentdesc($mycon, $recommitment);


?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Greencollar Networld - Earn 30% ROI after 7 days on every fund you invest.">
        <meta name="author" content="Greencollar Networld">

        <link rel="shortcut icon" href="img/logo/logowfg.ico">

        <title>Manual Matching - <?php echo pageTitle(); ?></title>


        <link href="assets/plugins/custombox/css/custombox.css" rel="stylesheet">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="css/style.css" rel="stylesheet" type="text/css" />


        <link href="assets/plugins/bootstrap-table/css/bootstrap-table.min.css" rel="stylesheet" type="text/css" />


        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>
        
    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

               <?php include_once('inc_header.php') ?>

               <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">Manual Matching Information</h4>
                                <p class="text-muted page-title-alt">Welcome <?php echo getCookie("fullname") ?>!</p>
                            </div>
                        </div>
                        <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <h4 class="m-t-0 header-title"><b>Manual Matching</b></h4>
                            <p class="text-muted font-13 m-b-30">
                                Lists of all donations ready to be matched</code>.
                            </p>

                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Fullname (Username)</th>
                                    <th>Amount Paid</th>
                                    <th>Date Created</th>
                                    <th>Date for Receiving Fund</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>


                                <tbody>
                                    <?php
                                    $count = 0;
                                    $totalamount = 0;
                                    foreach($donationsall as $row)
                                    {
                                     if ($row['type'] !== '20% Monthly Due')
                                     {   
                                    ?>
                                <tr>
                                    <td><?php echo ++$count ?></td>
                                    <td><a href="members-view.php?username=<?php echo $row['username']; ?>">
                                    <?php echo $row['firstname']." ".$row['lastname']. " (". $row['username']. ") "; ?></a></td>
                                    <td><?php echo $row['donation_ph'] ?></td>
                                    <td><?php echo formatDate($row['donationcreatedon'], "yes"); ?></td>
                                    <td><?php echo formatDate($row['readydonation_gh'], "yes"); ?></td>
                                    <td><?php if ($row['matchingstatus'] == '5') echo 'Processed'; else if ($row['matchingstatus'] == '4') echo 'Rematched'; else if ($row['matchingstatus'] == '3') echo 'Pending'; else echo $row['type']; ?></td>
                                    <td><?php  if ((strtotime($row['readydonation_gh']) > strtotime(date("Y-m-d H:i:s")))) {
                                        ?>
                                        <a href="#manual-modal<?php echo $row['donation_id'] ?>" class="btn btn-primary waves-effect waves-light" data-animation="fadein" data-plugin="custommodal" 
                                                        data-overlaySpeed="200" data-overlayColor="#36404a">Manual Match </a>
                                        <?php
                                    }
                                    ?>
                                                        </td>
                                    
                                    
                                </tr>
                                <div id="manual-modal<?php echo $row['donation_id']; ?>" class="modal-demo">
                                <button type="button" class="close" onclick="Custombox.close();">
                                    <span>&times;</span><span class="sr-only">Close</span>
                                </button>
                                <h4 class="custom-modal-title bg-primary">Match Account Manually</h4>
                                <div class="custom-modal-text">
                                <div id="result<?php echo $row['donation_id'] ?>"></div>
                                   Are you sure you want to match <?php echo $row['firstname']; ?>'s donation manually?
                                   <div class="form-group extend error" id="extendpassworddiv<?php echo $row['donation_id'] ?>"> 
                                        <label for="extendpassword" class="control-label">Enter your password*</label> 
                                        <input type="password" class="form-control" name="extendpassword" id="extendpassword<?php echo $row['donation_id'] ?>" placeholder="Enter password  ">
                                    </div>
                                </div>
                                <div class="modal-footer"> 
                                    <button type="button" class="btn btn-custom waves-effect waves-light" type="button"  onclick="maergeAccount(<?php echo $row['member_id'] ?>, '<?php echo $row['donation_id']; ?>')">Yes</button>
                                    <button type="button" class="btn btn-danger waves-effect" onclick="Custombox.close();">No</button>
                                </div> 
                             </div>
                             <?php 
                                     }
                                    }
                                    ?>
                            </tbody>
                        </table>
                            </div>
                            </div>
         
            </div>
                </div> 

                </div>

                 <?php 
               include_once('inc_footer.php'); 
               ?>

               </div>
            </div>

                <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>

        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        <script src="assets/plugins/peity/jquery.peity.min.js"></script>

        <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
        <script src="js/custom.js"></script>


        <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>


        <!-- Modal-Effect -->
        <script src="assets/plugins/custombox/js/custombox.min.js"></script>
        <script src="assets/plugins/custombox/js/legacy.min.js"></script>

        <script type="text/javascript">
    $(document).ready(function () {
        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({keys: true});
        $('#datatable-responsive').DataTable();
        $('#datatable-colvid').DataTable({
            "dom": 'C<"clear">lfrtip',
            "colVis": {
                "buttonText": "Change columns"
            }
        });
    });

</script>
    </body>
</html>