<?php
require_once("admin/config.php");
require_once("admin/inc_dbfunctions.php");
$username = '';
if(isset($_GET['username']) &&  $_GET['username'] != '') $username = $_GET['username'];
else {
  showAlert('No user specified. Please click on the username to continue');
  openPage('members-lists.php');
}

$mycon = databaseConnect();
$dataRead = New DataRead();


$memberdetail = $dataRead->member_getbyusername($mycon,$username);


//get all PH and GH request of the user
$getallrequest = $dataRead->donations_getbyid($mycon, $memberdetail['member_id']);

$referalldetails = $dataRead->memberreferral_getbyid($mycon,$memberdetail['member_id']);


?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Greencollar Networld - Earn 30% ROI after 7 days on every fund you invest.">
        <meta name="author" content="Greencollar Networld">

        <link rel="shortcut icon" href="img/logo/logowfg.ico">

        <title><?php echo ucwords($username); ?>'s Information - <?php echo pageTitle(); ?></title>


        <link href="assets/plugins/custombox/css/custombox.css" rel="stylesheet">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="css/style.css" rel="stylesheet" type="text/css" />


        <link href="assets/plugins/bootstrap-table/css/bootstrap-table.min.css" rel="stylesheet" type="text/css" />


        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>
        
    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

               <?php include_once('inc_header.php') ?>

               <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title"><?php echo ucwords($memberdetail['firstname']); ?>'s Information</h4>
                                <p class="text-muted page-title-alt">Welcome <?php echo getCookie("fullname") ?>!</p>
                                <div style="margin-bottom: 15px">
                                    <button type="button" class="btn btn-custom waves-effect waves-light" type="button"  onclick="document.location.href='members-lists.php'">Back to Members Lists</button>
                                </div>
                            </div>
                        </div>
                                <div class="profile-detail card-box row">
                                    <div class="col-md-4 col-lg-3">
                                    <div style="padding-top: 30px">
                                        <img src="member_image/<?php if ($memberdetail['picturestatus'] == '0') echo 'avatar.png'; else echo $memberdetail['username'].'.jpg'; ?>"  class="img-circle" alt="<?php echo $memberdetail['username'] ?>_photo">
                                        <iframe name="actionframe" id="actionframe" width="1px" height="1px" frameborder="0"></iframe> 
                                    </div>
                                </div>
                                <div class="col-md-8 col-lg-9">
                                    <div  style="padding-top: 30px">
                                        <div class="text-left">
                                            <p class="text-muted font-13"><strong>Full Name :</strong> <span class="m-l-15"><?php echo $memberdetail['firstname']. " ". $memberdetail['lastname'] ?></span></p>

                                            <p class="text-muted font-13"><strong>Username :</strong> <span class="m-l-15"><?php echo $memberdetail['username'] ?></span></p>

                                            <p class="text-muted font-13"><strong>Mobile :</strong><span class="m-l-15"><?php echo $memberdetail['phonenumber']; ?></span></p>

                                            <p class="text-muted font-13"><strong>Email :</strong> <span class="m-l-15"><?php echo $memberdetail['email']; ?></span></p>

                                             <p class="text-muted font-13"><strong>Join since :</strong> <span class="m-l-15"><?php echo formatDate($memberdetail['createdon'], "yes"); ?></span></p>
                                             <p class="text-muted font-13"><strong>Status :</strong> <span class="m-l-15"><?php if ($memberdetail['status'] == '0') echo "<span class='text-custom'>Active</span>"; else if ($memberdetail['status'] == '3') echo "<span class='text-default'>Paused</span>"; else if($memberdetail['status'] == '10') echo "<span class='text-primary'>Activation fee pending</span>"; else if($memberdetail['status'] == '5') echo "<span class='text-danger'>Suspend Account</span>"; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <a href="#suspend-modal<?php echo $memberdetail['member_id'] ?>" class="btn btn-primary waves-effect waves-light" data-animation="fadein" data-plugin="custommodal" 
                                                        data-overlaySpeed="200" data-overlayColor="#36404a"><?php if($memberdetail['status'] == 5) echo "Unsuspend"; else echo "Suspend" ?> account </a> <br /><br />
                                                        <a href="#pause-modal<?php echo $memberdetail['member_id'] ?>" class="btn btn-default waves-effect waves-light" data-animation="fadein" data-plugin="custommodal" 
                                                        data-overlaySpeed="200" data-overlayColor="#36404a"><?php if($memberdetail['status'] == 3) echo "Unpause"; else echo "Pause" ?> account</a> <br /><br />
                                                        <a href="#delete-modal<?php echo $memberdetail['member_id'] ?>" class="btn btn-danger waves-effect waves-light" data-animation="fadein" data-plugin="custommodal" 
                                                        data-overlaySpeed="200" data-overlayColor="#36404a"><?php if($memberdetail['status'] == 8) echo "Undelete"; else echo "Delete" ?> account</a>
                                                        <a href="#upgrade-to-admin-modal<?php echo $memberdetail['member_id'] ?>" class="btn btn-custom waves-effect waves-light" data-animation="fadein" data-plugin="custommodal" 
                                                        data-overlaySpeed="200" data-overlayColor="#36404a"><?php if($memberdetail['role'] == 1) echo "Downgrade from"; else echo "Upgrade to" ?> Admin</a>

                                                        <div id="suspend-modal<?php echo $memberdetail['member_id']; ?>" class="modal-demo">
                                <button type="button" class="close" onclick="Custombox.close();">
                                    <span>&times;</span><span class="sr-only">Close</span>
                                </button>
                                <h4 class="custom-modal-title bg-primary">Suspend account</h4>
                                <div class="custom-modal-text">
                                <div id="<?php if ($memberdetail['status'] == 10) echo 'unsuspend'; else echo 'suspend' ?>result<?php echo $memberdetail['member_id'] ?>"></div>
                                   Are you sure you want to <?php if ($memberdetail['status'] == 10) echo 'unsuspend'; else echo 'suspend' ?> <?php echo $memberdetail['firstname']; ?>'s account?
                                   <div class="form-group extend error" id="extendpassworddiv<?php echo $memberdetail['member_id'] ?>"> 
                                        <label for="extendpassword" class="control-label">Enter your password*</label> 
                                        <input type="password" class="form-control" name="extendpassword" id="<?php if ($memberdetail['status'] == 10) echo 'unsuspend'; else echo 'suspend' ?><?php echo $memberdetail['member_id'] ?>" placeholder="Enter password  ">
                                        <input type="hidden" class="form-control" name="command" id="command<?php echo $memberdetail['member_id'] ?>" value="<?php echo $memberdetail['member_id'] ?>">
                                    </div>
                                </div>
                                <div class="modal-footer"> 
                                    <button type="button" class="btn btn-custom waves-effect waves-light" type="button"  onclick="accountStatus(<?php echo $memberdetail['member_id'] ?>, '<?php if ($memberdetail['status'] == 10) echo 'unsuspend'; else echo 'suspend' ?>')">Yes</button>
                                    <button type="button" class="btn btn-danger waves-effect" onclick="Custombox.close();">No</button>
                                </div> 
                             </div>
                             <div id="pause-modal<?php echo $memberdetail['member_id']; ?>" class="modal-demo">
                                <button type="button" class="close" onclick="Custombox.close();">
                                    <span>&times;</span><span class="sr-only">Close</span>
                                </button>
                                <h4 class="custom-modal-title bg-default">Pause Account</h4>
                                <div class="custom-modal-text">
                                <div id="<?php if ($memberdetail['status'] == 3) echo 'unpause'; else echo 'pause' ?>result<?php echo $memberdetail['member_id'] ?>"></div>
                                   Are you sure you want to <?php if ($memberdetail['status'] == 3) echo 'unpause'; else echo 'pause' ?> <?php echo $memberdetail['firstname']; ?>'s account?
                                   <div class="form-group extend error" id="extendpassworddiv<?php echo $memberdetail['member_id'] ?>"> 
                                        <label for="extendpassword" class="control-label">Enter your password*</label> 
                                        <input type="password" class="form-control" name="extendpassword" id="<?php if ($memberdetail['status'] == 3) echo 'unpause'; else echo 'pause' ?><?php echo $memberdetail['member_id'] ?>" placeholder="Enter password to pause this account">
                                        <input type="hidden" class="form-control" name="command" id="command<?php echo $memberdetail['member_id'] ?>" value="<?php echo $memberdetail['member_id'] ?>">
                                    </div> 
                                </div>
                                <div class="modal-footer"> 
                                    <button type="button" class="btn btn-custom waves-effect waves-light" type="button"  onclick="accountStatus(<?php echo $memberdetail['member_id'] ?>, '<?php if ($memberdetail['status'] == 3) echo 'unpause'; else echo 'pause' ?>')" >Yes</button>
                                    <button type="button" class="btn btn-danger waves-effect" onclick="Custombox.close();">No</button>
                                </div> 
                             </div>
                             <div id="upgrade-to-admin-modal<?php echo $memberdetail['member_id']; ?>" class="modal-demo">
                                <button type="button" class="close" onclick="Custombox.close();">
                                    <span>&times;</span><span class="sr-only">Close</span>
                                </button>
                                <h4 class="custom-modal-title bg-default">Change Account Status</h4>
                                <div class="custom-modal-text">
                                <div id="<?php if ($memberdetail['role'] == 1) echo 'downgrade'; else echo 'upgrade' ?>result<?php echo $memberdetail['member_id'] ?>"></div>
                                   Are you sure you want to <?php if ($memberdetail['role'] == 1) echo 'Downgrade from'; else echo 'Upgrade to' ?> Admin for <?php echo $memberdetail['firstname']; ?>'s account?
                                   <div class="form-group extend error" id="extendpassworddiv<?php echo $memberdetail['member_id'] ?>"> 
                                        <label for="extendpassword" class="control-label">Enter your password*</label> 
                                        <input type="password" class="form-control" name="extendpassword" id="<?php if ($memberdetail['role'] == 1) echo 'downgrade'; else echo 'upgrade' ?><?php echo $memberdetail['member_id'] ?>" placeholder="Enter password to change type status of this account">
                                        <input type="hidden" class="form-control" name="command" id="command<?php echo $memberdetail['member_id'] ?>" value="<?php echo $memberdetail['member_id'] ?>">
                                    </div> 
                                </div>
                                <div class="modal-footer"> 
                                    <button type="button" class="btn btn-custom waves-effect waves-light" type="button"  onclick="accountStatus(<?php echo $memberdetail['member_id'] ?>, '<?php if ($memberdetail['role'] == 1) echo 'downgrade'; else echo 'upgrade' ?>')" >Yes</button>
                                    <button type="button" class="btn btn-danger waves-effect" onclick="Custombox.close();">No</button>
                                </div> 
                             </div>
                             <div id="delete-modal<?php echo $memberdetail['member_id']; ?>" class="modal-demo">
                                <button type="button" class="close" onclick="Custombox.close();">
                                    <span>&times;</span><span class="sr-only">Close</span>
                                </button>
                                <h4 class="custom-modal-title bg-danger">Delete Account</h4>
                                <div class="custom-modal-text">
                                <div id="deleteresult<?php echo $memberdetail['member_id'] ?>"></div>
                                   Are you sure you want to delete <?php echo $memberdetail['firstname']; ?>'s account?
                                   <div class="form-group extend error" id="extendpassworddiv<?php echo $memberdetail['member_id'] ?>"> 
                                        <label for="extendpassword" class="control-label">Enter your password*</label> 
                                        <input type="password" class="form-control" name="extendpassword" id="delete<?php echo $memberdetail['member_id'] ?>" placeholder="Enter password to delete this account">
                                        <input type="hidden" class="form-control" name="command" id="command<?php echo $memberdetail['member_id'] ?>" value="<?php echo $memberdetail['member_id'] ?>">
                                    </div> 
                                </div>
                                <div class="modal-footer"> 
                                    <button type="button" class="btn btn-custom waves-effect waves-light" type="button"  onclick="accountStatus(<?php echo $memberdetail['member_id'] ?>, 'delete')">Yes</button>
                                    <button type="button" class="btn btn-danger waves-effect" onclick="Custombox.close();">No</button>
                                </div> 
                             </div>
                                </div>
                                </div>
                        <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <h4 class="m-t-0 header-title"><b>List of all Donations Made by <?php echo $memberdetail['firstname']; ?></b></h4>
                            <p class="text-muted font-13 m-b-30">
                                Lists of all donations</code>.
                            </p>

                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Amount</th>
                                    <th>Type</th>
                                    <th>Date</th>
                                </tr>
                                </thead>


                                <tbody>
                                    <?php
                                    $count = 0;
                                    foreach($getallrequest as $row)
                                    {
                                    
                                    ?>
                                <tr>
                                    <td><?php echo ++$count ?></td>
                                    <td><?php if ($row['donation_ph'] != '') echo $row['donation_ph']; else echo $row['donation_gh']; ?></td>
                                    <td><?php echo $row['type']; ?></td>
                                    <td><?php echo formatDate($row['createdon'], "yes"); ?></td>
                                  
                                </tr>
                                <?php
                                } 
                                ?>
                                 <tr>
                                    <td>Total Requests</td>
                                    <td colspan="3"><?php echo $count ?></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                            </div>
                            </div>
                            <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <h4 class="m-t-0 header-title"><b>Upline Info for <?php echo $memberdetail['firstname']; ?></b></h4>
                            <p class="text-muted font-13 m-b-30">
                                Infomation about the upline</code>.
                            </p>
                                <?php 
                                    if ($referalldetails)
                                    {

                                ?>
                            <div  style="padding-top: 30px">
                                        <div class="text-left">
                                            <p class="text-muted font-13"><strong>Full Name :</strong> <span class="m-l-15"><?php echo $referalldetails['firstname']. " ". $referalldetails['lastname'] ?></span></p>

                                            <p class="text-muted font-13"><strong>Username :</strong> <span class="m-l-15"><?php echo $referalldetails['username'] ?></span></p>

                                            <p class="text-muted font-13"><strong>Mobile :</strong><span class="m-l-15"><?php echo $referalldetails['phonenumber']; ?></span></p>

                                            <p class="text-muted font-13"><strong>Email :</strong> <span class="m-l-15"><?php echo $referalldetails['email']; ?></span></p>

                                             <p class="text-muted font-13"><strong>Join since :</strong> <span class="m-l-15"><?php echo formatDate($referalldetails['createdon'], "yes"); ?></span></p>

                                        </div>
                            </div>
                            <?php 
                                }
                            else {
                                echo "<p> No Referral </p>";
                            }
                            ?>
                            </div>
         
            </div>
                </div> 

                </div>

                 <?php 
               include_once('inc_footer.php'); 
               ?>

               </div>
            </div>

                <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>

        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        <script src="assets/plugins/peity/jquery.peity.min.js"></script>

        <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
        <script src="js/custom.js"></script>


        <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>


        <!-- Modal-Effect -->
        <script src="assets/plugins/custombox/js/custombox.min.js"></script>
        <script src="assets/plugins/custombox/js/legacy.min.js"></script>

        <script type="text/javascript">
    $(document).ready(function () {
        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({keys: true});
        $('#datatable-responsive').DataTable();
        $('#datatable-colvid').DataTable({
            "dom": 'C<"clear">lfrtip',
            "colVis": {
                "buttonText": "Change columns"
            }
        });
    });

</script>
    </body>
</html>