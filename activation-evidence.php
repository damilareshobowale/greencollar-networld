<?php
require_once("admin/config.php");
require_once("admin/inc_dbfunctions.php");

$mycon = databaseConnect();
$mycon = databaseConnect();
$dataRead = New DataRead();

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Greencollar Networld - Earn 30% ROI after 7 days on every fund you invest.">
        <meta name="author" content="Greencollar Networld">

        <link rel="shortcut icon" href="img/logo/logowfg.ico">

        <title>Activation Evidence - <?php echo pageTitle(); ?></title>


        <link href="assets/plugins/custombox/css/custombox.css" rel="stylesheet">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="css/style.css" rel="stylesheet" type="text/css" />


        <link href="assets/plugins/bootstrap-table/css/bootstrap-table.min.css" rel="stylesheet" type="text/css" />


        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>
        
    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

               <?php include_once('inc_header.php') ?>

               <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">Activation Evidence Uplaod  Information</h4>
                                <p class="text-dark page-title-alt">Welcome <?php echo getCookie("fullname") ?>!</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <?php
                                        if ((isset($_GET['confirm']) && $_GET['confirm'] != ''))
                                        {
                                    ?>
                                    <div class='alert alert-info alert-dismissable'>
                                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                                            <div id='result'></div>
                                        </div>
                                    <?php
                                    }

                                    ?>
                                    <h4 class="m-t-0 header-title"><b>Activation Fee Evidence details</b></h4>
                                    <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Date added</th>
                                    <th>Member Name</th>
                                    <th>Message</th>
                                    <th>Upload</th>
                                    <th>Action</th>
                                </tr>
                                </thead>


                                <tbody>
                                    <?php
                                    $count = 0;
                                    $type = 0;
                                    if ($memberdetails['role'] == '1')
                                    {
                                      $type = 1;
                                    }
                                    $activation_evidence_upload_add = $dataRead->activation_evidence_upload_getall($mycon);
                                    foreach ($activation_evidence_upload_add as $row)
                                    {
                                    ?>
                                <tr>
                                    <td><?php echo ++$count ?></td>
                                    <td><?php echo formatDate($row['created_on'], "yes"); ?></td>
                                    <td><?php echo $row['firstname']. " ".$row['lastname']; ?></td>
                                    <td><?php echo $row['message']; ?></td>
                                    <td><a href="activation-evidence/<?php echo generatePassword($row['member_id']); ?>.jpg" target="_blank"> View Evidence uploaded </a></td>
                                    <td>
                                    <?php if ($row['status'] != '0') {
                                      ?>
                                    <a href="#confirm-modal<?php echo $row['member_id'] ?>" class="btn btn-custom waves-effect waves-light" data-animation="fadein" data-plugin="custommodal" 
                                    data-overlaySpeed="200" data-overlayColor="#36404a">Confirm</a>
                                    <button class="btn btn-danger waves-effect waves-light" data-toggle="modal" data-target="#flag-model<?php echo $row['member_id'] ?>">Flag</button>
                                    <?php
                                    }
                                    else {
                                    ?>
                                    <span class='text-custom'>Confirmed</span>
                                      <?php
                                    }
                                    ?>
                                    </td>

                                </tr>
                                <div id="confirm-modal<?php echo $row['member_id']; ?>" class="modal-demo">
                                <button type="button" class="close" onclick="Custombox.close();">
                                    <span>&times;</span><span class="sr-only">Close</span>
                                </button>
                                <h4 class="custom-modal-title" style="background-color: #356800;">Are you sure?</h4>
                                <div class="custom-modal-text">
                                   Are you sure you want to confirm <?php echo $row['firstname']; ?>'s evidence?
                                </div>
                                <div class="modal-footer"> 
                                    <button type="button" class="btn btn-custom waves-effect waves-light" type="button"  onclick="document.location.href='?confirm=<?php echo $row['member_id']; ?>&amountpaid=<?php echo $row['amount'] ?>'">Yes</button>
                                    <button type="button" class="btn btn-primary waves-effect" onclick="Custombox.close();">No</button>
                                </div> 
                             </div>
                             <div id="flag-model<?php echo $row['member_id'] ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog"> 
                                            <div class="modal-content"> 
                                                <div class="modal-header"> 
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                    <h4 class="modal-title text-danger">Give Message to flag evidence</h4> 
                                                </div>
                                                <form action="admin/actionmanager.php" method="post" id="updateActivationEvidence" >
                                                <div class="modal-body">
                                                    <p class="text-left text-primary is-required">* is required </p>
                                                    <div id="updatebankresult"></div>
                                                    <span id="account_id" class="<?php echo $row['accountdetail_id'] ?>"></span>
                                                    <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group update" id="updatenamediv"> 
                                                                <label for="name" class="control-label">Reply Message to <?php echo $memberdetails['firstname']; ?>*</label> 
                                                                <textarea rows="6" class="form-control" name="flagmessage" id="flagmessage"></textarea> 
                                                            </div> 
                                                        </div>
                                                    </div> 
                                                </div> 
                                                <div class="modal-footer"> 
                                                    <button class="btn btn-custom waves-effect waves-light" type="submit" id="bankaccountupdatebutton">Send Message</button>
                                                    <input type="hidden" value="<?php echo $row['member_id'] ?>" id="member_id" name="member_id" /> 
                                                    <input type="hidden" value="<?php echo $row['amount'] ?>" id="amountpaid" name="amountpaid" /> 
                                                    <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal">Close</button>
                                                </div> 
                                            </form>
                                            </div> 
                                        </div>
                                    </div><!-- /.modal -->
                                <?php
                                } 
                                ?>
                            </tbody>
                        </table>
                            </div>
                <!-- Modal -->
         
            
                                  </div>
                </div> 

                </div>

                 <?php 
               include_once('inc_footer.php'); 
               ?>

               </div>
            </div>                   

                <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>

        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        <script src="assets/plugins/peity/jquery.peity.min.js"></script>

        <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
        <script src="js/custom.js"></script>


        <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>


        <!-- Modal-Effect -->
        <script src="assets/plugins/custombox/js/custombox.min.js"></script>
        <script src="assets/plugins/custombox/js/legacy.min.js"></script>

        <script type="text/javascript">
    $(document).ready(function () {
        $('#datatable').dataTable();
        $('#datatable2').dataTable();
        $('#datatable-keytable').DataTable({keys: true});
        $('#datatable-responsive').DataTable();
        $('#datatable-colvid').DataTable({
            "dom": 'C<"clear">lfrtip',
            "colVis": {
                "buttonText": "Change columns"
            }
        });
    });

</script>
        <?php


            if (isset($_GET['confirm']) && $_GET['confirm'] != "") confirmActivationEvidence($_GET['confirm'], $_GET['amountpaid']);
          
            //delete all bank accounts
            function confirmActivationEvidence($member_id, $amountpaid)
            {
                $dataRead = New DataRead();
                $dataWrite = New DataWrite();
                $mycon = databaseConnect();

                //1. to confirm, change the member status to 0
                // 2. update the activation evidence table and input the message as 'confirmed successfully'
                
                $mycon->beginTransaction();
                // 1
                $membersupdate = $dataWrite->members_updatestatus($mycon, $member_id, '0');
                if (!$membersupdate)
                {
                  $mycon->rollBack();  
                  echo "<script type='text/javascript'>
                            $('#result').html('Sorry, the user no longer exists, or the account has already been confirmed.');
                             window.setTimeout(function(){
                            document.location.reload();
                        },4000);
                          </script>";
                    return;
                }

                $flagmessage = 'User already confirmed.';
                
                // 2
                $updateactivationevidencestatus = $dataWrite->updateActivationEvidenceStatus($mycon, $amountpaid, $member_id, '0', $flagmessage);
                if(!$updateactivationevidencestatus)
                {
                    $mycon->rollBack();
                    echo "<script type='text/javascript'>
                            $('#result').html('There was an error confirming the evidence, please try again later.');
                            window.setTimeout(function(){
                            document.location.href='activation-evidence.php';
                        },4000);
                          </script>";
                    return;
                }
                $mycon->commit();
                echo "<script type='text/javascript'>
                            $('#result').html('Confirmed successfully.');
                             window.setTimeout(function(){
                            document.location.href='activation-evidence.php';
                        },4000);
                          </script>";
                    return;

            }

        ?>
    </body>
</html>