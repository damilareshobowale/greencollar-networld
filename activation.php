<?php
require_once('admin/config.php');
require_once('admin/inc_dbfunctions.php');

$dataRead = New DataRead();
$dataWrite = New DataWrite();
$mycon = databaseConnect();
$currentuserid = getCookie("userid");

$memberdetails = $dataRead->member_getbyid($mycon, $currentuserid);

$memberactivationdetails = $dataRead->memberactivationdetails($mycon, $currentuserid);

//get the account details to pay to
$activationfeedetails = $dataRead->bankaccountdetails_one($mycon,'1');

//get the account amount
$amountaccountdetails = $dataRead->amountaccountdetails_one($mycon);

//check whether the user has made any donation before
$donationdetails  = $dataRead->donations_getbyid($mycon, $memberdetails['member_id'], "LIMIT 1");

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Greencollar Networld - Earn 30% ROI after 7 days on every fund you invest.">
        <meta name="author" content="Greencollar Networld">

        <link rel="shortcut icon" href="img/logo/logogcn.ico">

        <title><?php if ($donationdetails != null) echo 'Reactivation'; else echo 'Activation'; ?> Page - <?php echo pageTitle(); ?></title>


        <link href="assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
        
        <link href="assets/plugins/custombox/css/custombox.css" rel="stylesheet">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="css/style.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>
        <style>

          .footer {
            left: 0px !important;
          }
        </style>


    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar" style="position: absolute;">

               <?php //include_once('inc_header.php') ?>
               <div class="text-center" style="padding: 10px 0px;">
                        <a href="../index.php" class="logo">
                        <span><img src="img/logo/logogcn.png" height="72" width=""/></span></a>
                        
                        <!-- Image Logo here -->
                    </div>
        </div>


            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page" style="margin-left: 0px;">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title"><?php if ($donationdetails != null) echo 'Reactive'; else echo 'Activate'; ?> Your Account</h4>
                                <p class="text-dark page-title-alt">Welcome <?php echo $memberdetails['firstname']; ?>!</p>
                            </div>
        </div>
                         <?php
                        if ($memberdetails['memberstatus'] == '10')
                        {
                        ?>
                         <div class="card-box bg-danger text-white">
                              <h4 class="text-white header-title m-t-0"><?php if ($donationdetails != null) echo 'Reactivation Your Account'; else echo 'Activation Fee Request'; ?> </h4>
                              <?php if ($donationdetails != null) 
                              {
                                 ?>
                                 <p> Activation Fee </p>
                            <?php
                              }
                              ?>
                              <p class="text-white m-b-30 font-15">
                                        Dear <?php echo $memberdetails['firstname'] ?>,

                                    </p>
                                    <p class="text-white m-b-30 font-15">
                                    <?php

                                        if ($donationdetails != null)
                                        {
                                        ?>
                                        You are to pay the fee of  &#x20a6;<?php echo $amountaccountdetails['amount'] ?> as a defaulter 
                                       for the reopening of suspended account. 
                                        <?php
                                        }
                                        else {
                                        ?>
                                            Kindly make the payment of &#x20a6;<?php echo $amountaccountdetails['amount'] ?> to the account below for account activation. 
                                        <?php
                                        }
                                        ?>
                                    </p>
                                    <p class="text-white m-b-30 font-15">
                                       For complaints and clearification, contact <span style='text-decoration: underline;font-weight: bold;'>support@greencollarnetworld.com</span> or use our live chat support or call: 09076673770

                                    </p>
                                    <p class="text-white m-b-30 font-15 text-uppercase">
                                       Account Name: <?php echo $activationfeedetails['bankaccountname'] ?> <br /><br />

                                       Account Number: <?php echo $activationfeedetails['bankaccountnumber'] ?> <br /><br />

                                       Bank Name: <?php echo $activationfeedetails['bankname'] ?> <br /><br />

                                    </p>
                                    <?php
                                        if ($memberactivationdetails['message'] != null && $memberactivationdetails['status'] == '3')
                                        {
                                    ?>
                                      <h3 class="text-white text-bold"><?php echo $memberactivationdetails['message']; ?></h3>
                                    <?php
                                        }
                                        else if ($memberactivationdetails['status'] == 5) {
                                          ?>
                                          <h3><span class="blinking"><?php echo $memberactivationdetails['message']; ?></span></h3>
                                          <?php 
                                        }
                                          else {
                                            ?>
                                          <button class="btn btn-custom waves-effect waves-light" data-toggle="modal" data-target="#upload-pop-model">Upload POP</button>
                                          <?php
                                        }
                                            ?>
                                          
                                        <p class="text-white m-b-30 m-t-30 font-15">
                                       After verification and confirmation, you will be able to continue to use your account. 

                                    </p>
                                    <div style="height: 30px;width: 100%;justify-content:center;display:flex;">
                                        <p stle="text-align: center;">Want to Logout? <a href="login.php?logout=yes" style="color: #FFFFFF;text-decoration: underline;">Click here</a></p>
                                    </div>
                                    
                                        <div id="upload-pop-model" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog"> 
                                            <div class="modal-content"> 

                                                <div class="modal-header"> 
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                    <h4 class="modal-title">Upload POP</h4> 
                                                </div> 
                                                <iframe name="actionframe" id="actionframe" width="1px" height="1px" frameborder="0"></iframe> 
                                                <form action='admin/actionmanager.php' method='post' id='evidenceform' target="actionframe" enctype="multipart/form-data">
                                                <div class="modal-body">        
                                                <div class="matchingpaid" id="<?php echo $row['matching_id'] ?>"></div>
                                                        <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group paid error" id="amountpaiddiv"> 
                                                                <label for="amountpaid" class="control-label">Amount Paid*</label> 
                                                                <input type="text" class="form-control" name="amountpaid" id="amountpaid" placeholder="Enter amount">
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group paid error" id="uploadevidencediv"> 
                                                                <label for="uploadevidence" class="control-label">Upload Evidence*</label> 
                                                                <input type="file" class="form-control" name="uploadevidence" id="uploadevidence">
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer"> 
                                                        <button type="submit" class="btn btn-custom waves-effect waves-light">Save</button>
                                                        <input type='hidden' name='command' id='command' value='activation_evidence_add'>
                                                        <button type="button" class="btn btn-danger waves-effect" id="paidresetbutton">Reset</button> 
                                                        <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal">Close</button>
                                                    </div> 
                                                </div> 
                                                    </form>
                                            </div> 
                                        </div>
                                    </div><!-- /.modal -->
                                    </div>
                        <?php
                        }
                        else {
                          openPage('dashboard.php');
                        }

                        ?>
                        </div>


                    </div> <!-- container -->

                </div> <!-- content -->
              
               <?php 
              
               include_once('inc_footer.php'); 
               ?>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


  

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>

        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        <script src="assets/plugins/peity/jquery.peity.min.js"></script>

        <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

         <!-- Modal-Effect -->
        <script src="assets/plugins/custombox/js/custombox.min.js"></script>
        <script src="assets/plugins/custombox/js/legacy.min.js"></script>

        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
        <script src="js/custom.js"></script>




    </body>
</html>